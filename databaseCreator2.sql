START TRANSACTION;

DROP USER IF EXISTS 'admin'@'localhost';
DROP DATABASE IF EXISTS `ABP_BD`;

CREATE USER 'admin'@'localhost' IDENTIFIED BY 'admin';

CREATE DATABASE IF NOT EXISTS `ABP_BD`; 
USE ABP_BD;

CREATE TABLE IF NOT EXISTS `USER`(
	`email` VARCHAR(64) NOT NULL,
	`password` VARCHAR(255) NOT NULL,
	`nombre` VARCHAR(64) NOT NULL,
	`edad` INT UNSIGNED NOT NULL,
	`genero` ENUM('M','F') NOT NULL,
	`dni` CHAR(9) NOT NULL,
	`tipo`ENUM('D','E','A') NOT NULL,
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,

	CONSTRAINT `uniques_in_USER` UNIQUE(dni,email),
	CONSTRAINT `pk_USER` PRIMARY KEY USING HASH(`id`)
);

CREATE TABLE IF NOT EXISTS `PISTA`(
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(64) NOT NULL,
	`descripcion` VARCHAR(256),
	`inicio` DATETIME DEFAULT NULL,
	`fin` DATETIME DEFAULT NULL,

	CONSTRAINT `uniques_in_PISTA` UNIQUE(nombre),
	CONSTRAINT `pk_PISTA` PRIMARY KEY USING HASH(`id`)
);

CREATE TABLE IF NOT EXISTS `RESERVA`(
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`hora` DATETIME NOT NULL,
	`pista` INT UNSIGNED NOT NULL,
	`creador` INT UNSIGNED NOT NULL,
	`confirmada` BOOLEAN NOT NULL,

	CONSTRAINT `fk_Pista_Reservada` FOREIGN KEY (`pista`) REFERENCES `PISTA`(`id`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `fk_Creador_Reserva` FOREIGN KEY (`creador`) REFERENCES `USER`(`id`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `pk_PISTA` PRIMARY KEY USING HASH(`id`)
);

CREATE TABLE IF NOT EXISTS `PARTIDO`(
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`reservaID` INT UNSIGNED DEFAULT NULL,

	CONSTRAINT `fk_Reserva_pista` FOREIGN KEY (`reservaID`) REFERENCES `RESERVA`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `pk_PARTIDO` PRIMARY KEY USING HASH(`id`)
);

CREATE TABLE IF NOT EXISTS `INSCRIPCION`(
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`jugador` INT UNSIGNED NOT NULL,
	`partido` INT UNSIGNED NOT NULL,

	CONSTRAINT `fk_jugador` FOREIGN KEY (`jugador`) REFERENCES `USER`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `fk_partido` FOREIGN KEY (`partido`) REFERENCES `PARTIDO`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `pk_INSCRIPCION` PRIMARY KEY USING HASH(`id`)
);

CREATE TABLE IF NOT EXISTS `NIVEL`(
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(10) NOT NULL,
	`descripcion` VARCHAR(128),

	CONSTRAINT `pk_NIVEL` PRIMARY KEY USING HASH(`id`)
);

CREATE TABLE IF NOT EXISTS `CATEGORIA`(
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(10) NOT NULL,
	`descripcion` VARCHAR(128),

	CONSTRAINT `pk_CATEGORIA` PRIMARY KEY USING HASH(`id`)
);

CREATE TABLE IF NOT EXISTS `CAMPEONATO`(
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`nombre` VARCHAR(50) NOT NULL,
	`inicio` DATETIME,
	`fin` DATETIME,
	`normativa` TEXT NOT NULL,
	`nivel` INT UNSIGNED,
	`categoria` INT UNSIGNED,

	CONSTRAINT `fk_NIVEL` FOREIGN KEY (`nivel`) REFERENCES `NIVEL`(`id`) ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT `fk_CATEGORIA` FOREIGN KEY (`categoria`) REFERENCES `CATEGORIA`(`id`) ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT `pk_CAMPEONATO` PRIMARY KEY USING HASH(`id`)
);

CREATE TABLE IF NOT EXISTS `PAREJA`(
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`capitan` INT UNSIGNED NOT NULL,
	`acomp` INT UNSIGNED NOT NULL,

	CONSTRAINT `fk_CAPITAN` FOREIGN KEY (`capitan`) REFERENCES `USER`(`id`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `fk_ACOMP` FOREIGN KEY (`acomp`) REFERENCES `USER`(`id`) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT `pk_PAREJA` PRIMARY KEY USING HASH(`id`)
);

CREATE TABLE IF NOT EXISTS `ENFRENTAMIENTO`(
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`pareja1` INT UNSIGNED NOT NULL,
	`pareja2`INT UNSIGNED NOT NULL,
	`res1` INT UNSIGNED NOT NULL DEFAULT 0,
	`res2` INT UNSIGNED NOT NULL DEFAULT 0,
	`reserva` INT UNSIGNED DEFAULT NULL,

	CONSTRAINT `fk_Pareja1` FOREIGN KEY (`pareja1`) REFERENCES `PAREJA`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `fk_Pareja2` FOREIGN KEY (`pareja2`) REFERENCES `PAREJA`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `fk_Reserva` FOREIGN KEY (`reserva`) REFERENCES `RESERVA`(`id`) ON UPDATE CASCADE ON DELETE SET NULL,
	CONSTRAINT `pk_ENFRENTAMIENTO` PRIMARY KEY USING HASH(`id`)
);

CREATE TABLE IF NOT EXISTS `BLOQUE`(
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`campeonatoID` INT UNSIGNED NOT NULL,
	`numParejas` INT UNSIGNED DEFAULT NULL,

	CONSTRAINT `fk_Bloque_Campeonato` FOREIGN KEY (`campeonatoID`) REFERENCES `CAMPEONATO`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `pk_BLOQUE` PRIMARY KEY USING HASH(`id`)
);

CREATE TABLE IF NOT EXISTS `PLAYOFF`(
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`comienzo` DATETIME NOT NULL,
	`campeonatoID` INT UNSIGNED NOT NULL,

	CONSTRAINT `fk_PLAOFF_Campeonato` FOREIGN KEY (`campeonatoID`) REFERENCES `CAMPEONATO`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `pk_PLAYOFF` PRIMARY KEY USING HASH(`id`)
);


CREATE TABLE IF NOT EXISTS `INSCRIPCIONCAMPEONATO`(
	`id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
	`campeonatoID` INT UNSIGNED NOT NULL,
	`pareja` INT UNSIGNED NOT NULL,
	`idBloque` INT UNSIGNED DEFAULT NULL,

	CONSTRAINT `fk_INSCRIPCION_pareja_Campeonato` FOREIGN KEY (`pareja`) REFERENCES `PAREJA`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `fk_INSCRIPCION_Campeonato` FOREIGN KEY (`campeonatoID`) REFERENCES `CAMPEONATO`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `fk_Bloque_Pareja` FOREIGN KEY (`idBloque`) REFERENCES `BLOQUE`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `pk_INSCRIPCIONCAMPEONATO` PRIMARY KEY USING HASH(`id`)
);

CREATE TABLE IF NOT EXISTS `ENFRENTAMIENTOSBLOQUE`(
	`idBloque` INT UNSIGNED NOT NULL,
	`idEnfrentamiento` INT UNSIGNED NOT NULL,

	CONSTRAINT `fk_Enfrentamiento_Bloque` FOREIGN KEY (`idEnfrentamiento`) REFERENCES `ENFRENTAMIENTO`(`id`) ON DELETE CASCADE ON UPDATE CASCADE, 
	CONSTRAINT `fk_Bloque_Enfrentamiento` FOREIGN KEY (`idBloque`) REFERENCES `BLOQUE`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `pk_PAREJASBLOQUE` PRIMARY KEY USING HASH(`idBloque`,`idEnfrentamiento`)
);

CREATE TABLE IF NOT EXISTS `PARTIDOSPLAYOFF`(
	`playoff` INT UNSIGNED NOT NULL,
	`idEnfrentamiento` INT UNSIGNED NOT NULL,
	`fase` INT UNSIGNED NOT NULL,

	CONSTRAINT `fk_Enfrentamiento_playoff` FOREIGN KEY (`idEnfrentamiento`) REFERENCES `ENFRENTAMIENTO`(`id`) ON DELETE CASCADE ON UPDATE CASCADE, 
	CONSTRAINT `fk_playoff` FOREIGN KEY (`playoff`) REFERENCES `PLAYOFF`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT `pk_PARTIDOS_PLAYOFF` PRIMARY KEY USING HASH(`playoff`,`idEnfrentamiento`)
);

GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE `USER` TO 'admin'@'localhost';
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE `PISTA` TO 'admin'@'localhost';
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE `RESERVA` TO 'admin'@'localhost';
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE `PARTIDO` TO 'admin'@'localhost';
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE `INSCRIPCION` TO 'admin'@'localhost';
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE `PAREJA` TO 'admin'@'localhost';
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE `USER` TO 'admin'@'localhost';
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE `NIVEL` TO 'admin'@'localhost';
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE `CAMPEONATO` TO 'admin'@'localhost';
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE `CATEGORIA` TO 'admin'@'localhost';
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE `ENFRENTAMIENTO` TO 'admin'@'localhost';
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE `BLOQUE` TO 'admin'@'localhost';
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE `PLAYOFF` TO 'admin'@'localhost';
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE `INSCRIPCIONCAMPEONATO` TO 'admin'@'localhost';
 -- GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE `PAREJASBLOQUE` TO 'admin'@'localhost';
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE `ENFRENTAMIENTOSBLOQUE` TO 'admin'@'localhost';
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE `PARTIDOSPLAYOFF` TO 'admin'@'localhost';

INSERT INTO `USER` VALUES ('deportista@prueba.com', '', 'Deportista', 25, 'M', '12345678A', 'D', 0);
INSERT INTO `USER` VALUES ('admin@prueba.com', '', 'Administrador', 25, 'F', '34567891B', 'A', 0);
INSERT INTO `USER` VALUES ('deportista2@prueba.com', '', 'Deportista2', 26, 'F', '23456789C', 'D', 0);
INSERT INTO `USER` VALUES ('deportista3@prueba.com', '', 'Deportista3', 27, 'M', '01234567D', 'D', 0);
INSERT INTO `USER` VALUES ('deportista4@prueba.com', '', 'Deportista4', 28, 'F', '56789012E', 'D', 0);

INSERT INTO `PISTA` VALUES (1, 'Pista Mayor', 'La pista grande', '0001-01-01 09:00:00', '0001-01-01 22:00:00');
INSERT INTO `RESERVA` VALUES (1,  '2019-11-14 12:00:00', 1, 2, 0);
INSERT INTO `PARTIDO` VALUES (1, 1);

INSERT INTO `INSCRIPCION` VALUES (0, 3, 1);
INSERT INTO `INSCRIPCION` VALUES (0, 4, 1);
INSERT INTO `INSCRIPCION` VALUES (0, 5, 1);

INSERT INTO `NIVEL` VALUES('1','P','Es el nivel más básico');

INSERT INTO `CATEGORIA` VALUES('1','M','Es el nivel más básico');

INSERT INTO `CAMPEONATO` VALUES(0, 'Campeonato Prueba','2019-11-10 23:59:59','2019-11-21 23:59:59','Lorem ipsum dolor sit amet, consectetur adipiscing elit. In fermentum ornare sapien vitae ultrices. Nulla quis nulla ligula. Vestibulum non mauris nulla. Ut in porttitor quam. Nullam sed neque vitae turpis bibendum efficitur eget sed orci. Aliquam eget volutpat sapien, in tristique felis. Integer cursus arcu et tincidunt molestie. Sed porttitor, mauris dictum ornare bibendum, elit purus pretium nulla, vel facilisis eros ipsum non sapien. Nullam lacinia egestas elit, et pellentesque nibh imperdiet in. Cras ut neque sed tellus posuere tempor. Maecenas mi leo, ornare id faucibus eu, sodales eget augue. Donec ac lorem in dui viverra accumsan. Nam accumsan diam et sem vestibulum, sit amet euismod mi laoreet. Phasellus porta elit et pulvinar condimentum. Sed rutrum vulputate lectus, sit amet consequat sem convallis quis.
Cras luctus gravida augue, et congue justo faucibus et. Nulla accumsan ex sit amet fringilla finibus. Aenean scelerisque, dui nec dictum convallis, ex elit scelerisque dolor, et convallis diam odio non nibh. Aliquam sed ullamcorper ipsum. Ut tristique nulla quam, in ornare nisi lacinia et. Pellentesque et laoreet nulla. Praesent pulvinar, metus vitae imperdiet tempus, metus arcu egestas nibh, a molestie ex arcu ut velit. Phasellus tristique mattis arcu eu sagittis. Mauris blandit mattis neque, at interdum dolor varius et. Maecenas nulla nunc, rutrum porttitor arcu et, aliquam dignissim nibh. Vivamus auctor sollicitudin orci, eget laoreet urna scelerisque eget.
Aenean finibus nisl at dui volutpat tempus. Maecenas malesuada, quam at finibus eleifend, orci arcu scelerisque risus, et rutrum lacus magna ut lectus. Vestibulum lacinia, arcu posuere suscipit lobortis, leo neque euismod orci, at cursus erat libero ut arcu. Donec sit amet lectus at est posuere eleifend. Vivamus nec velit id dolor gravida gravida. Cras lacinia neque sit amet magna auctor, eu luctus neque auctor. Aliquam nec ligula scelerisque, volutpat risus sed, ullamcorper dui. Donec lorem tellus, feugiat vitae mi eu, condimentum tempus libero. Donec efficitur ipsum magna, in varius mi pellentesque eget. Nam non suscipit augue. Nulla egestas commodo tincidunt. Proin metus leo, pretium vel rhoncus pulvinar, vehicula ut mauris. Nullam luctus suscipit consequat. Nullam vitae turpis velit. Donec sapien odio, egestas sit amet ultrices in, semper ut libero. Quisque laoreet enim non orci hendrerit, sit amet finibus ligula consectetur.
Quisque eget justo ut ligula blandit venenatis ac et erat. Ut non elit vel mauris dictum bibendum nec quis nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Morbi lobortis, felis varius pellentesque vestibulum, eros lectus aliquam nulla, non consequat lorem sapien nec libero. Praesent eget justo posuere nulla dapibus finibus. Nunc imperdiet lectus ut tortor dictum, vel eleifend ante rhoncus. Vestibulum scelerisque dui tellus, eu porttitor leo aliquam congue. Duis at felis vel metus tincidunt interdum ut vitae nibh.
','1','1');

COMMIT;
