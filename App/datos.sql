use APPBD;

DELETE FROM PISTA;
INSERT INTO PISTA VALUES (0, 'Pista Mayor', 'La mayor pista', '0001-01-01 09:00:00', '0001-01-01 22:00:00');
INSERT INTO PISTA VALUES (1, 'Pista Alpha', 'La pista original', '0001-01-01 09:00:00', '0001-01-01 22:00:00');
INSERT INTO PISTA VALUES (2, 'Pista Beta', 'La pista que sigue a la original', '0001-01-01 09:00:00', '0001-01-01 22:00:00');

DELETE FROM USER;

insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('afrangleton0@baidu.com', '', 'Angy Frangleton', 46, 'F', '175-42', 'D', 1);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('bshilston1@nyu.edu', '', 'Bennie Shilston', 5, 'M', '388-16', 'D', 2);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('dalekseicik2@google.de', '', 'Demetris Alekseicik', 100, 'M', '652-53', 'D', 3);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('apriddy3@walmart.com', '', 'Archambault Priddy', 36, 'M', '533-14', 'D', 4);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('aportigall4@earthlink.net', '', 'Asa Portigall', 40, 'M', '327-06', 'D', 5);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('gaskwith5@theatlantic.com', '', 'Giavani Askwith', 70, 'M', '324-70', 'D', 6);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('lmcphillips6@tripadvisor.com', '', 'L;urette McPhillips', 14, 'F', '863-07', 'D', 7);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('bsabates7@blinklist.com', '', 'Bradly Sabates', 24, 'M', '704-81', 'A', 8);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('emushett8@chronoengine.com', '', 'Edwin Mushett', 68, 'M', '867-19', 'D', 9);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('cassaf9@intel.com', '', 'Charity Assaf', 75, 'F', '648-16', 'D', 10);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('ugullena@ed.gov', '', 'Urbanus Gullen', 82, 'M', '390-04', 'D', 11);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('dbulbeckb@nature.com', '', 'Darcie Bulbeck', 92, 'F', '438-26', 'D', 12);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('eingerithc@wisc.edu', '', 'Elwin Ingerith', 33, 'M', '282-85', 'D', 13);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('ddakhnod@networkadvertising.org', '', 'Daven Dakhno', 50, 'M', '138-91', 'A', 14);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('vclinkarde@constantcontact.com', '', 'Violette Clinkard', 87, 'F', '737-51', 'A', 15);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('pbottonf@liveinternet.ru', '', 'Paolo Botton', 34, 'M', '211-30', 'D', 16);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('rharremag@unc.edu', '', 'Ruthie Harrema', 81, 'F', '615-57', 'D', 17);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('gdeh@pen.io', '', 'Gayel De Freitas', 87, 'F', '564-91', 'D', 18);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('vflisheri@wisc.edu', '', 'Vernice Flisher', 60, 'F', '332-87', 'D', 19);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('nicomj@163.com', '', 'Neall Icom', 7, 'M', '835-1', 'D', 20);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('test@test.test', '098f6bcd4621d373cade4e832627b4f6', 'test', 23, 'M', '1234', 'A', 21);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('test2@test.test', '098f6bcd4621d373cade4e832627b4f6', 'test', 23, 'M', '1234', 'A', 22);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('entrenador@entrenador.entrenador', '098f6bcd4621d373cade4e832627b4f6', 'entrenador', 23, 'M', '1234', 'E', 23);

insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('nicomj1@163.com', '', 'Neall Icom', 7, 'M', '835-727', 'D', 24);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('nicomj2@163.com', '', 'Neall Icom', 7, 'M', '835-737', 'D', 25);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('nicomj3@163.com', '', 'Neall Icom', 7, 'M', '835-74347', 'D', 26);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('nicomj4@163.com', '', 'Neall Icom', 7, 'M', '835-7247', 'D', 27);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('nicomj5@163.com', '', 'Neall Icom', 7, 'M', '835', 'D', 28);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('nicomj6@163.com', '', 'Neall Icom', 7, 'M', '835-77', 'D', 29);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('nicomj7@163.com', '', 'Neall Icom', 7, 'M', '835-7', 'D', 30);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('nicomj8@163.com', '', 'Neall Icom', 7, 'M', '835-8', 'D', 31);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('nicomj9@163.com', '', 'Neall Icom', 7, 'M', '835-9', 'D', 32);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('nicomj10@163.com', '', 'Neall Icom', 7, 'M', '835-10', 'D', 33);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('nicomj11@163.com', '', 'Neall Icom', 7, 'M', '835-11', 'D', 34);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('nicomj12@163.com', '', 'Neall Icom', 7, 'M', '835-12', 'D', 35);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('nicomj13@163.com', '', 'Neall Icom', 7, 'M', '835-13', 'D', 36);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('nicomj14@163.com', '', 'Neall Icom', 7, 'M', '835-14', 'D', 37);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('nicomj15@163.com', '', 'Neall Icom', 7, 'M', '835-15', 'D', 38);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('nicomj16@163.com', '', 'Neall Icom', 7, 'M', '835-16', 'D', 39);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('nicomj17@163.com', '', 'Neall Icom', 7, 'M', '835-17', 'D', 40);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('nicomj18@163.com', '', 'Neall Icom', 7, 'M', '835-18', 'D', 41);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('nicomj19@163.com', '', 'Neall Icom', 7, 'M', '835-19', 'D', 42);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('nicomj20@163.com', '', 'Neall Icom', 7, 'M', '835-20', 'D', 43);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('nicomj21@163.com', '', 'Neall Icom', 7, 'M', '835-21', 'D', 44);
insert into USER (email, password, nombre, edad, genero, dni, tipo, id) values ('nicomj22@163.com', '', 'Neall Icom', 7, 'M', '835-22', 'D', 45);



DELETE FROM PAREJA;

insert into PAREJA values (1, 21, 19);
insert into PAREJA values (2, 22, 17);
insert into PAREJA values (3, 16, 15);
insert into PAREJA values (4, 14, 13);
insert into PAREJA values (5, 12, 11);
insert into PAREJA values (6, 10, 9);
insert into PAREJA values (7, 8, 7);
insert into PAREJA values (8, 6, 5);
insert into PAREJA values (9, 4, 3);
insert into PAREJA values (10, 2, 1);

INSERT INTO PAREJA values (11,18,20);
INSERT INTO PAREJA values (12,24,25);
INSERT INTO PAREJA values (13,26,27);
INSERT INTO PAREJA values (14,28,29);
INSERT INTO PAREJA values (15,30,31);
INSERT INTO PAREJA values (16,32,33);
INSERT INTO PAREJA values (17,34,35);
INSERT INTO PAREJA values (18,36,37);
INSERT INTO PAREJA values (19,38,39);
INSERT INTO PAREJA values (20,40,41);
INSERT INTO PAREJA values (21,42,43);
INSERT INTO PAREJA values (22,44,45);



DELETE FROM NIVEL;
insert into NIVEL values (1, 'M', 'Sexo masculino');
insert into NIVEL values (2, 'F', 'Sexo Femenino');
insert into NIVEL values (3, 'A', 'Mixto');

DELETE FROM CATEGORIA;
insert into CATEGORIA values (1, 'P', 'Para principiantes');
insert into CATEGORIA values (2, 'I', 'Para intermedio');
insert into CATEGORIA values (3, 'E', 'Para expertos');

DELETE FROM CAMPEONATO;
insert into CAMPEONATO values (1, 'Campeonato 1', '2019-01-01 12:00:00', '2020-01-01 12:00:00', 'La normativa', 1, 1);
insert into CAMPEONATO values (2, 'Campeonato 2', '2020-01-01 12:00:00', '2021-01-01 12:00:00', 'La normativa2', 2, 2);

DELETE FROM INSCRIPCIONCAMPEONATO;
INSERT INTO INSCRIPCIONCAMPEONATO VALUES(1,1,1);
INSERT INTO INSCRIPCIONCAMPEONATO VALUES(2,1,2);
INSERT INTO INSCRIPCIONCAMPEONATO VALUES(3,1,3);
INSERT INTO INSCRIPCIONCAMPEONATO VALUES(4,1,4);
INSERT INTO INSCRIPCIONCAMPEONATO VALUES(5,1,5);
INSERT INTO INSCRIPCIONCAMPEONATO VALUES(6,1,6);
INSERT INTO INSCRIPCIONCAMPEONATO VALUES(7,1,7);
INSERT INTO INSCRIPCIONCAMPEONATO VALUES(8,1,8);
INSERT INTO INSCRIPCIONCAMPEONATO VALUES(9,1,9);
INSERT INTO INSCRIPCIONCAMPEONATO VALUES(10,1,10);

INSERT INTO INSCRIPCIONCAMPEONATO VALUES(11,1,11);
INSERT INTO INSCRIPCIONCAMPEONATO VALUES(12,1,12);
INSERT INTO INSCRIPCIONCAMPEONATO VALUES(13,1,13);
INSERT INTO INSCRIPCIONCAMPEONATO VALUES(14,1,14);
INSERT INTO INSCRIPCIONCAMPEONATO VALUES(15,1,15);
INSERT INTO INSCRIPCIONCAMPEONATO VALUES(16,1,16);
INSERT INTO INSCRIPCIONCAMPEONATO VALUES(17,1,17);
INSERT INTO INSCRIPCIONCAMPEONATO VALUES(18,1,18);
INSERT INTO INSCRIPCIONCAMPEONATO VALUES(19,1,19);
INSERT INTO INSCRIPCIONCAMPEONATO VALUES(20,1,20);
INSERT INTO INSCRIPCIONCAMPEONATO VALUES(21,1,21);
INSERT INTO INSCRIPCIONCAMPEONATO VALUES(22,1,22);

DELETE FROM HORARIO;
INSERT INTO HORARIO VALUES (1, '0001-01-01 09:00:00', '0001-01-01 10:30:00');
INSERT INTO HORARIO VALUES (2, '0001-01-01 10:30:00', '0000-01-01 11:30:00');
INSERT INTO HORARIO VALUES (3, '0001-01-01 11:30:00', '0000-01-01 13:00:00');
INSERT INTO HORARIO VALUES (4, '0001-01-01 13:00:00', '0000-01-01 14:30:00');

DELETE FROM CONTENIDO;
insert into CONTENIDO values (0, 'Ejemplo de noticia', 'Este es un ejemplo de contenido creado para la web del club de padel.', '2020-01-20 12:00:00', 8);

DELETE FROM ESCUELA;
insert into ESCUELA values (1, 'Escuela1', '999999999', 'Direccion1');
insert into ESCUELA values (2, 'Escuela2', '999999998', 'Direccion2');
insert into ESCUELA values (3, 'Escuela3', '999999997', 'Direccion3');

DELETE FROM RESERVA;
insert into RESERVA values (1, '2020-01-25 00:00:00', '3', '1', '23', '1');

DELETE FROM CLASES;
insert into CLASES values (1, '1', '23', '1');

