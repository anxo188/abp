<html>
	<body>

	<?php
    require_once "Vistas/Menu.php";
    new Header();
	require_once "Funciones/isAdmin.php";
	require_once "Modelos/Horario_model.php";
?>


<legend class="inscripcionCampeonato text-center">Partidos activos</legend>
            <table class="table">
              <thead>
                  <tr>
                      <th  scope="col">Pista</th>
                      <th  scope="col">Dia</th>
					  <th scope="col"> Hora</th>
					  <th scope="col"> Inscritos</th>
                      <th  scope="col"></th>
                  </tr>
              </thead>
              <tbody>
                    <?php
                    
                      foreach($partidos as $p) {
						$inscritos = $p->getNumInscritos();
						console_log($p);
						if(in_array($p->id, $inscripciones_partido)){
							?>
                        <tr bgcolor="#AAFFAA">
							<?php
						}else{
						?>
                        <tr>
						<?php
							}
						?>
                            <td> <?php echo $p->reserva->pista->nombre; ?></td>
                            <td> <?php echo $p->reserva->dia->format("Y-m-d"); ?></td>
							<td> <?php echo Horario::get($p->reserva->horario);?></td>
							<td> <?php echo $inscritos; ?></td>
                            <td>
							<?php
								if($inscritos < 4 && !in_array($p->id, $inscripciones_partido)) {
							?>
								<span><a href="?controller=Partido&action=inscription&id=<?= $p->id ?>">Inscribirse</a></span>
							<?php
								}
							?>
							<?php
								if(in_array($p->id, $inscripciones_partido)) {
							?>
								<form method="post" action="?controller=Partido&action=unsubscribe&id=<?= $p->id?>">
								<input type="submit" value="Desinscribirse"/>
								</form>
							<?php
								}
								if(isAdmin()){
							?>
								<form method="post" action="?controller=Partido&action=delete&id=<?= $p->id ?>">
								<input type="submit" value="Borrar"/>
								</form>
								</p>
							<?php
								}		
							?>

							</td>
                              
                        </tr>
					<?php 
					}  
                    
                    ?>
                </tbody>
              </table>  
			  <p><a href="?controller=Partido&action=create">Crear partido</a></p>

	


	</body>
</html>
