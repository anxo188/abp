<?php
//session_start();  --> no hace falta xq ya lo has indicado en el controlador  y como todas las vistas se cargan en el controlador ya funcionan las sesiones



class Campeonato_Edit{
var $campeonatos;
var $nivel;
var $categoria;


  function __construct($campeonato,$nivel,$categoria){
    $this->campeonatos=$campeonato;
    $this->nivel=$nivel;
  	$this->categoria=$categoria;
    $this->render();
  }


 
  function render(){
	require_once "Vistas/Menu.php";
    new Header();
    $tupla =  mysqli_fetch_object($this->campeonatos);
  ?>

	<link rel="stylesheet" href="Vistas/formulario.css" type="text/css">  
	<div class="mainBox">
	<form id="formularioCampeonatoEdit" name = 'form' action='./?controller=CampeonatoGeneral&action=edit_Confirm&idCampeonato=<?php echo "$tupla->id"; ?>' method = 'post' enctype="multipart/form-data" onsubmit="">
		
			
				<label for="nombre">
					Nombre
				</label>
					<input type="text" id="nombre" name="nombre" value="<?php echo $tupla->nombre; ?>">
				<label for="inicio">
					Inicio
				</label>
					<input type="text" id="inicio" name="inicio" value="<?php echo $tupla->inicio; ?>" >
				<label for="fin">
					Fin
				</label>
					<input type="text" id="fin" name="fin" value="<?php echo $tupla->fin; ?>">
		
				<label for="normativa">Normativa</label>
        
              	<textarea name="normativa" id="field" size="256" onkeyup="countChar(this)" > <?php echo $tupla->normativa; ?> </textarea>
				  <label for="nivel">Nivel
				</label>
				<select id="nivel" class="roulete" name="nivel">
					

					 <?php while($tuplaNivel =  mysqli_fetch_object($this->nivel)){ 
						if($tupla->nivel == $tuplaNivel->id){?>

							<option value="<?php echo $tuplaNivel->id;?>" selected><?php echo $tuplaNivel->nombre; ?></option>
						<?php }else{?>

							<option value="<?php echo $tuplaNivel->id;?>"><?php echo $tuplaNivel->nombre; ?></option>
						
						<?php }
						 } ?>
					
				</select>
				<br>
			
				<label for="categoria">Categoria
				</label>
				<select id="categoria" class="roulete" name="categoria">
					

					 <?php while($tuplaCategoria =  mysqli_fetch_object($this->categoria)){ 
						if($tupla->categoria == $tuplaCategoria->id){?>

							<option value="<?php echo $tuplaCategoria->id;?>" selected><?php echo $tuplaCategoria->nombre; ?></option>
						<?php }else{?>

							<option value="<?php echo $tuplaCategoria->id;?>"><?php echo $tuplaCategoria->nombre; ?></option>
						
						<?php }
						 } ?>
					
				</select>
				<br>
			
				<button type="submit">Editar</button>
			
		<a role="button" href="./?controller=CampeonatoGeneral&action=list">Atrás</a>
		
		</form>
		</div>

<?php

  //include 'footer.php';
  } /*FIN RENDER*/

}   /*FIN CLASS*/

?>
