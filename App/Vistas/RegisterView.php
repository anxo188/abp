<?php 
  class Register{
    function __construct(){
      $this->render();
    }
    function render(){

?>
 <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>ABP_PADLE</title>
        <link rel="stylesheet" href="Vistas/login.css" type="text/css">
    </head>

    <body>
      
      <div class="grid-container">
        <div class="mainBox">
           
            <form action = "?controller=Register" method = 'post'>
              
                <label for="email">Email</label>
                <input type="email" name= "email" id="email" required=""placeholder="Email">
            
                <label for="dni">DNI</label>
                <input type="" name= "dni" id="dni" required=""  placeholder="DNI">
              
                <label for="pass">Password</label>
                <input type="password" name= "pass" id="pass" required="" placeholder="Pass">
              
                <label for="nombre">Nombre</label>
                <input type="" name= "nombre" id="nombre" required="" placeholder="Nombre">
              
                <label for="edad">Edad</label>
                <input type="number" name= "edad" id="edad" required="" placeholder="Edad">
             
                <label for="genero">Genero</label>
                <select name="genero" class="roulete" autofocus> 
                    <option value="M">Masculino</option> 
                    <option value="F">Femenino</option> 
                </select> 

              
                <label for="tipo">Tipo de usuario</label>
                <select  class="roulete" autofocus name="tipo"> 
                    <option value="D">Deportista</option> 
                    <option value="E">Entrenador</option> 
                </select> 
              
              <button type="submit">Registrar</button>
              <a href="./?controller=Login" class="btn btn-primary" role="button">Ya tienes una cuenta</a> 
            </form>
        </div>
        <div class="contenedorImagen">
          <img class="imagen" src="https://www.esade.edu/EsadeCMSView/image?imgId=12884939979">
        </div>
      </div>
    </body>

</html>
<?php    }//End render

  }//End class
?>
