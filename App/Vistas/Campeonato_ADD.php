<?php
//session_start();  --> no hace falta xq ya lo has indicado en el controlador  y como todas las vistas se cargan en el controlador ya funcionan las sesiones



class Campeonato_Add{


  function __construct(){

    $this->render();
  }


 
  function render(){
    require_once "Vistas/Menu.php";
    new Header();
  ?>
	 <link rel="stylesheet" href="Vistas/formulario.css" type="text/css">
      
	<div class="mainBox">
        
	<form id="formularioCampeonatoADD" name = 'form' action='./?controller=CampeonatoGeneral&action=add_Confirm' method = 'post' enctype="multipart/form-data" onsubmit="">
		
			
				<label for="nombre">
					Nombre
				</label>
				<input type="text" id="nombre" name="nombre">
				<label for="inicio">Inicio</label>
              	<input type="" name= "inicio" id="inicio" required="" placeholder="Formato YYYY-MM-DD HH:MM:SS">
            
				<label for="fin">Fin</label>
				<input type="" name= "fin" id="fin" required="" placeholder="Formato YYYY-MM-DD HH:MM:SS">
				<label for="normativa">Normativa</label>          
            	<textarea name="normativa" id="field" size="256" onkeyup="countChar(this)"> </textarea>
				<br>
				<button type="submit">Añadir</button>
			
		<a role="button" href="./?controller=CampeonatoGeneral&action=list">Atrás</a>
		</form>
		</div>

		<script src="http://code.jquery.com/jquery-1.5.js"></script>
    <script>
      function countChar(val) {
        var len = val.value.length;
        if (len >= 65535) {
          val.value = val.value.substring(0, 65535);
        } else {
          $('#charNum').text(65535 - len);
        }
      };
    </script>

<?php

  //include 'footer.php';
  } /*FIN RENDER*/

}   /*FIN CLASS*/

?>
