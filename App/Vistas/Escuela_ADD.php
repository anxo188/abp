<?php
//session_start();  --> no hace falta xq ya lo has indicado en el controlador  y como todas las vistas se cargan en el controlador ya funcionan las sesiones



class Escuela_Add{

  function __construct(){

    $this->render();
  }


 
  function render(){
    require_once "Vistas/Menu.php";
    new Header();
  ?>
	 <link rel="stylesheet" href="Vistas/formulario.css" type="text/css">
      
	<div class="mainBox">
        
	<form id="formularioEscuelaADD" name = 'form' action='./?controller=EscuelaGeneral&action=add_Confirm' method = 'post' enctype="multipart/form-data" onsubmit="">
		
			
				<label for="nombre">
					Nombre
				</label>
				<input type="text" id="nombre" name="nombre">
				<label for="telefono">Telefono</label>
              	<input type="text" name= "telefono" id="Telefono" required="" placeholder="9 digitos sin espacios">
            
				<label for="direccion">Direccion</label>
				<input type="text" name= "direccion" id="Direccion" required="">
				<br>
				<button type="submit">Añadir</button>
			
		<a role="button" href="./?controller=EscuelaGeneral&action=list">Atrás</a>
		</form>
		</div>

<?php

  //include 'footer.php';
  } /*FIN RENDER*/

}   /*FIN CLASS*/

?>
