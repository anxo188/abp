<?php


class CampeonatoInscripcion_Pareja{
	var $usuarios;
  var $idCampeonato;

  function __construct($usuarios,$idCampeonato){
    $this->usuarios=$usuarios;
    $this->idCampeonato=$idCampeonato;
    $this->render();
  }


 
  function render(){
    require_once "Vistas/Menu.php";
    new Header();
  ?>

<div> 

                               
  <fieldset>
              <legend class="inscripcionCampeonato text-center">Usuarios disponibles</legend>

                <table class="table">
                    <thead>
                        <tr>

                            <th scope="col">Nombre</th>
                            <th scope="col">Edad</th>
                            <th scope="col">Genero</th>
                            <th ></th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        if($this->usuarios != false){
                           while($tupla =  mysqli_fetch_object($this->usuarios)){ ?>
                            <tr>
                                <td> <?php echo $tupla->nombre; ?></td>
                                <td> <?php echo $tupla->edad; ?></td>
                                <td> <?php echo $tupla->genero; ?></td>
                                <td>
                                    <a href="./?controller=CampeonatoInscription&action=add&idCampeonato=<?php echo $this->idCampeonato; ?>&idAcomp=<?php echo $tupla->id; ?>" />seleccionar pareja</a>
                                </td>
                            </tr>
                        <?php }  
                        }
                         ?>
                    </tbody>
                </table>

            </fieldset>
            <a role="button" class="btn btn-primary " href="javascript:history.go(-1)">Atras</a>
</div>

</body>


<?php

  //include 'footer.php';
  } /*FIN RENDER*/

}   /*FIN CLASS*/

?>
