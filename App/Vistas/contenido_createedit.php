<?php


class ContenidoCreateEdit{
  var $contenido;
  var $user;
  function __construct($user, $contenido = NULL){
    $this->contenido = $contenido;
    $this->user = $user;
    $this->render();
  }


 
  function render(){
    require_once "Vistas/Menu.php";
    new Header();
    require_once "Funciones/isAdmin.php";
		
  ?>

  <body>
<?php
    if($this->contenido == NULL) {
        echo "<h1>Crear noticia</h1>";
    } else {
        echo "<h1>Editar noticia</h1>";
    }
?>
    
    <div>
    <form action="/?controller=Front&action=confirm" method="POST">
<?php
    if($this->contenido != NULL) {
?>
    <input type="hidden" name="id" value="<?= $this->contenido->id ?>"/>
<?php
    }
?>
    <input type="text" name="title" placeholder="Titulo" value="<?= $this->contenido != NULL ? $this->contenido->titulo : "" ?>"> </br>
    <textarea name="text" placeholder="Texto"><?php
        if($this->contenido != NULL) {
            echo $this->contenido->texto;
        }
?></textarea> </br>
    <input type="submit" value="Enviar">
    </form>
    </div>
    </body>
</html>
  <?php }
} ?>
