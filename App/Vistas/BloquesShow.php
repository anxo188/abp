<?php


class BloquesShowView{
	var $bloques;


  function __construct($bloques){
    $this->bloques=$bloques;
    $this->render();
  }


 
  function render(){
    require_once "Modelos/ParejasBloque_Model.php";
    require_once "Vistas/Menu.php";
    new Header();
    require_once "Funciones/isAdmin.php";
		
  ?>
    <body>
      <div>                     
       
          <legend class="inscripcionCampeonato text-center">Bloques del campeonato</legend>
            <table class="table">
                <thead>
                    <tr>
                        <th  scope="col">Id</th>
                        <th  scope="col">Número de Parejas</th>
                        <th  scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($this->bloques != false){
                    while( $bloque = mysqli_fetch_object($this->bloques) ){ 
                       
                        if(isset($_SESSION['userid']) && ParejaBloque::pertenece($bloque->id,$_SESSION['userid'])){
                          ?>
                            <tr bgcolor="#AAFFAA">
                          <?php

                        }else{
                          ?>
                            <tr>
                          <?php
                        }
                        ?>
                            <td> <?php echo $bloque->id; ?></td>
                            <td> <?php echo $bloque->numParejas; ?></td>
                            
                                <td>
                                    <a role="button" href="/?controller=GestorLiga&action=showBlockMatches&idBloque=<?php echo $bloque->id;  ?>" class="btn btn-primary " >Ver Enfrentamientos </a> 
                                </td>
                                <td>
                                  <a role="button" href="/?controller=GestorLiga&action=showOwnBlockMatches&idBloque=<?php echo $bloque->id;  ?>" class="btn btn-primary " >Ver Solo Enfrentamientos Propios</a> 
                                </td>
                               <td>
                                <a role="button" href="/?controller=GestorLiga&action=showBlockRanking&idBloque=<?php echo $bloque->id;  ?>" class="btn btn-primary " >Ver Ranking </a> 
                               </td>
                        </tr>
                    <?php }  
                    }
                    ?>
                </tbody>
            </table>
            <a role="button" class="btn btn-primary " href="./?controller=CampeonatoGeneral&action=list">Atrás</a>
      </div>
    </body>
</html>

<?php

  //include 'footer.php';
  } /*FIN RENDER*/

}   /*FIN CLASS*/

?>
