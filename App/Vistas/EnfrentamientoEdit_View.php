<?php


class EnfrentamientoEdit{
	


  function __construct($enfrentamiento){
    
    $this->render($enfrentamiento);
  }


 
  function render($enfrentamiento){
    require_once "Vistas/Menu.php";
    require_once "Modelos/User_Model.php";
    require_once "Modelos/Enfrentamiento_Model.php";
			require_once "Modelos/Pareja_Model.php";
    new Header();
  ?>
    <body>
    <link rel="stylesheet" href="Vistas/formulario.css" type="text/css">
      
      <div class="mainBox">
        
          <form action = '?controller=GestorLiga&action=editEnfrentamiento&idEnfrentamiento=<?php echo $enfrentamiento->id ?>' method = 'post'>
            
              <label for="res1">Pareja: <?php  $parejaNom = Pareja_Model::get($enfrentamiento->pareja1);
              $capitan = mysqli_fetch_object($parejaNom)->capitan;
              $user = USUARIOS_Model::get($capitan);
              $user = mysqli_fetch_object($user);
        echo $user->nombre;?></label>
              <input type="" name= "res1" id="res1" required="" value="<?php echo $enfrentamiento->res1?>">
            
              <label for="res2">Pareja: <?php
              $parejaNom = Pareja_Model::get($enfrentamiento->pareja2);
              $capitan = mysqli_fetch_object($parejaNom)->capitan;
              $user = USUARIOS_Model::get($capitan);
              $user = mysqli_fetch_object($user);
        echo $user->nombre;?></label>
              <input type="" name= "res2" id="res2" required="" value="<?php echo $enfrentamiento->res2?>">
            
            <button type="submit">Edit</button>
            <a role="button" class="btn btn-primary " href="javascript:history.go(-1)">Atras</a>
          </form> 
      </div>
    </body>

<?php

  //include 'footer.php';
  } /*FIN RENDER*/

}   /*FIN CLASS*/

?>
