<?php


class Escuela_Listado{
	var $escuelas;


  function __construct($escuelas){
    $this->escuelas=$escuelas;
    $this->render();
  }


 
  function render(){
    require_once "Vistas/Menu.php";
    new Header();
  ?>

<div>





                             
  <fieldset>
              <legend class="inscripcionCampeonato text-center">Escuelas</legend>

                <table class="table">
                    <thead>
                        <tr>

                            <th scope="col">Nombre</th>
                            <th scope="col">Telefono</th>
                            <th scope="col">Direccion</th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        if($this->escuelas != false){
                           while($tupla =  mysqli_fetch_object($this->escuelas)){ ?>
                            <tr>
                                <td> <?php echo $tupla->nombre; ?></td>
                                <td> <?php echo $tupla->telefono; ?></td>
                                <td> <?php echo $tupla->direccion; ?></td>
                                <?php
                                    if(isadmin()){
                                ?>
                                <td>
                                    <a href="./?controller=EscuelaGeneral&action=delete&idEscuela=<?php echo $tupla->id; ?>" >Eliminar</a>
                                </td>
                                <td>
                                    <a href="./?controller=EscuelaGeneral&action=edit&idEscuela=<?php echo $tupla->id; ?>" >Editar</a>
                                </td>
                                <?php
                                }
                                ?>
                                <td>
                                    <a href="./?controller=ClaseGeneral&action=list&idEscuela=<?php echo $tupla->id; ?>" >Ver Clases</a>
                                </td>
                            </tr>
                        <?php }  
                        }
                         ?>
                    </tbody>
                </table>

            </fieldset>
            <div>
            <?php
            if(isadmin()){
            ?>
        
            <a role="button" href="./?controller=EscuelaGeneral&action=add" class="btn btn-primary " > Añadir Escuela </a> 
       
            <?php
              }
              ?>
     

        
            <?php
                if(isentrenador()){
            ?>
                <a role="button" href="./?controller=ClaseGeneral&action=add" class="btn btn-primary " > Añadir clase </a> 
                <a role="button" href="./?controller=ClaseGeneral&action=list_Clases" class="btn btn-primary " > Ver las clases que imparto </a> 
      
            <?php
              }
              ?>
            </div>
</div>

</body>


<?php

  //include 'footer.php';
  } /*FIN RENDER*/

}   /*FIN CLASS*/

?>
