
<?php
	class Header{
		function __construct(){
			$this->render();
		}

		function render(){
			
			?>
		<html lang="en">
			<head>
				<meta charset="UTF-8">
						<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
						<link rel="stylesheet" href="Vistas/header.css" type="text/css">
						<!-- jQuery first, then Popper.js, then Bootstrap JS -->
						<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
						<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
						<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

						<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
						<title>ABP_PADLE</title>
						
						<nav id="navBar" class="navbar navbar-expand-lg navbar-light">
							<a class="navbar-brand text-white" href="/?controller=Front">PadelDie</a>
							<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
								<span class="navbar-toggler-icon"></span>
							</button>

							<div class="collapse navbar-collapse" id="navbarSupportedContent">
								<ul class="navbar-nav mr-auto">
									<li class="nav-item active">
										<a class="nav-link text-white" href="./?controller=Pista&action=list">Pistas</a>
									</li>
									<?php
										if(isset($_SESSION['userid'])){
									?>
									<li class="nav-item active">
										<a class="nav-link text-white" href="./?controller=Reserva&action=list">Reservas</a>
									</li>
									<?php
										}
									?>
									<?php
										if(isset($_SESSION['userid'])){
									?>
									<li class="nav-item active">
										<a class="nav-link text-white" href="/?controller=Partido&action=list">Partidos</a>
									</li>
									
									<?php
										}
									?>
									<li class="nav-item active">
										<a class="nav-link text-white" href="./?controller=CampeonatoGeneral&action=list">Campeonatos</a>
									</li>
									
									<?php
										if(isset($_SESSION['userid'])){
									?>
									<li class="nav-item active">
										<a class="nav-link text-white" href="./?controller=EscuelaGeneral&action=list">Escuelas</a>
									</li>
									<?php
										}
									?>
								</ul>
								<?php
									if(isset($_SESSION['userid'])){
								?>
										<div class="dropdown UserMenu">
											<button class="btn btn-secondary dropdown-toggle text-white" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<?php echo $_SESSION['userName']; ?>
											</button>
											<div  class="dropdown-menu UserMenu" aria-labelledby="dropdownMenuButton">
												<a class="dropdown-item" href="?action=logout">Logout</a>
												<a class="dropdown-item" href="#">Another action</a>
												<a class="dropdown-item" href="#">Something else here</a>
											</div>
										</div>
								<?php
									}else{
										?>
										<div class="UserZone">
											<a id="login" class="text-white" href="/?controller=Login">Login</a>
											<a id="signIn"   class="text-white" href="/?controller=Register">Sign-in</a>
										<div>
								<?php
									}
								?>
								
							</div>
						</nav>
			</head>
<?php
		
		}
	}
?>

