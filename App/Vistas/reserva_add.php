<?php 
  class ReservaAdd{
    var $pistas;
    var $schedule;
    function __construct($pistas,$schedule){
      $this->pistas=$pistas;
      $this->schedule=$schedule;
      $this->render();
    }
    function render(){
      require_once "Modelos/Horario_model.php";
      require_once "Vistas/Menu.php";
      new Header();
      
?>
    <body>
    <link rel="stylesheet" href="Vistas/formulario.css" type="text/css">
    <link rel="stylesheet" href="Vistas/reservas.css" type="text/css">
      <div class="mainBox">
        
          <form action = '?controller=Reserva&action=add_confirm' method = 'post'>
            
              <label for="pista">Pista</label>
              <select name="pista" class="roulete">
                <?php
                
                  foreach($this->pistas as $p) {
                ?>
                  <option value="<?=$p->id?>"><?=$p->nombre?></option>
                <?php
                  }
                ?>
              </select>
              <label for="horaVisual">Hora seleccionada</label>
              <input name="selected" id="selected"  value="nada" type="hidden" readonly/> 
              <input name="horaVisual" id="horaVisual" value="nada" readonly />
              <label for="dia">Dia</label>
              <input name="dia" id="day" value="nada" readonly />
              <input name="diaData" id="diaData"  type="hidden" readonly />
              <?php
              
                $days = ["Lunes","Martes","Miércoles","Jueves","Viernes","Sábado","Domingo"];
                $dayNumber = jddayofweek(0);      
        
              ?>
          


              <div class="gridMain">
            
               
                <?php 
                  for ($i = 0; $i < 7; $i++) {
                ?>
                  <table class="<?php echo "day".$i+1; ?>">
                    <thead>
                    <tr bgcolor="#FFFFFF">
                      <th><?php echo $days[(($dayNumber+$i)%7)];?></th>
                    </tr>
                  </thead>
                    <tbody>
                      <?php 
                        $horarios = Horario::getHorarios();
                      
                        if(isset($this->schedule)){
                          $reservasDelDia = $this->schedule[$i];
                                      
                          while($reserva = mysqli_fetch_object($reservasDelDia)){
                            $aux[$reserva->horario] = $days[(($dayNumber+$i)%7)];
                          }
                        }
                        if(!isset($aux)){
                          $aux["nada"]=1;
                        }
                        while($horario = mysqli_fetch_object($horarios)){
                          $dateI = explode(" ",$horario->horaInicio);
                          $dateI = $dateI[1];
                          $dateF = explode(" ",$horario->horaFin);
                          $dateF = $dateF[1];
                      
                        
                          if(array_key_exists($horario->id,$aux) && $aux[$horario->id] == $days[(($dayNumber+$i)%7)]){
                        ?>
                            <tr >
                              <td>
                                <button type="button" style="background-color:#FF0000;font-size:0.5vw;" disabled><?php echo $dateI." - ".$dateF; ?></button>
                              </td>
                            </tr>
                          <?php
                          }else{
                          ?>
                            <tr>
                              <td>
                              <button type="button" style="font-size:0.5vw;" onclick="document.getElementById('selected').value='<?php echo $horario->id ?>'
                                    document.getElementById('day').value='<?php echo  $days[(($dayNumber+$i)%7)];?>'
                                    document.getElementById('diaData').value='<?php echo $i;?>'
                                    document.getElementById('horaVisual').value='<?php echo $dateI.' - '.$dateF;?>'" ><?php echo $dateI." - ".$dateF;?></button>
                              </td>
                            </tr>
                          <?php
                          }
                          ?>
                      <?php 
                        }
                      ?>
                    </tbody>
                  </table>
                <?php
                  }
                ?>
          </div>

              
              
            <button type="submit">Reservar</button>

            <a role="button" href="/?controller=Reserva&action=list">Atrás</a>
          </form> 
      </div>
      
    </body>
</html>
<?php    }//End render

  }//End class
?>

