<?php 
  class PistaAdd{
    function __construct(){
      $this->render();
    }
    function render(){
      require_once "Vistas/Menu.php";
      new Header();
?>
 <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>ABP_PADLE</title>
    </head>

    <body>
    <link rel="stylesheet" href="Vistas/formulario.css" type="text/css">
      
      <div class="mainBox">
        
          <form action = '?controller=Pista&action=add' method = 'post'>
            
              <label for="nombre">Nombre</label>
              <input type="" name= "nombre" id="nombre" required="" placeholder="Nombre">
            
              <label for="inicio">Inicio</label>
              <input type="" name= "inicio" id="inicio" required="" placeholder="Formato YYYY-MM-DD HH:MM:SS">
            
              <label for="fin">Fin</label>
              <input type="" name= "fin" id="fin" required="" placeholder="Formato YYYY-MM-DD HH:MM:SS">
        
              <label for="descripcion">Descripcion</label>          
              <textarea name="descripcion" id="field" size="256" onkeyup="countChar(this)"> </textarea>
              <div id="charNum"></div>

            <button type="submit">Registrar Pista</button>

            <a role="button" href="/?controller=Pista&action=list">Atrás</a>
          </form> 
      </div>
    </body>
     
    <script src="http://code.jquery.com/jquery-1.5.js"></script>
    <script>
      function countChar(val) {
        var len = val.value.length;
        if (len >= 255) {
          val.value = val.value.substring(0, 255);
        } else {
          $('#charNum').text(255 - len);
        }
      };
    </script>
</html>
<?php    }//End render

  }//End class
?>
