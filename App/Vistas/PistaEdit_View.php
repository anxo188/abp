<?php


class PistaEdit{
	var $pista;


  function __construct($pista){
    $this->pista=$pista;
    $this->render();
  }


 
  function render(){
    require_once "Vistas/Menu.php";
    new Header();
    require_once "Funciones/isAdmin.php";
  ?>
    <body>
    <link rel="stylesheet" href="Vistas/formulario.css" type="text/css">
      
      <div class="mainBox">
        
          <form action = '?controller=Pista&action=edit' method = 'post'>
            
              <label for="nombre">Nombre</label>
              <input type="" name= "nombre" id="nombre" required="" value="<?php echo $this->pista->nombre?>">
            
              <label for="inicio">Inicio</label>
              <input type="" name= "inicio" id="inicio" required="" value="<?php echo $this->pista->inicio?>">
            
              <label for="fin">Fin</label>
              <input type="" name= "fin" id="fin" required="" value="<?php echo $this->pista->fin?>">
                 
              <label for="descripcion">Descripcion</label>          
              <textarea name="descripcion" id="field" size="256" onkeyup="countChar(this)"><?php echo $this->pista->descripcion?> </textarea>
              <div id="charNum"></div>
            
                <input type="hidden" name="id" id="id" value="<?php echo $this->pista->id ?>"/>

            <button type="submit">Edit</button>
            <a role="button" class="btn btn-primary " href="./?controller=Pista&action=list">Atras</a>
          </form> 
      </div>
    </body>
    <script src="http://code.jquery.com/jquery-1.5.js"></script>
    <script>
      function countChar(val) {
        var len = val.value.length;
        if (len >= 255) {
          val.value = val.value.substring(0, 255);
        } else {
          $('#charNum').text(255 - len);
        }
      };
    </script>
<?php

  //include 'footer.php';
  } /*FIN RENDER*/

}   /*FIN CLASS*/

?>
