<?php
require_once "Vistas/Menu.php";
new Header();
require_once "Funciones/isAdmin.php";
?>

<html>
<body>
<h1>Eliminar reserva</h1>
<div>
<form action="?controller=Reserva&action=delete_confirm&id=<?=$id?>" method="post">
<p>
Desea eliminar la reserva en la pista
    <?= $reserva->pista->nombre . " " ?>
 el día <?= $reserva->dia->format("Y m d") . " " ?>
a las <?= $reserva->dia->format("H:i") ?>
?
</p>
<input type="submit" value="Enviar"/>
<a href="?controller=Reserva&action=list">Cancelar</a>
</form>
</div>
</body>
</html>
