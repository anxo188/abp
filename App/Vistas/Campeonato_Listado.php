<?php


class Campeonato_Listado{
	var $campeonatos;
 	var $fechaActual;

  function __construct($campeonatos,$fechaActual){
    $this->campeonatos=$campeonatos;
    $this->fechaActual=$fechaActual;
    $this->render();
  }


 
  function render(){
    require_once "Modelos/Bloque_Model.php";
    require_once "Vistas/Menu.php";
    new Header();
  ?>

<div>





                             
  <fieldset>
              <legend class="inscripcionCampeonato text-center">Campeonatos activos</legend>

                <table class="table">
                    <thead>
                        <tr>

                            <th scope="col">Nombre</th>
                            <th scope="col">Inicio</th>
                            <th scope="col">Fin</th>
                            <th scope="col">Normativa</th>
                            <th scope="col">Nivel</th>
                            <th scope="col">Categoria</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        if($this->campeonatos != false){
                           while($tupla =  mysqli_fetch_object($this->campeonatos)){ ?>
                            <tr>
                                <td> <?php echo $tupla->nombre; ?></td>
                                <td> <?php echo $tupla->inicio; ?></td>
                                <td> <?php echo $tupla->fin; ?></td>
                                <td> <?php echo $tupla->normativa; ?></td>
                                <td> <?php echo $tupla->nivel; ?></td>
                                <td> <?php echo $tupla->categoria; ?></td>
                                <?php
                                    if(isadmin()){
                                ?>
                                <td>
                                    <a href="./?controller=CampeonatoGeneral&action=delete&idCampeonato=<?php echo $tupla->id; ?>" >Eliminar</a>
                                </td>
                                <td>
                                    <a href="./?controller=CampeonatoGeneral&action=edit&idCampeonato=<?php echo $tupla->id; ?>" >Editar</a>
                                </td>
                                <?php
                                if(Bloque_Model::getAll($tupla->id) == false){
                                  ?>
                                    <td>
                                      <a role="button" href="./?controller=GestorLiga&action=closeInscriptions&idCampeonato=<?php echo $tupla->id ?>" > Comenzar Liga </a> 
                                    </td>
                                  <?php
                                } ?>
                                <?php
                                }
                                ?>
                                <td>
                                  <a href="./?controller=GestorLiga&action=showBlocks&idCampeonato=<?php echo $tupla->id; ?>" >Ver Bloques</a>
                                </td>
                                <?php if($this->fechaActual<=$tupla->inicio && isset($_SESSION['userid'])  ){ ?>
                                <td>
                                    <a href="./?controller=CampeonatoInscription&action=seleccionarPareja&idCampeonato=<?php echo $tupla->id; ?>" />Inscribirse</a>
                                </td>
                              <?php }else{?>
                              	<td></td>
                             <?php } ?>
                            </tr>
                        <?php }  
                        }
                         ?>
                    </tbody>
                </table>

            </fieldset>
           
           <div> 
           	<?php
            if(isadmin()){
        	?>

               <a role="button" href="./?controller=CampeonatoGeneral&action=add" class="btn btn-primary " > Añadir Campeonato </a> 
        	
        	<?php  
      		} 
      		if(isset($_SESSION['userid'])){
      		?>
              <a role="button" href="./?controller=CampeonatoInscription&action=list" class="btn btn-primary " > Ver mis campeonatos </a>
             <?php 
             }
             ?>  
     	   </div>
 


</div>

</body>


<?php

  //include 'footer.php';
  } /*FIN RENDER*/

}   /*FIN CLASS*/

?>
