<?php  
	class ShowMatchs{
		function __construct($parejas){
			$this->render($parejas);
		}
		function render($parejas){
            require_once 'Modelos/Enfrentamiento_Model.php';
            require_once 'Modelos/Pareja_Model.php';
            require_once "Modelos/User_Model.php";
            require_once "Vistas/Menu.php";
            new Header();
?>
                <body>
      <div>                     
       
          <legend class="inscripcionCampeonato text-center">Enfrentamientos</legend>
            <table class="table">
                <thead>
                    <tr>
                        <th  scope="col">Capitán pareja A</th>
                        <th  scope="col">Capitán pareja B</th>
                        <th  scope="col">Resultado Pareja A</th>
                        <th  scope="col">Resultado Pareja B</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        while($pareja = mysqli_fetch_object($parejas)){
                            
                            
                            $parejaid = Enfrentamiento::get($pareja->idEnfrentamiento);
                           
                            if(isset($_SESSION['userid']) && 
                            (Pareja_Model::pertenece($parejaid->pareja1,$_SESSION['userid']) || Pareja_Model::pertenece($parejaid->pareja2,$_SESSION['userid'])) ){
                                ?>
                                <tr bgcolor="#AAFFAA">
                              <?php
    
                            }else{
                              ?>
                                <tr>
                              <?php
                            }
                            ?>
                                <?php 
                                    if(Enfrentamiento::ganado($pareja->idEnfrentamiento, $parejaid->pareja1)){
                                    ?>
                                     <th bgcolor="#04871e" scope="col">
                                    
                                    <?php }else if(Enfrentamiento::jugado($pareja->idEnfrentamiento, $parejaid->pareja1)){
                                        ?>
                                        <th bgcolor="#d41414" scope="col">
                                        <?php
                                    }else{
                                        ?>
                                        <th  scope="col">
                                        <?php
                                    }
                                    $parejaNom = Pareja_Model::get($parejaid->pareja1);
                                    $capitan = mysqli_fetch_object($parejaNom)->capitan;
                                    $user = USUARIOS_Model::get($capitan);
                                    $user = mysqli_fetch_object($user);
			                        echo $user->nombre;
                                    ?> </th>
                               <?php 
                                    if(Enfrentamiento::ganado($pareja->idEnfrentamiento, $parejaid->pareja2)){
                                        ?>
                                     <th bgcolor="#04871e" scope="col">
                                    
                                    <?php }else if(Enfrentamiento::jugado($pareja->idEnfrentamiento, $parejaid->pareja2)){
                                        ?>
                                        <th bgcolor="#d41414" scope="col">
                                        <?php
                                    }else{
                                        ?>
                                        <th  scope="col">
                                        <?php
                                    }
                                   
                                    $parejaNom = Pareja_Model::get($parejaid->pareja2);
                                    $capitan = mysqli_fetch_object($parejaNom)->capitan;
                                    $user = USUARIOS_Model::get($capitan);
                                    $user = mysqli_fetch_object($user);
			                        echo $user->nombre;

                                    ?> </th>
                                <th scope="col"><?php echo $parejaid->res1; ?> </th>
                                <th scope="col"><?php echo $parejaid->res2; ?> </th>
                                <th scope="col">
                                    <?php 
                                        if(isadmin()){
                                        ?>
                                            <a role="button" href="/?controller=GestorLiga&action=editEnfrentamiento&idEnfrentamiento=<?php echo $pareja->idEnfrentamiento; ?>" class="btn btn-primary " > Editar resultados</a>     
                                        <?php
                                        }
                                        if(isset($_SESSION['userid']) && 
                                            (Pareja_Model::pertenece($parejaid->pareja1,$_SESSION['userid']) || Pareja_Model::pertenece($parejaid->pareja2,$_SESSION['userid'])) ){
                                        ?>
                                            <a role="button" href="/?controller=Chat&action=sendFirst&enfrentamientoId=<?php echo $pareja->idEnfrentamiento;?>" class="btn btn-primary " > Concertar enfrentamiento </a>
                                        <?php
                                        }
                                    ?>   
                                </th>
                            </tr>
                    <?php
                        }

                    ?>
                </tbody>
            </table>
            <a role="button" class="btn btn-primary " href="javascript:history.go(-1)">Atras</a>
      </div>
    </body>
</html>
<?php }
}?>