<?php


class Gestor{
	var $campeonato;


  function __construct($campeonato){
    $this->campeonato = $campeonato;
    $this->render();
  }

  function render(){
    require_once "Vistas/Menu.php";
    new Header();
    $camp = mysqli_fetch_object($this->campeonato);
    console_log($camp);
?>
    <legend class="inscripcionCampeonato text-center"><?php echo $camp->nombre ?></legend>
    <div class="mainBox">
        <div>
            <a role="button" href="./?controller=GestorLiga&action=closeInscriptions&idCampeonato=<?php echo $camp->id ?>" class="btn btn-primary " > Comenzar Liga </a> 
        </div>
        <div>
            <a role="button" href="./?controller=GestorLiga&action=showBlocks&idCampeonato=<?php echo $camp->id ?>" class="btn btn-primary " > Mostrar Bloques</a> 
        </div>
    </div>

<?php
  }
}
?>