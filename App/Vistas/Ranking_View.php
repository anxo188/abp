<?php  
	class Ranking{
		function __construct($parejas){
			$this->render($parejas);
		}
		function render($parejas){
            require_once 'Modelos/Enfrentamiento_Model.php';
            require_once 'Modelos/Pareja_Model.php';
            require_once "Vistas/Menu.php";
            require_once "Modelos/User_Model.php";
            new Header();
?>
                <body>
      <div>                     
       
          <legend class="inscripcionCampeonato text-center">Ranking del bloque</legend>
            <table class="table">
                <thead>
                    <tr>
                        <th  scope="col">Posición</th>
                        <th  scope="col">Capitán de la pareja</th>
                        <th  scope="col">Puntuacion</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $index = 0;
                        $empty = empty($parejas);
                       
                        while($index<10 && $empty == false){
                            end($parejas);
                            $idPareja = key($parejas);
                            $score = array_pop($parejas);
                            $index++;
                            if($score == null){
                                $empty = true;
                            }else{
                                if(isset($_SESSION['userid']) && Pareja_Model::pertenece($idPareja,$_SESSION['userid']) ){
                                    ?>
                                    <tr bgcolor="#AAFFAA">
                                <?php
        
                                }else{
                                ?>
                                    <tr>
                                <?php
                                }
                                ?>
                                    <th scope="col"><?php echo $index; ?> </th>
                                    <th scope="col"><?php 
                                    $parejaNom = Pareja_Model::get( $idPareja);
                                    $capitan = mysqli_fetch_object($parejaNom)->capitan;
                                    $user = USUARIOS_Model::get($capitan);
                                    $user = mysqli_fetch_object($user);
			                        echo $user->nombre;
                                    ?> </th>
                                    <th scope="col"><?php 
                                    if($score < 0){
                                        $score = 0;
                                    }
                                    echo $score; ?> </th>
                                </tr>
                    <?php
                             }
                        }

                    ?>
                </tbody>
            </table>
            <a role="button" class="btn btn-primary " href="javascript:history.go(-1)">Atras</a>
      </div>
    </body>
</html>
<?php }
}?>