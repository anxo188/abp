<?php
require_once "Vistas/Menu.php";
    new Header();
    require_once "Funciones/isAdmin.php";
?>

<html>
	<body>
	<h1>Playoff</h1>
	
	<h3><?= $campeonato->nombre ?></h3>
	<h4>Normativa</h4>
	<p><?= $campeonato->normativa ?></p>
<?php

    $fases = array("Cuartos de final", "Semifinales", "Final");
    $max_fase = 0;
    if($playoff == FALSE) {
?>
    <p>No hay un playoff creado. <a href="?controller=Playoff&action=create&campeonato=<?= $campeonato->id ?>">Crear</a>.</p>
<?php
    } else {
        foreach($playoff->enfrentamientos as $e) {
            $max_fase = $e['fase'] > $max_fase? $e['fase'] : $max_fase;
    ?>
        <p>
        <span><?=$fases[$e['fase']] ?></span> <span><?= $e['reserva']->hora->format("Y-m-d H:i:s") ?> </span><br/>
        <span><?=$e['reserva']->pista->nombre ?></span> </br>
        <span><?=$e['p1'] ?> - <?= $e['r1'] ?></span></br>
        <span><?=$e['p2'] ?> - <?= $e['r2'] ?></span>
        <form action="?controller=Playoff&action=result&campeonato=<?= $campeonato->id ?>&enfrentamiento=<?= $e['eid'] ?>" method="post">
            <input type="submit" value="Insertar resultado" />
        </form>
        </p>
<?php
        }
        
        if($max_fase < 2) {
?>
        <form action="?controller=Playoff&action=create_phase&campeonato=<?=$campeonato->id ?>" method="post">
            <input type="submit" value="Crear fase" />
        </form>
<?php
        }
    }
?>
	</body>
</html>
