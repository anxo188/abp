<?php


class ContenidoDelete{
  var $contenido;
  var $user;
  function __construct($contenido, $user){
    $this->contenido = $contenido;
    $this->user = $user;
    $this->render();
  }


 
  function render(){
    require_once "Vistas/Menu.php";
    new Header();
    require_once "Funciones/isAdmin.php";
		
  ?>

  <body>
    <h1>Eliminar noticia</h1>
    
    <div>
    
    <form action="/?controller=Front&action=delete_confirm" method="POST">
    
    <p>¿Desea eliminar la noticia "<?= $this->contenido->titulo ?>" publicada el <?= $this->contenido->creacion->format("Y-m-d H:i:s") ?>?</p>
    
    </br>
    <input type="hidden" name="id" value="<?= $this->contenido->id ?>"/>
    <span><input type="submit" value="Borrar"> - <a href="/?controller=Front&action=list">Cancelar</a></span>
    </form>
    
    </div>
    </body>
</html>
  <?php }
} ?>
