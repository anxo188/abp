<html>
	<body>
	
	<?php
    require_once "Vistas/Menu.php";
    new Header();
    require_once "Funciones/isAdmin.php";
    ?>
	
	<h1>Inscribirse en partido</h1>

	<form action="?controller=Partido&action=inscription_confirm&id=<?= $partido->id ?>"
	      method="post">
	<p>
	Desea inscribirse en el partido en <?= $partido->reserva->pista->nombre ?>
	a las <?= $partido->reserva->dia->format("Y-m-d H:i:s") ?> ?
	</p>
	<input type="submit" value="Aceptar"/>
	<a href="?controller=Partido&action=list">Cancelar</a>
	</form>
	</body>
</html>
