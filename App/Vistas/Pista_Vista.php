<?php


class Pista{
	var $pista;


  function __construct($pista){
    $this->pista=$pista;
    $this->render();
  }


 
  function render(){
    require_once "Vistas/Menu.php";
    new Header();
    require_once "Funciones/isAdmin.php";
     console_log($this->pista);
  ?>
    <body>
    <link rel="stylesheet" href="Vistas/formulario.css" type="text/css">
      
      <div class="mainBox">
        
          <form action = '?controller=Pista&action=list' method = 'post'>
            
              <label for="nombre">Nombre</label>
              <input readonly type="" name= "nombre" id="nombre" required="" value="<?php echo $this->pista->nombre?>">
            
              <label for="inicio">Inicio</label>
              <input readonly type="" name= "inicio" id="inicio" required="" value="<?php echo $this->pista->inicio?>">
            
              <label for="fin">Fin</label>
              <input readonly type="" name= "fin" id="fin" required="" value="<?php echo $this->pista->fin?>">
        
              <label for="descripcion">Descripcion</label>          
              <textarea readonly><?php echo $this->pista->descripcion?></textarea>
                    
            <button type="submit">Atrás</button>
          </form>
          <?php 
                if(isadmin()){
                  ?>
                  <form action="/?controller=Pista&action=delete" method="post">
                    <input type="hidden" name="idPista" value="<?php echo $this->pista->id; ?>">
                    <button type="submit">Eliminar</button>
                </form>
                <form action="/?controller=Pista&action=edit" method="post">
                <input type="hidden" name="idPista" value="<?php echo $this->pista->id; ?>">
                  <button type="submit">Edit</button>
                </form>
              <?php
                }
              ?>  
      </div>
    </body>
<?php

  //include 'footer.php';
  } /*FIN RENDER*/

}   /*FIN CLASS*/

?>
