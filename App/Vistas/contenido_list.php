<?php


class ContenidoList{
  var $contenidos;
  var $user;
  function __construct($contenidos, $user){
    $this->contenidos = $contenidos;
    $this->user = $user;
    $this->render();
  }


 
  function render(){
    require_once "Vistas/Menu.php";
    new Header();
    require_once "Funciones/isAdmin.php";
		
  ?>

  <body>
    <h1>Noticias</h1>
    
<?php
    if($this->user['tipo'] == 'A') {
?>
    <p><a href="/?controller=Front&action=create">Crear noticia</a></p>
<?php
    }
?>
    
    <div>
<?php
    echo "<div>";
    foreach($this->contenidos as $c) {
        echo "<h3>" . $c->titulo . "</h3>";
        echo "<span>" . $c->creacion->format("Y-m-d H:i:s") . "</span>";
        echo "<p>" . $c->texto . "</p>";
        
        if($this->user['tipo'] == 'A') {
?>
        <span><a href="/?controller=Front&action=edit&id=<?= $c->id ?>">Modificar</a> - <a href="/?controller=Front&action=delete&id=<?= $c->id ?>">Eliminar</a></span>
<?php
        }
    }
    
    echo "</div>";
?>
    </div>
    </body>
</html>
  <?php }
} ?>
