<?php 
  class Login{
    function __construct(){
      $this->render();
    }
    function render(){
      
?>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="Vistas/login.css" type="text/css">
        <title>ABP_PADLE</title>
    </head>

    <body>
      
      <div class="grid-container">
        <div class="mainBox">
          
          <form action = '/?controller=Login' method = 'post'>
          <p>PadelDie</p>
            
              <label for="loginemail">Identificador</label>
              <input type="" name= "identificador" id="loginemail" required="" aria-describedby="emailHelp" placeholder="Email o DNI">
           

         
              <label for="loginpassword">Contraseña</label>
              <input type="password" required="" name= "pass" id="loginpassword" placeholder="Contraseña">
            
           
              <button type="submit">Entrar</button>
              <a href="./?controller=Register" class="btn btn-primary" role="button">No tienes una cuenta</a> 
           
          </form>
          
        </div>
        <div class="contenedorImagen">
          <img class="imagen" src="https://www.esade.edu/EsadeCMSView/image?imgId=12884939979">
        </div>
      </div>
    </body>

</html>
<?php    }//End render

  }//End class
?>
