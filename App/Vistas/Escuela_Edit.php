<?php
//session_start();  --> no hace falta xq ya lo has indicado en el controlador  y como todas las vistas se cargan en el controlador ya funcionan las sesiones



class Escuela_Edit{
var $escuela;



  function __construct($escuela){
    $this->escuela=$escuela;
    $this->render();
  }


 
  function render(){
	require_once "Vistas/Menu.php";
    new Header();
    $tupla =  mysqli_fetch_object($this->escuela);
  ?>

	<link rel="stylesheet" href="Vistas/formulario.css" type="text/css">  
	<div class="mainBox">
	<form id="formularioEscuelaEdit" name = 'form' action='Controladores/Escuela_Controller.php?action=edit_Confirm&idEscuela=<?php echo "$tupla->id"; ?>' method = 'post' enctype="multipart/form-data" onsubmit="">
		
			
				<label for="nombre">
					Nombre
				</label>
					<input type="text" id="nombre" name="nombre" value="<?php echo $tupla->nombre; ?>">

				<label for="telefono">Telefono</label>
              	<input type="text" name= "telefono" id="Telefono" required="" placeholder="9 digitos sin espacios" value="<?php echo $tupla->telefono; ?>">
            
				<label for="direccion">Direccion</label>
				<input type="text" name= "direccion" id="Direccion" required="" value="<?php echo $tupla->direccion; ?>">	
				<br>
			
				<button type="submit">Editar</button>
			
		<a role="button" href="./?controller=EscuelaGeneral&action=list">Atrás</a>
		
		</form>
		</div>

<?php

  //include 'footer.php';
  } /*FIN RENDER*/

}   /*FIN CLASS*/

?>
