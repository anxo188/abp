<?php


class Pistas{
	var $pistas;


  function __construct($pistas){
    $this->pistas=$pistas;
    $this->render();
  }


 
  function render(){
    require_once "Vistas/Menu.php";
    new Header();
    require_once "Funciones/isAdmin.php";
		
  ?>
    <body>
      <div>                     
       
          <legend class="inscripcionCampeonato text-center">Pistas registradas</legend>
            <table class="table">
                <thead>
                    <tr>
                        <th  scope="col">Nombre</th>
                        <th  scope="col">Horario inicio</th>
                        <th  scope="col">Horario fin</th>
                        <th  scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if($this->pistas != false){
                    while( ($pista = array_pop($this->pistas))!=NULL){ 
                        ?>
                        <tr>
                            <td> <?php echo $pista->nombre; ?></td>
                            <td> <?php echo $pista->inicio; ?></td>
                            <td> <?php echo $pista->fin; ?></td>
                            <td>
                              <form action="/?controller=Pista&action=view" method="post">
                              <input type="hidden" name="idPista" value="<?php echo $pista->id; ?>">
                                <button type="submit">Ver</button>
                              </form>
                              <?php 
                              if(isadmin()){
                                ?>
                                <form action="/?controller=Pista&action=delete" method="post">
                                  <input type="hidden" name="idPista" value="<?php echo $pista->id; ?>">
                                  <button type="submit">Eliminar</button>
                              </form>
                              <form action="/?controller=Pista&action=edit" method="post">
                              <input type="hidden" name="idPista" value="<?php echo $pista->id; ?>">
                                <button type="submit">Edit</button>
                              </form>
                            <?php
                              }
                            ?>
                              
                        </tr>
                    <?php }  
                    }
                    ?>
                </tbody>
            </table>
        
      </div>
      <?php 
          if(isadmin()){
        ?>
        <div>
            <a role="button" href="/?controller=Pista&action=add" class="btn btn-primary " > Añadir Pista </a> 
        </div>
        <?php
          }
        ?>
    </body>
</html>

<?php

  //include 'footer.php';
  } /*FIN RENDER*/

}   /*FIN CLASS*/

?>
