<?php


class CampeonatoInscripcion_ListaMisCampeonatos{
	var $campeonatos;


  function __construct($campeonatos){
    $this->campeonatos=$campeonatos;

    $this->render();
  }


 
  function render(){
    require_once "Vistas/Menu.php";
    new Header();
  ?>

<div> 

                               
  <fieldset>
              <legend class="inscripcionCampeonato text-center">Mis Campeonatos</legend>

                <table class="table">
                    <thead>
                        <tr>

                            <th scope="col">Nombre</th>
                            <th scope="col">inicio</th>
                            <th scope="col">fin</th>
                            <th scope="col">normativa</th>
                            <th scope="col">nivel</th>
                            <th scope="col">categoria</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        if($this->campeonatos != false){
                           while($tupla =  mysqli_fetch_object($this->campeonatos)){ ?>
                            <tr>
                                <td> <?php echo $tupla->nombre; ?></td>
                                <td> <?php echo $tupla->inicio; ?></td>
                                <td> <?php echo $tupla->fin; ?></td>
                                <td> <?php echo $tupla->normativa; ?></td>
                                <td> <?php echo $tupla->nivel; ?></td>
                                <td> <?php echo $tupla->categoria; ?></td>

                                <td>
                                    <a href="./?controller=CampeonatoInscription&action=delete&id=<?php echo $tupla->id; ?>" />Quitar_inscripcion</a>
                                </td>
                            </tr>
                        <?php }  
                        }
                         ?>
                    </tbody>
                </table>

            </fieldset>
<a role="button" href="./?controller=CampeonatoGeneral&action=list" class="btn btn-primary " >Volver a los campeonatos</a>
</div>

</body>


<?php

  //include 'footer.php';
  } /*FIN RENDER*/

}   /*FIN CLASS*/

?>
