<?php


class ReservaList{
  var $reservas;
  function __construct($reservas){
    $this->reservas=$reservas;
    $this->render();
  }


 
  function render(){
    require_once "Vistas/Menu.php";
    require_once "Modelos/Horario_model.php";
    new Header();
    require_once "Funciones/isAdmin.php";
		
  ?>

  <body>
    <h1>Reservas</h1>
    
    <div>
    <legend class="inscripcionCampeonato text-center">Reservas realizadas</legend>
            <table class="table">
              <thead>
                  <tr>
                      <th  scope="col">Pista</th>
                      <th  scope="col">dia</th>
                      <th scope="col"> hora</th>
                      <th  scope="col"></th>
                  </tr>
              </thead>
              <tbody>
                    <?php
                    if($this->reservas != false){
                      foreach($this->reservas as $r) {
                        ?>
                        <tr>
                            <td> <?php echo $r->pista->nombre; ?></td>
                            <td> <?php echo $r->dia->format("Y-m-d"); ?></td>
                            <td> <?php echo Horario::get($r->horario);?></td>
                            <td>
                              <form action="/?controller=Reserva&action=delete&id=<?= $r->id ?>" method="post">
                              <input type="hidden" name="idPista" value="<?php echo $pista->id; ?>">
                                <button type="submit">Eliminar</button>
                              </form> 
                        </tr>
                    <?php }  
                    }
                    ?>
                </tbody>
              </table>  
      </div>
      <div>
        <a class="btn btn-primary " role="button" href="?controller=Reserva&action=add">Crear reserva</a>
      </div>
    </body>
</html>
  <?php }
} ?>
