<?php

require_once 'Funciones/isAuthenticated.php';

// Si el usuario está conectado, desconectarle borrando los datos de sesión asociados
console_log(isAuthenticated());
if (isAuthenticated()) {
	session_destroy();
	header('Location: /?controller=Front');
}


?>