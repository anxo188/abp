<?php

function isAdmin(){
	require_once 'Funciones/isAuthenticated.php';
	if(isAuthenticated()){
		require_once 'Modelos/User_Model.php';
		$resultado = USUARIOS_Model::isAdmin($_SESSION['userid']);
		if($resultado == true){
			return true;
		}else if ($resultado == false){
			return false;
		}else if(isset($_GET['debug']) && $_GET['debug'] == 777){
			return $resultado;
		}else{
			return false;
		}
	}else{
		return false;
	}

	
}

function isEntrenador(){
	require_once 'Funciones/isAuthenticated.php';
	if(isAuthenticated()){
		require_once 'Modelos/User_Model.php';
		$resultado = USUARIOS_Model::isEntrenador($_SESSION['userid']);
		if($resultado == true){
			return true;
		}else if ($resultado == false){
			return false;
		}else if(isset($_GET['debug']) && $_GET['debug'] == 777){
			return $resultado;
		}else{
			return false;
		}
	}else{
		return false;
	}

	
}

?>