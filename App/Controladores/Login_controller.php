<?php
require_once "Funciones/consoleLogger.php";

/**
 * Comprueba que el usuario esta autenticado
 */
	if(!isset($_SESSION['userid']) && ( !isset( $_POST['identificador']) || !isset($_POST['pass']) ) ){
		/**
		 * En caso de no estar ya autenticado se procede al login
		 */
		
			console_log("Procediendo al login ...");
		require_once ('Vistas/LoginView.php');
		new Login();
			
	}
	elseif (!isset($_SESSION['userid']) && isset($_POST['identificador']) && isset($_POST['pass'])){
		
		/**
		 * No esta logeado pero está en proceso
		 *
		 * Realiza una query a la BD para comprobar si el usuario es correcto
		 */
		require_once ('Modelos/User_Model.php');
		$usuario = new USUARIOS_Model($_POST['identificador'],md5($_POST['pass']),0,"","",$_POST['identificador'],"");
		$respuesta = $usuario->login();
		
		if(is_integer($respuesta)){
			/**
			 * En caso de login exitoso establece las variables de sesion
			 */
			$_SESSION['userid'] = $respuesta;
			$_SESSION['userName'] = $_POST['identificador'];
			
			console_log("Loging exitoso, volviendo a front ...");
			header("Location: /?controller=Front", TRUE, 301);	

		} else {
			
			/**
			 * Muestra el posible error de login
			 */
			console_log("Errore en el loging");
			require_once 'Vistas/error.php';
			new CustomError("Identificador o contraseña incorrecta");
			
		}
	} else{
		/**
		 * Ya logeado
		 */
		console_log("Usuario ya logeado");
		//header('Location:./?controller=Register',TRUE,301);
		
		
	}


?>
