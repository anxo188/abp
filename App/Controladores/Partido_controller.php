<?php

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

$user_id = 1;

if(!isset($_GET['action'])) {
    $_GET['action'] = "list";
}

function getPartidoId($ins) {
    return $ins->partido_id;
}

if(isset($_GET['action'])) {
    if($_GET['action'] == "list") {
        require_once('Modelos/Partido_Model.php');
        require_once('Modelos/Inscripcion_Model.php');
        require_once('Modelos/Reserva_Model.php');
        require_once('Modelos/Pista_Model.php');

        $now = new DateTime(date("Y-m-d H:i:s"));
        $partidos = Partido_Model::getAfterDate($now);
        
        foreach($partidos as $p) {
            $p->reserva = Reserva_Model::getById($p->reserva_id);
            $p->reserva->pista = Pista_Model::get($p->reserva->pista_id);
        }
        
        $inscripciones = Inscripcion_Model::getByJugador($user_id);
        $inscripciones_partido = array_map("getPartidoId", $inscripciones);

        require_once('Vistas/partido_list.php');
    } else if($_GET['action'] == "inscription" && isset($_GET['id'])) {
        require_once('Modelos/Partido_Model.php');
        require_once('Modelos/Reserva_Model.php');
        require_once('Modelos/Pista_Model.php');
        
        $partido = Partido_Model::getById($_GET['id']);
        $partido->reserva = Reserva_Model::getById($partido->reserva_id);
        $partido->reserva->pista = Pista_Model::get($partido->reserva->pista_id);

        require_once('Vistas/partido_inscription.php');
    } else if($_GET['action'] == "inscription_confirm" && isset($_GET['id'])) {
        require_once('Modelos/Inscripcion_Model.php');
        require_once('Modelos/Partido_Model.php');

        $inscripcion = new Inscripcion_Model(0, $user_id, $_GET['id']);
        $inscripcion->partido = Partido_Model::getById($inscripcion->partido_id);
        if($inscripcion->partido->getNumInscritos() == 4) {
            echo "Error: ya hay 4 inscritos en el partido.\n";
            return;
        }

        // Confirmar reserva del partido
        if($inscripcion->partido->getNumInscritos() == 3) {
            $inscripcion->partido->reserva->confirmada = 1;
            $inscripcion->partido->reserva->update();
        }
        
        $inscripcion->save();
        header("Location: /?controller=Partido&action=list", TRUE, 301);
    } else if($_GET['action'] == "create") {
        require_once('Modelos/Pista_Model.php');

        $pistas = Pista_Model::getAll();
        require_once('Modelos/Reserva_Model.php');
        $reservas = Reserva_Model::getWeek();
        
        require_once('Vistas/partido_create.php');
        new PartidoAdd($pistas,$reservas);

    } else if($_GET['action'] == "create_confirm") {
        if(!isset($_POST['pista']) || !isset($_POST['selected']) || !isset($_POST['diaData'])) {
            echo "Error: campos incorrectos\n";
            exit;
        }
        require_once('Modelos/Reserva_Model.php');
        require_once('Modelos/Partido_Model.php');
        require_once('Modelos/Pista_Model.php');
        
        $pista = Pista_Model::get($_POST['pista']);
        $day = getdate(strtotime("+".$_POST['diaData']." day"));
        $dia = $day['year']."-".$day['mon']."-".$day['mday']." 00:00:00";
        if(!$pista) {
            echo "Error: pista no encontrada\n";
            
            return;
        }
        $reserva = new Reserva_Model(0,
        $dia,
        $_POST['selected'],
        $pista->id,
        $user_id,
        0);
        $reserva->save();
        
        $partido = new Partido_Model(0, $reserva->id);
        $partido->save();
        header("Location: /?controller=Partido&action=list", TRUE, 301);
    } else if($_GET['action'] == "delete" && isset($_GET['id'])) {
        require_once('Modelos/Partido_Model.php');
        require_once('Modelos/Reserva_Model.php');

        $partido = Partido_Model::getById($_GET['id']);
        $partido->reserva = Reserva_Model::getById($partido->reserva_id);
        $partido->reserva->delete();
        $partido->delete();

        header("Location: /?controller=Partido&action=list", TRUE, 301);
    } else if($_GET['action'] == "unsubscribe" && isset($_GET['id'])) {
        require_once('Modelos/Inscripcion_Model.php');

        $ins = Inscripcion_Model::getByJugadorPartido($user_id, $_GET['id']);

        foreach($ins as $i) {
            $i->delete();
        }
        header("Location: /?controller=Partido&action=list", TRUE, 301);
    }
}

    

?>
