<?php 
	
	require_once "Funciones/isAdmin.php";
	if(!isset($_GET['action']) ){
		$_GET['action']="showRanking";
	}
	
	switch($_GET['action']){

		case "showActions":
			
			if(isAdmin()){
				
				require_once 'Vistas/GestionarLiga_Actions.php';
				require_once 'Modelos/Campeonato_Model.php';
				$campeonato = Campeonato_Model::get($_GET['idCampeonato']);
				new Gestor($campeonato);


			}
		
		break;

		case "editEnfrentamiento":
				require_once 'Modelos/Enfrentamiento_Model.php';
				
				if(!isset($_POST['res1']) || !isset($_POST['res2'])){
					$enfrentamiento = Enfrentamiento::get($_GET['idEnfrentamiento']);
					require_once 'Vistas/EnfrentamientoEdit_View.php';
					new EnfrentamientoEdit($enfrentamiento);
				}else{
					Enfrentamiento::edit($_GET['idEnfrentamiento'],$_POST['res1'],$_POST['res2']);
					$enfrentamiento = Enfrentamiento::get($_GET['idEnfrentamiento']);
					require_once 'Vistas/EnfrentamientoEdit_View.php';
					new EnfrentamientoEdit($enfrentamiento);
				}
		break;

		case "showBlocks":
			
			$idCampeonato = $_GET['idCampeonato'];

			require_once 'Modelos/Bloque_Model.php';
			
			$bloques = Bloque_Model::getAll($idCampeonato);
			require_once 'Vistas/BloquesShow.php';
			
			new BloquesShowView($bloques);
			break;
		
		case "showBlockRanking":
			require_once 'Modelos/Pareja_Model.php';
			require_once 'Modelos/ParejasBloque_Model.php';
			$idBloque = $_GET['idBloque'];
			$parejas = ParejaBloque::getParejas($idBloque);
			
			while($parejaID = mysqli_fetch_object($parejas)){
				
				$parejaScore = calculateScore($parejaID->idPareja,$idBloque);
				if($parejaScore == 0){
					$parejaScore = -1;
				}
				$parejasArray[$parejaID->idPareja] = $parejaScore;
			}
			
			asort($parejasArray);
			require_once 'Vistas/Ranking_View.php';
			new Ranking($parejasArray);
			break;

		case "showBlockMatches":

			$idBloque = $_GET['idBloque'];
			require_once 'Modelos/EnfrentamientosBloque_Model.php';
			$resultado = EnfrentamientosBloque::getEnfrentamientos($idBloque);
			
			require_once 'Vistas/ShowMatchs_View.php';
			new ShowMatchs($resultado);
			break;
		case "showOwnBlockMatches":

				$idBloque = $_GET['idBloque'];
				require_once 'Modelos/EnfrentamientosBloque_Model.php';
				$resultado = EnfrentamientosBloque::getEnfrentamientos($idBloque);
				
				require_once 'Vistas/ShowOwnMatchs_View.php';
				new ShowOwnMatchs($resultado);
				break;

		case "closeInscriptions":
			$idCampeonato = $_GET['idCampeonato'];
			$resultado = balanceBlocks($idCampeonato);
			if($resultado != true){
				//TODO notificar parejas descartadas
				require_once 'Modelos/InscripcionCampeonato_Model.php';
				while($descarte = mysqli_fetch_object($resultado)){
					InscripcionCampeonato::eliminarDescarte(
				$descarte->idPareja);	
				}	
			}
			require_once 'Modelos/Bloque_Model.php';
			$bloques = Bloque_Model::getAll($idCampeonato);
			while($bloque = mysqli_fetch_object($bloques)){
				generateMatrix($idCampeonato,$bloque->id);
			}
			header('Location:./?controller=GestorLiga&action=showBlocks&idCampeonato='.$idCampeonato);
			
	}

	/**
	 * Dado un campeonato obtiene las parejas asociadas y crea los bloques correspondientes
	 * @return pareja[] | boolean devuelve un array con las parejas en caso de que haya parejas descartadas
	 */
function balanceBlocks($idCampeonato){

		require_once 'Modelos/CampeonatoInscripcion_Model.php';
		$parejas = CampeonatoInscripcion_Model::getParejas($idCampeonato);
		$numParejas = $parejas->num_rows;
		$bloquesNecesitados = intdiv ( $numParejas , 8 );
		if($bloquesNecesitados == 0){
			$bloquesNecesitados =1;
		}
		$repartirEntreBloques = $numParejas%8;

		require_once 'Modelos/Bloque_Model.php';
		require_once 'Modelos/ParejasBloque_Model.php';

		$bloquesCubiertos = 0;
		$numBloques = 0;
		while($bloquesCubiertos < $bloquesNecesitados){
		
			$parejasPorBloque = 8;
			$bloque = new Bloque_Model($idCampeonato,$parejasPorBloque);
			$bloque->add();
			$idBloque = $bloque->getId();
			$numBloques = $numBloques +1 ;

			while($parejasPorBloque > 0 && $pareja = mysqli_fetch_object($parejas)){
				
				$tmp = new ParejaBloque($idBloque,$pareja->id);
				$tmp->add();
				$parejasPorBloque = $parejasPorBloque-1;
			}
			$bloquesCubiertos = $bloquesCubiertos+1;
		}	
		$todosLlenos=false;
		require_once 'Modelos/Bloque_Model.php';
		require_once 'Modelos/ParejasBloque_Model.php';
		while($repartirEntreBloques > 0 && $todosLlenos==false){
			$llenos = 0;
			$bloques = Bloque_Model::getAll($idCampeonato);
			while($bloque=mysqli_fetch_object($bloques)){
				if( ($pareja=mysqli_fetch_object($parejas)) != false && $bloque->numParejas <12 ){
					
					$tmp = new ParejaBloque($bloque->id,$pareja->id);
					$tmp->add();
					Bloque_Model::increaseNumPairs($bloque->id);
					$repartirEntreBloques = $repartirEntreBloques-1;
				}else{
					$llenos = $llenos+1;
				}
			}
			if($llenos == $bloquesCubiertos){
					$todosLlenos = true;
			}
		}
		if($todosLlenos && $repartirEntreBloques>0){
			$descartadas = array();
			$num = 0;
			while($pareja = mysqli_fetch_object($parejas)){
				array_push($descartadas,$num,$pareja);
			}
			return $descartadas;
		}
		return true;
	}
	/**
	 * Dado un campeonato y un bloque genera la matriz de enfrentamientos
	 */
	function generateMatrix($idCampeonato,$idBloque){
		require_once 'Modelos/EnfrentamientosBloque_Model.php';
		require_once 'Modelos/Enfrentamiento_Model.php';
		require_once 'Modelos/ParejasBloque_Model.php';
		$parejas = ParejaBloque::getParejas($idBloque);
		$parejas1 = $parejas;
		while($pareja1 = mysqli_fetch_object($parejas1)){
		
			$parejas2 =  ParejaBloque::getParejas($idBloque);
			while($pareja2 = mysqli_fetch_object($parejas2)){

				if( $pareja1->idPareja != $pareja2->idPareja){
					$enfrentamiento = new Enfrentamiento($pareja1->idPareja,$pareja2->idPareja);
					$enfrentamiento->add();
					$enfBloque = new EnfrentamientosBloque($enfrentamiento->getId(),$idBloque);
					$resultado = $enfBloque->add();
				}
			}
		}
	}

	function calculateScore($idPareja,$idBloque){
		require_once 'Modelos/EnfrentamientosBloque_Model.php';
		require_once 'Modelos/Enfrentamiento_Model.php';
		
		$enfrentamientos = EnfrentamientosBloque::getEnfrentamientos($idBloque);
		$numGanados = 0;
		$numJugados = 0;
		while($enfrentamiento = mysqli_fetch_object($enfrentamientos)){
			$resultado = Enfrentamiento::ganado($enfrentamiento->idEnfrentamiento,$idPareja);
			
			if($resultado==true){
				$numGanados = $numGanados + 1;
				$numJugados = $numJugados + 1;
			}else if(Enfrentamiento::jugado($enfrentamiento->idEnfrentamiento,$idPareja)){
					$numJugados = $numJugados + 1;
			}
			
		}

		$score = ($numGanados*3)+($numJugados);
		return $score;
	}
 ?>
