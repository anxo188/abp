<?php  
	//session_start();
$user = isset($_SESSION['userid']) ? $_SESSION['userid'] : 0;

	if(!isset($_GET['action'])){
		$_GET['action']="list";

	}

	switch($_GET['action']){
		case "add":
			
	        require_once('Modelos/Pista_Model.php');
	        
	        $pistas = Pista_Model::getAll();
	        require_once('Modelos/Reserva_Model.php');
	        $reservas = Reserva_Model::getWeek();
			require_once 'Modelos/Escuela_Model.php';
			$respuestaEscuela = Escuela_Model::getAll();
			require_once 'Vistas/Clase_ADD.php';
			new Clase_Add($respuestaEscuela,$pistas,$reservas);

		break;

		case "add_Confirm":

		 if(!isset($_POST['escuela']) || !isset($_POST['pista']) || !isset($_POST['selected']) || !isset($_POST['diaData'])) {
            echo "Error: campos incorrectos\n";
            exit;
        }

        require_once('Modelos/Pista_Model.php');
        require_once('Modelos/Reserva_Model.php');
        require_once 'Modelos/Clase_Model.php';

        
        $pista = Pista_Model::get($_POST['pista']);
        $day = getdate(strtotime("+".$_POST['diaData']." day"));
        $dia = $day['year']."-".$day['mon']."-".$day['mday']." 00:00:00";
        $reserva = new Reserva_Model(0,
                                     $dia,
                                     $_POST['selected'],
                                     $pista->id,
                                     $user,
                                     1);
        


				$respuestaReserva = $reserva->save();

		        /*if($reserva->save() == FALSE) {
   					require_once 'Vistas/mensaje.php';
					new CustomMessage("Error al insertar reserva");
       			 }*/

       			
       			$idReserva = Reserva_Model::getIdReserva($dia,$_POST['selected'],$pista->id,$user);
       			$reserva =  mysqli_fetch_object($idReserva);
				$claseM = new Clase_Model($_POST['escuela'],$user,$reserva->id);
				$respuesta = $claseM->add();

				
				if( isset($_GET['debug']) && $_GET['debug'] == 777){

					require_once 'Vistas/mensaje.php';
					new CustomMessage($respuesta);

				}else{ 
					
				if($respuesta == true){

					require_once 'Vistas/mensaje.php';
					new CustomMessage("Clase añadida correctamente");
					
				}else{
						require_once 'Vistas/mensaje.php';
						new CustomMessage("No se pudo añadir la clase");
					}
				}
				
			break;

		case "delete":
			require_once 'Funciones/isAdmin.php';
			if(isEntrenador())
			{
				require_once 'Modelos/Clase_Model.php';
				$respuestaClase = Clase_Model::get($_GET['idClase']);
				$aux =  mysqli_fetch_object($respuestaClase);

				if($aux->idEntrenador == $user){

				$respuesta = Clase_Model::delete($_GET['idClase']);
				
				if($respuesta == true)
				{
					require_once 'Vistas/mensaje.php';
					new CustomMessage("Clase borrada correctamente");
				}else{ 
					if( isset($_GET['debug']) && $_GET['debug'] == 777){

						require_once 'Vistas/mensaje.php';
						new CustomMessage($respuesta);
					}else
					{
						require_once 'Vistas/mensaje.php';
						new CustomMessage("No se pudo borrar la clase");
					}
				}

			}else{
				require_once 'Vistas/mensaje.php';
				new CustomMessage("No puedes eliminar una clase que no es tuya");
			}	
			}else{
				header("HTTP/1.0 403 Forbidden");
			}

			break;

		case "inscribirse":
		require_once 'Funciones/isAdmin.php';



			
			require_once 'Modelos/InscripcionClase_Model.php';
			$inscribirse = new InscripcionClase_Model($_GET['idClase'],$user);

			$respuesta = $inscribirse->add();
			
			if($respuesta == true)
			{
				require_once 'Vistas/mensaje.php';
				new CustomMessage("Te has inscrito correctamente en la clase");
			}else{ 
				if( isset($_GET['debug']) && $_GET['debug'] == 777){

					require_once 'Vistas/mensaje.php';
					new CustomMessage($respuesta);
				}else
				{
					require_once 'Vistas/mensaje.php';
					new CustomMessage("No te puedes inscribir en la clase");
				}
			}


		break;

		case "list_Clases":
			require_once 'Funciones/isAdmin.php';
		
					
			require_once 'Modelos/Escuela_Model.php';
			require_once 'Modelos/User_Model.php';
			require_once 'Modelos/Clase_Model.php';
			require_once 'Vistas/Clase_Listado.php';
			$respuesta = Clase_Model::getClaseByEntrenador($user);

			
			$respuestaEntrenador = USUARIOS_Model::get($user);
			$entrenador =  mysqli_fetch_object($respuestaEntrenador);
		
			

			
			if(isset($_GET['debug']) && $_GET['debug'] == 777){

				require_once 'Vistas/mensaje.php';
				new CustomMessage($respuesta);

			}else{

				
				new Clase_Listado($respuesta,NULL,$entrenador);
			}
			break;

		case "list":
		require_once 'Funciones/isAdmin.php';
		
					
					require_once 'Modelos/Escuela_Model.php';
					require_once 'Modelos/User_Model.php';
					require_once 'Modelos/Clase_Model.php';
					require_once 'Vistas/Clase_Listado.php';
					$respuestaEscuela = Escuela_Model::get($_REQUEST['idEscuela']);
					$escuela =  mysqli_fetch_object($respuestaEscuela);
					
					$respuestaEntrenador = USUARIOS_Model::get($user);
					$entrenador =  mysqli_fetch_object($respuestaEntrenador);
				
					$respuesta = Clase_Model::getClaseByEscuela($_REQUEST['idEscuela']);

					
					if(isset($_GET['debug']) && $_GET['debug'] == 777){

						require_once 'Vistas/mensaje.php';
						new CustomMessage($respuesta);

					}else{

						
						new Clase_Listado($respuesta,$escuela,$entrenador);
					}
			break;
		
		
	}

?>