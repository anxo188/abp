<?php

$userid = isset($_SESSION['userid'])? $_SESSION['userid'] : 0;
$user = NULL;

if($userid > 0) {
    require_once 'Modelos/User_Model.php';
    $user_result = USUARIOS_Model::get($userid);
    
    if($user_result == false) {
        echo "Error: usuario id=$userid desconocido.";
        exit;
    }
    $user = $user_result->fetch_array();
}

function checkAdmin($user) {
    if($user != NULL && $user['tipo'] != "A") {
        echo "Error: usuario no es administrador";
        return FALSE;
    } else return TRUE;
}

if(!isset($_GET['action']) || $_GET['action'] == 'list') {
    require_once 'Modelos/Contenido_Model.php';
    
    $contenidos = Contenido_Model::getAll();
    require_once 'Vistas/contenido_list.php';
    new ContenidoList($contenidos, $user);
} else if($_GET['action'] == "create") {

    if(!checkAdmin($user)) {
        exit;
    }
    
    require_once 'Vistas/contenido_createedit.php';
    new ContenidoCreateEdit($user);
} else if($_GET['action'] == "edit") {
    if(!checkAdmin($user)) exit;

    require_once 'Modelos/Contenido_Model.php';
    
    if(!isset($_GET['id'])) {
        echo "Error: id de noticia no especificada.";
        exit;
    }
    
    $contenido = Contenido_Model::getById($_GET['id']);
    if($contenido == NULL) {
        echo "Error: noticia id=" . $_GET['id'] . " no encontrada.";
        exit;
    }
    
    require_once 'Vistas/contenido_createedit.php';
    new ContenidoCreateEdit($user, $contenido);
} else if($_GET['action'] == "confirm") {
    if(!checkAdmin($user)) exit;

    require_once 'Modelos/Contenido_Model.php';
    
    if(!isset($_POST['text']) || !isset($_POST['title'])) {
        echo "Faltan campos.";
        exit;
    }
    
    if(!isset($_POST['id'])) {
        $contenido = new Contenido_Model(0,
                            $_POST['title'],
                            $_POST['text'],
                            date(),
                            $user['id']);
        if($contenido->save() != FALSE) {
            header("Location: /?controller=Front&action=list", TRUE, 301);
        } else {
            echo "Error insertando columna.";
        }
    } else {
        $contenido = Contenido_Model::getById($_POST['id']);
        if($contenido == NULL) {
            echo "Noticia id=" . $_POST['id'] . " no encontrada.";
            exit;
        }
        $contenido->titulo = $_POST['title'];
        $contenido->texto = $_POST['text'];
        
        if($contenido->update() == FALSE) {
            echo "Error actualizando contenido.";
            exit;
        } else {
            header("Location: /?controller=Front&action=list", TRUE, 301);
        }
    }
} else if($_GET['action'] == "delete") {
    if(!checkAdmin($user)) exit;

    if(!isset($_GET['id'])) {
        echo "Error: id no especificada";
        exit;
    }
    require_once 'Modelos/Contenido_Model.php';
    
    $contenido = Contenido_Model::getById($_GET['id']);
    if($contenido == NULL) {
        echo "Error: contenido con id=" . $_GET['id'] . " no encontrado.";
        exit;
    }
    
    require_once 'Vistas/contenido_delete.php';
    new ContenidoDelete($contenido, $user);
}else if($_GET['action'] == "delete_confirm") {
    if(!checkAdmin($user)) exit;
    
    if(!isset($_POST['id'])) {
        echo "Error: id no especificado";
        exit;
    }
    require_once 'Modelos/Contenido_Model.php';
    
    $contenido = Contenido_Model::getById($_POST['id']);
    if($contenido == NULL) {
        echo "Error: contenido con id=" . $_GET['id'] . " no encontrado";
        exit;
    }
    
    $contenido->delete();
    header("Location: /?controller=Front&action=list", TRUE, 301);
} else {
    echo "Accion desconocida: " . $_GET['action'];
}

?>
