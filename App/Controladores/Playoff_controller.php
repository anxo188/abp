<?php

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

$user_id = $_SESSION['userid'];

function calculateScore($idPareja,$idBloque) {
		require_once 'Modelos/EnfrentamientosBloque_Model.php';
		require_once 'Modelos/Enfrentamiento_Model.php';
		
		$enfrentamientos = EnfrentamientosBloque::getEnfrentamientos($idBloque);
		$numGanados = 0;
		$numJugados = 0;
		while($enfrentamiento = mysqli_fetch_object($enfrentamientos)){
			$resultado = Enfrentamiento::ganado($enfrentamiento->idEnfrentamiento,$idPareja);
			
			if($resultado==true){
				$numGanados = $numGanados + 1;
				$numJugados = $numJugados + 1;
			}else if(Enfrentamiento::jugado($enfrentamiento->idEnfrentamiento,$idPareja)){
					$numJugados = $numJugados + 1;
			}
			
		}

		$score = ($numGanados*3)+($numJugados);
		return $score;
}

if(isset($_GET['action'])) {
    if($_GET['action'] == "show" && isset($_GET['campeonato'])) {
        require_once('Modelos/Campeonato_Model.php');
        require_once('Modelos/Playoff_Model.php');
        require_once('Modelos/PartidoPlayoff_Model.php');

        $tupla = Campeonato_Model::get(intval($_GET['campeonato']))->fetch_array();
        $campeonato = new Campeonato_Model($tupla['nombre'],
                                    $tupla['inicio'],
                                    $tupla['fin'],
                                    $tupla['normativa'],
                                    $tupla['nivel'],
                                    $tupla['categoria']);
        $campeonato->id = $tupla['id'];
        if($campeonato == FALSE) {
            echo "Error: campeonato no encontrado\n";
            return;
        }

        $playoff = Playoff_Model::getByCampeonato($campeonato->id);
        require_once('Vistas/playoff_show.php');
    } else if($_GET['action'] == "create" && isset($_GET['campeonato'])) {
        require_once('Modelos/Playoff_Model.php');
        require_once('Modelos/Reserva_Model.php');
        require_once('Modelos/PartidoPlayoff_Model.php');
        
        $hora = new DateTime();
        $playoff = new Playoff_Model(0, $hora->format("Y-m-d H:00:00"), $_GET['campeonato']);
        $playoff->save();
        $parejas = Playoff_Model::getParejasBloque($_GET['campeonato']);
        
        $enfrentamientos = array();
        
        for($i=0;$i < 4; $i++) {
            array_push($enfrentamientos, array($parejas[$i][2], $parejas[7 - $i]));
        }
        
        $i = 0;
        $pistas = Pista_Model::getAll();
        $hora->add(new DateInterval("P7D"));
        
        foreach($enfrentamientos as $e) {
            $reserva = new Reserva_Model(0, $hora->format("Y-m-d H:00:00"), 1, $pistas[$i++], $user_id, 1);
            $reserva->save();
            if($i == sizeof($pistas)) {
                $i = 0;
                $hora->add(new DateInterval("PT1H"));
            }
            $mysqli = BDConector::createConection();
            $rid = $reserva->id;
            $p1 = $e[0]; $p2 = $e[1];
            $mysqli->query("insert into ENFRENTAMIENTO values (0, $p1, $p2, 0, 0, $rid)");
            $eid = $mysqli->insert_id;
            $partidoplayoff = new PartidoPlayoff_Model($playoff->id, $eid, 0);
            $partidoplayoff->save();
        }
        
        $cid = $_GET['campeonato'];
        header("Location: /?controller=Playoff&action=show&campeonato=$cid", TRUE, 301);
    } else if($_GET['action'] == "result" && isset($_GET['campeonato']) && isset($_GET['enfrentamiento'])) {
        require_once('Modelos/Playoff_Model.php');
        require_once('Modelos/Enfrentamiento_Model.php');
        require_once('Modelos/Campeonato_Model.php');
        
        $tupla = Campeonato_Model::get($_GET['campeonato']);
        $campeonato = new Campeonato_Model($tupla['nombre'],
                                    $tupla['inicio'],
                                    $tupla['fin'],
                                    $tupla['normativa'],
                                    $tupla['nivel'],
                                    $tupla['categoria'],
                                    $tupla['id']);
        $enfrentamiento = Enfrentamiento::get($_GET['enfrentamiento']);
        require_once('Vistas/playoff_result.php');
    } else if($_GET['action'] == "result_confirm" && isset($_GET['campeonato']) && isset($_GET['enfrentamiento'])) {
        require_once('Modelos/BDConector.php');
        
        $mysqli = BDConector::createConection();
        $r1 = $_POST['res1'];
        $r2 = $_POST['res2'];
        $eid = $_GET['enfrentamiento'];
        $cid = $_GET['campeonato'];
        $mysqli->query("update ENFRENTAMIENTO set res1 = $r1, res2 = $r2 where id = $eid");
        
        header("Location: /?controller=Playoff&action=show&campeonato=$cid", TRUE, 301);
    } else if($_GET['action'] == "create_phase" && isset($_GET['campeonato'])) {
        require_once('Modelos/Playoff_Model.php');
        require_once('Modelos/Reserva_Model.php');
        require_once('Modelos/PartidoPlayoff_Model.php');
        
        $hora = new DateTime();
        $playoff = Playoff_Model::getByCampeonato($_GET['campeonato']);
        $parejas = array();
        
        $max_fase = 0;
        foreach($playoff->enfrentamientos as $e) {
            if($e['fase'] > $max_fase) {
                $max_fase = $e['fase'];
            }
        }
        
        foreach($playoff->enfrentamientos as $e) {
            if($e['fase'] == $max_fase) {
                if($e['r1'] >= $e['r2']) {
                    array_push($parejas, $e['p1']);
                } else {
                    array_push($parejas, $e['p2']);
                }
            }
        }
        
        $enfrentamientos = array();
        
        for($i=0;$i < sizeof($parejas) / 2; $i++) {
            array_push($enfrentamientos, array($parejas[$i], $parejas[sizeof($parejas) - 1 - $i]));
        }
        
        print_r($enfrentamientos);
        
        $i = 0;
        $pistas = Pista_Model::getAll();
        $hora->add(new DateInterval("P7D"));
        
        foreach($enfrentamientos as $e) {
            $reserva = new Reserva_Model(0, $hora->format("Y-m-d H:00:00"), $pistas[$i++], $user_id, 1);
            $reserva->save();
            if($i == sizeof($pistas)) {
                $i = 0;
                $hora->add(new DateInterval("PT1H"));
            }
            $mysqli = BDConector::createConection();
            $rid = $reserva->id;
            $p1 = $e[0]; $p2 = $e[1];
            $mysqli->query("insert into ENFRENTAMIENTO values (0, $p1, $p2, 0, 0, $rid)");
            $eid = $mysqli->insert_id;
            $partidoplayoff = new PartidoPlayoff_Model($playoff->id, $eid, $max_fase+1);
            $partidoplayoff->save();
        }
        
        $cid = $_GET['campeonato'];
        header("Location: /?controller=Playoff&action=show&campeonato=$cid", TRUE, 301);
    }
}

    

?>
