<?php  
	
	if(!isset($_GET['action'])){
		$_GET['action']="listConversations";
	}
	require_once "Modelos/Chat_Model.php";
	switch($_GET['action']){
		case "history":
			//Carga el fichero completo en una cadena
			$chat = $_GET['chatId'];
			$chatFile = Chat_Model::getFilePath($chat);
			$history = file_get_contents($chatFile);
			require_once "Vistas/Chat_View.php";
			new Chat($chat,$history);
			// file_get_contents ( string $filename [, bool $use_include_path = FALSE [, resource $context [, int $offset = 0 [, int $maxlen ]]]] ) : string
		break;
		case "send":
			//Escribe data en el fichero
			$chat = $_GET['chatId'];
			$chatFile = Chat_Model::getFilePath($chat);
			
			require_once "Modelos/User_Model.php";
			$user = USUARIOS_Model::get($_SESSION['userid']);
			$user = mysqli_fetch_object($user);
			$sender= $user->nombre;
			$content = " <div class='mensaje'> <p>Hora: ".date("Y/m/d h:i:sa")."</p> <p>Mensaje: ".$_POST['mensaje']."</p>
			<p>Sender: ".$sender."</p></div>";
			$history = file_put_contents($chatFile,$content,FILE_APPEND);
			header("Location:./?controller=Chat&action=history&chatId=$chat");
			//file_put_contents ( string $filename , mixed $data );
		break;
		
		case "sendFirst":
			//Escribe data en el fichero
			require_once "Modelos/Enfrentamiento_Model.php";
			require_once "Modelos/Pareja_Model.php";
			$file = "Files/".uniqid($_SESSION['userid'],true).".txt";
			$enfrentamiento = Enfrentamiento::get($_GET['enfrentamientoId']);
			if(Pareja_Model::pertenece($enfrentamiento->pareja1,$_SESSION['userid'])){
				$pareja = Pareja_Model::get($enfrentamiento->pareja1);
				$parejaRival = Pareja_Model::get($enfrentamiento->pareja2);
			}else if(Pareja_Model::pertenece($enfrentamiento->pareja2,$_SESSION['userid'])){
				$pareja = Pareja_Model::get($enfrentamiento->pareja2);
				$parejaRival = Pareja_Model::get($enfrentamiento->pareja1);
			}else{
				exit;
			}
			$pareja = mysqli_fetch_object($pareja);
			$parejaRival = mysqli_fetch_object($parejaRival);
			$capitan = $pareja->capitan;
			$capitanRival = $parejaRival->capitan;
			require_once "Funciones/consoleLogger.php";

			$chatPrevio = Chat_Model::Exists($capitan,$capitanRival);

		
			if($chatPrevio != -1){
				$chatId = $chatPrevio;
				$chatFile = Chat_Model::getFilePath($chatPrevio);
				$content = file_get_contents($chatFile);
			}else{
				$chatId = Chat_Model::createChat($_SESSION['userid'],$capitanRival,$file);
				require_once "Modelos/User_Model.php";
				$user1 =  USUARIOS_Model::get($capitanRival);
				$user1 = mysqli_fetch_object($user1);
				$user2 =  USUARIOS_Model::get($parejaRival->acomp);
				$user2 = mysqli_fetch_object($user2);
				$content = " <h3>Chat con: ".$user1->nombre." y  ".$user2->nombre ."</h3><div class='mensaje'> <p>Hora: ".date("Y/m/d h:i:sa")."</p> <p>Mensaje: Chat has been created</p>
				<p>Sender: System </p></div>";
				file_put_contents($file,$content,FILE_APPEND);
			}
			require_once "Vistas/Chat_View.php";
				new Chat($chatId,$content);
			
		break;
		case "notify":

		break;
		case "markAsread":
			Chat_Model::removeNotification($_GET['notifId']);
			
		break;
		case "listConversations":
		default:
			$chatConversations = Chat_Model::getChatsByUser($_SESSION['userid']);
		break;
	}

?>
