<?php

$user = isset($_SESSION['userid']) ? $_SESSION['userid'] : 0;

if(!isset($_GET['action'])) {
    $_GET['action'] = "list";
}

if(isset($_GET['action'])) {
    if($_GET['action'] == "list") {
        
        require_once('Modelos/Reserva_Model.php');
        require_once('Modelos/Pista_Model.php');
        
        $reservas = Reserva_Model::getByCreador($user);
        
        foreach($reservas as $r) {
            $r->pista = Pista_Model::get($r->pista_id);
        }

        require_once('Vistas/reserva_list.php');
        new ReservaList($reservas);
    } else if($_GET['action'] == "add") {
        require_once('Modelos/Pista_Model.php');
        
        $pistas = Pista_Model::getAll();
        require_once('Modelos/Reserva_Model.php');
        $reservas = Reserva_Model::getWeek();

        require_once('Vistas/reserva_add.php');
        new ReservaAdd($pistas,$reservas);
    } else if($_GET['action'] == "add_confirm") {
        if(!isset($_POST['pista']) || !isset($_POST['selected']) || !isset($_POST['diaData'])) {
            echo "Error: campos incorrectos\n";
            exit;
        }

        require_once('Modelos/Pista_Model.php');
        require_once('Modelos/Reserva_Model.php');
        
        $pista = Pista_Model::get($_POST['pista']);
        $day = getdate(strtotime("+".$_POST['diaData']." day"));
        $dia = $day['year']."-".$day['mon']."-".$day['mday']." 00:00:00";
        $reserva = new Reserva_Model(0,
                                     $dia,
                                     $_POST['selected'],
                                     $pista->id,
                                     $user,
                                     1);
        
        if($reserva->save() == FALSE) {
            echo "Error al insertar reserva";
        }
        header("Location: /?controller=Reserva&action=list", TRUE, 301);
    } else if($_GET['action'] == "delete" && isset($_GET['id'])) {
        require_once('Modelos/Reserva_Model.php');

        $id = $_GET['id'];
        $reserva = Reserva_Model::getById($id);
        $reserva->pista = Pista_Model::get($reserva->pista_id);
        
        require_once('Vistas/reserva_delete.php');
    } else if($_GET['action'] == "delete_confirm" && isset($_GET['id'])) {
        require_once('Modelos/Reserva_Model.php');

        $reserva = Reserva_Model::getById($_GET['id']);
        $reserva->delete();
        header("Location: /?controller=Reserva&action=list", TRUE, 301);
    }
}

    

?>
