<?php

/**
 * Cargar la sesión.
 */
session_start();
	if(isset($_SESSION['userid'])){
		header('Location:/?controller=Front',TRUE,301); 
	}
	//No se está registrando, le mostramos la vista
	if(!isset($_POST['dni']) && (!isset($_POST['email']) || !(isset($_POST['pass'])))){
		require_once 'Vistas/RegisterView.php';
		new Register();
	}else{//Se está registrando
		/**
		 * Realiza una query a la BD para registrar al nuevo usuario
		 */
		require_once 'Modelos/User_Model.php';
		if($_POST['tipo'] == 'A'){
			//Error no puede registrarse como admin
			require_once 'Vistas/error.php';
			new CustomError("REGISTERERRORADMIN");
		}else{
			$usuario = new USUARIOS_Model($_POST['email'],md5($_POST['pass']),$_POST['edad'],$_POST['nombre'],$_POST['genero'],$_POST['dni'],$_POST['tipo']);

			$respuesta = $usuario->register();

			if($respuesta === true){

				header('Location:/?controller=Front',TRUE,301); 
				
			} else {

				require_once 'Vistas/error.php';
				new CustomError("REGISTERERROR");
			}
		}
		
	}

?>
