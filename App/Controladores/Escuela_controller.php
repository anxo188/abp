<?php  
	//session_start();
	if(!isset($_GET['action'])){
		$_GET['action']="list";

	}

	switch($_GET['action']){
		case "add":
			

			require_once 'Vistas/Escuela_ADD.php';
			require_once 'Modelos/User_Model.php';
			$respuestaEntrenador = USUARIOS_Model::getEntrenador();
			new Escuela_Add($respuestaEntrenador);

		break;

		case "add_Confirm":
			require_once 'Funciones/isAdmin.php';

			if(isAdmin())
			{

				require_once 'Modelos/Escuela_Model.php';

				$escuelaM = new Escuela_Model($_REQUEST['nombre'],$_REQUEST['telefono'],$_REQUEST['direccion']);
				$respuesta = $escuelaM->add();

				
				if( isset($_GET['debug']) && $_GET['debug'] == 777){

					require_once 'Vistas/mensaje.php';
					new CustomMessage($respuesta);

				}else{ 
					
				if($respuesta == true){

					require_once 'Vistas/mensaje.php';
					new CustomMessage("Escuela deportiva añadida correctamente");
					
				}else{
						require_once 'Vistas/mensaje.php';
						new CustomMessage("No se pudo añadir la escuela deportiva");
					}
				}
				
			}else{
				header("HTTP/1.0 403 Forbidden");
			}
			break;

		case "delete":
			require_once 'Funciones/isAdmin.php';
			if(isAdmin())
			{
				require_once 'Modelos/Escuela_Model.php';
				$respuesta = Escuela_Model::delete($_GET['idEscuela']);
				
				if($respuesta == true)
				{
					require_once 'Vistas/mensaje.php';
					new CustomMessage("Escuela deportiva borrada correctamente");
				}else{ 
					if( isset($_GET['debug']) && $_GET['debug'] == 777){

						require_once 'Vistas/mensaje.php';
						new CustomMessage($respuesta);
					}else
					{
						require_once 'Vistas/mensaje.php';
						new CustomMessage("No se pudo borrar la escuela deportiva");
					}
				}
			}else{
				header("HTTP/1.0 403 Forbidden");
			}		
			break;


			//Falta por acabar.
		case "edit":
			require_once 'Funciones/isAdmin.php';
			
			if(isAdmin())

			{
				require_once 'Modelos/Escuela_Model.php';
				$respuesta = Escuela_Model::get($_GET['idEscuela']);

				if(isset($_GET['debug']) && $_GET['debug'] == 777){

					require_once 'Vistas/mensaje.php';
					new CustomMessage($respuesta);

				}else if($respuesta == false){

					require_once 'Vistas/mensaje.php';
					new CustomMessage("No se pudo encontrar la escuela deportiva");

				}else{
					require_once 'Vistas/Escuela_Edit.php';
					new Escuela_Edit($respuesta);
				}
			}else{
				header("HTTP/1.0 403 Forbidden");
			}	
		break;

		case "edit_Confirm":
			require_once 'Funciones/isAdmin.php';
			if(isAdmin())
			{
			require_once 'Modelos/Escuela_Model.php';
				$escuelaM = new Escuela_Model($_REQUEST['nombre'],$_REQUEST['telefono'],$_REQUEST['direccion']);

				$respuesta = $escuelaM->edit($_REQUEST['idEscuela']);
				if($respuesta == true)
				{
					require_once 'Vistas/mensaje.php';
					new CustomMessage("Escuela deportiva editada correctamente");
				}else{ 
					if( isset($_GET['debug']) && $_GET['debug'] == 777){

						require_once 'Vistas/mensaje.php';
						new CustomMessage($respuesta);
					}else{
						require_once 'Vistas/mensaje.php';
						new CustomMessage("No se pudo editar la escuela deportiva");
					}
				}
			}else{
				header("HTTP/1.0 403 Forbidden");
			}	
			break;

		case "list":
		require_once 'Funciones/isAdmin.php';
		
		
					require_once 'Modelos/Escuela_Model.php';
					require_once 'Vistas/Escuela_Listado.php';
					$respuesta = Escuela_Model::getAll();

					
					if(isset($_GET['debug']) && $_GET['debug'] == 777){

						require_once 'Vistas/mensaje.php';
						new CustomMessage($respuesta);

					}else{

						
						new Escuela_Listado($respuesta);
					}
			break;
		
		
	}

?>