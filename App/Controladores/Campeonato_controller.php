<?php  
	//session_start();
	if(!isset($_GET['action'])){
		$_GET['action']="list";

	}

	switch($_GET['action']){
		case "add":
			

			require_once 'Vistas/Campeonato_ADD.php';
			new Campeonato_Add();

		break;

		case "add_Confirm":
			require_once 'Funciones/isAdmin.php';
			
			if(isAdmin())
			{

				require_once 'Modelos/Campeonato_Model.php';
				require_once 'Modelos/Nivel_Model.php';
				require_once 'Modelos/Categoria_Model.php';
				$respuestaNivel = Nivel_Model::getAll();
				
				

				while($tuplaNivel =  mysqli_fetch_object($respuestaNivel)){
				$respuestaCategoria = Categoria_Model::getAll();
				while($tuplaCategoria =  mysqli_fetch_object($respuestaCategoria)){

				$campeonatoM = new Campeonato_Model($_REQUEST['nombre'],$_REQUEST['inicio'],$_REQUEST['fin'],$_REQUEST['normativa'],$tuplaNivel->id,$tuplaCategoria->id);
				$respuesta = $campeonatoM->add();
				}
				}


				
				if( isset($_GET['debug']) && $_GET['debug'] == 777){

					require_once 'Vistas/mensaje.php';
					new CustomMessage($respuesta);

				}else{ 
				if($respuesta == true){

					require_once 'Vistas/mensaje.php';
					new CustomMessage("Campeonato añadido correctamente");
					
				}else{
						require_once 'Vistas/mensaje.php';
						new CustomMessage("No se pudo añadir el campeonato");
					}
				}
				
			}else{
				header("HTTP/1.0 403 Forbidden");
			}
			break;

		case "delete":
			require_once 'Funciones/isAdmin.php';
			if(isAdmin())
			{
				require_once 'Modelos/Campeonato_Model.php';
				$respuesta = Campeonato_Model::delete($_GET['idCampeonato']);
				
				if($respuesta == true)
				{
					require_once 'Vistas/mensaje.php';
					new CustomMessage("Campeonato borrado correctamente");
				}else{ 
					if( isset($_GET['debug']) && $_GET['debug'] == 777){

						require_once 'Vistas/mensaje.php';
						new CustomMessage($respuesta);
					}else
					{
						require_once 'Vistas/mensaje.php';
						new CustomMessage("No se pudo borrar el campeonato");
					}
				}
			}else{
				header("HTTP/1.0 403 Forbidden");
			}		
			break;


			//Falta por acabar.
		case "edit":
			require_once 'Funciones/isAdmin.php';
			
			if(isAdmin())

			{
				require_once 'Modelos/Campeonato_Model.php';
				$respuesta = Campeonato_Model::get($_GET['idCampeonato']);

				if(isset($_GET['debug']) && $_GET['debug'] == 777){

					require_once 'Vistas/mensaje.php';
					new CustomMessage($respuesta);

				}else if($respuesta == false){

					require_once 'Vistas/mensaje.php';
					new CustomMessage("No se pudo encontrar el campeonato");

				}else{
					require_once 'Vistas/Campeonato_Edit.php';
					require_once 'Modelos/Nivel_Model.php';
					require_once 'Modelos/Categoria_Model.php';
					$respuestaNivel = Nivel_Model::getAll();
					$respuestaCategoria = Categoria_Model::getAll();
					new Campeonato_Edit($respuesta,$respuestaNivel,$respuestaCategoria);
				}
			}else{
				header("HTTP/1.0 403 Forbidden");
			}	
		break;

		case "edit_Confirm":
			require_once 'Funciones/isAdmin.php';
			if(isAdmin())
			{
			require_once 'Modelos/Campeonato_Model.php';
				$campeonatoM = new Campeonato_Model($_REQUEST['nombre'],$_REQUEST['inicio'],$_REQUEST['fin'],$_REQUEST['normativa'],$_REQUEST['nivel'],$_REQUEST['categoria']);

				$respuesta = $campeonatoM->edit($_REQUEST['idCampeonato']);
				if($respuesta == true)
				{
					require_once 'Vistas/mensaje.php';
					new CustomMessage("Campeonato editado correctamente");
				}else{ 
					if( isset($_GET['debug']) && $_GET['debug'] == 777){

						require_once 'Vistas/mensaje.php';
						new CustomMessage($respuesta);
					}else{
						require_once 'Vistas/mensaje.php';
						new CustomMessage("No se pudo editar el campeonato");
					}
				}
			}else{
				header("HTTP/1.0 403 Forbidden");
			}
			break;

		case "list":
		require_once 'Funciones/isAdmin.php';
		
		
					require_once 'Modelos/Campeonato_Model.php';
					require_once 'Vistas/Campeonato_Listado.php';
					$respuesta = Campeonato_Model::getAll();
					$time = time();
					$fechaActual = date("Y-m-d ", $time).date("H:i:s", $time);
					
					if(isset($_GET['debug']) && $_GET['debug'] == 777){

						require_once 'Vistas/mensaje.php';
						new CustomMessage($respuesta);

					}else{

						
						new Campeonato_Listado($respuesta,$fechaActual);
					}
			break;
		
		
	}

?>