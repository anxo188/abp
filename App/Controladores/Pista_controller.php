<?php  
	
	if(!isset($_GET['action'])){
		$_GET['action']="list";
	}
	switch($_GET['action']){
		case "add":
			
			require_once 'Funciones/isAdmin.php';
			if(isAdmin())
			{
		
				
				if(isset($_POST['nombre']) && isset($_POST['descripcion']) && isset($_POST['inicio']) && isset($_POST['fin']) ){
					
					require_once 'Modelos/Pista_Model.php';
					$pistaM = new Pista_Model($_POST['nombre'],$_POST['descripcion'],$_POST['inicio'],$_POST['fin'],0);
					$respuesta = $pistaM->add();
					
					
					if($respuesta == true)
					{
						console_log("Pista registrada con éxito");
						require_once 'Vistas/Pista_Vista.php';
						new Pista($pistaM);
					}else{ 
						if( isset($_GET['debug']) && $_GET['debug'] == 777){

							require_once 'Vistas/mensaje.php';
							new CustomMenssage($respuesta);
						}else{
							require_once 'Vistas/error.php';
							new CustomError("No se pudo añadir la pista");
						}
					
					}
				}else{
					require_once 'Vistas/PistaAddView.php';
					new PistaAdd();
				}
			}else{
				
				require_once 'Vistas/error.php';
				new CustomError("No tiene permisos para realizar esta acción");
				header("HTTP/1.0 403 Forbidden");
			}
			break;

		case "delete":
			
			require_once 'Funciones/isAdmin.php';
			if(isAdmin())
			{

				require_once 'Modelos/Pista_Model.php';
				$respuesta = Pista_Model::delete($_POST['idPista']);
				if($respuesta == true)
				{
					header("Location: /?controller=Pista&action=list");
				
				}else{ 
					if( isset($_GET['debug']) && $_GET['debug'] == 777){

						require_once 'Vistas/mensaje.php';
						new CustomMessage($respuesta);
					}else
					{
						require_once 'Vistas/error.php';
						new CustomError("No se pudo borrar la pista");
					}
				}
			}else{
				require_once 'Vistas/error.php';
				new CustomError("No tiene permisos para realizar esta acción");
				header("HTTP/1.0 403 Forbidden");
			}			
			break;

		case "view":
			
			require_once 'Modelos/Pista_Model.php';
				if(isset($_POST['idPista'])){
					require_once 'Vistas/Pista_Vista.php';
					$resultado = Pista_Model::get($_POST['idPista']);
					
					if(isset($_GET['debug']) && $_GET['debug'] == 777){

						require_once 'Vistas/mensaje.php';
						new CustomMessage($resultado);

					}else if($resultado == false || $resultado == null){
						console_log("Error al cargal la pista");
						require_once 'Vistas/error.php';
						new CustomError("No se pudo encontrar la pista");

					}else{
					
						require_once 'Vistas/Pista_Vista.php';
						new Pista($resultado);
					}
				}else{

					header("Location:/?controller=Pista&action=list");
				}
			break;
		case "edit":
			require_once 'Funciones/isAdmin.php';
			if(isAdmin())
			{
				require_once 'Modelos/Pista_Model.php';
				console_log($_POST);
				if(isset($_POST['nombre']) && isset($_POST['descripcion']) && isset($_POST['inicio']) && isset($_POST['fin']) && isset($_POST['id'])){
				
					require_once 'Modelos/Pista_Model.php';
						$pistaM = new Pista_Model($_POST['nombre'],$_POST['descripcion'],$_POST['inicio'],$_POST['fin'],$_POST['id']);
						$respuesta = $pistaM->edit();
						console_log($respuesta);
						require_once 'Vistas/Pista_Vista.php';
						new Pista($pistaM);
				}else{
					$resultado = Pista_Model::get($_POST['idPista']);
					require_once 'Vistas/PistaEdit_View.php';
					new PistaEdit($resultado);
				}
			}
			break;
		case "list":
		default:
		
			require_once 'Modelos/Pista_Model.php';

					require_once 'Vistas/Pista_ShowAll.php';
					$resultado = Pista_Model::getAll();
					
					if(isset($_GET['debug']) && $_GET['debug'] == 777){

						require_once 'Vistas/MensajeDebug.php';
						new MensajeDebug($resultado);

					}else if($resultado == false){

						require_once 'Vistas/Pista_ShowAll.php';
						new Pistas(null);

					}else{
						require_once 'Vistas/Pista_ShowAll.php';
						new Pistas($resultado);
					}

	}

?>
