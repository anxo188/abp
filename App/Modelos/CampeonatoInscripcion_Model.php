<?php
class CampeonatoInscripcion_Model {

	var $id;

	/**
	 * Id del campeonato
	 */
	var $campeonatoID;
	
	/**
	 * Id de la pareja
	 */
	var $pareja;
	
	
	/**
	 * Conexión con la BD usada
	 */
	var $mysqli;


//Constructor de la clase
function __construct($campeonatoID,$pareja){
     


	$this->campeonatoID = $campeonatoID;
	$this->pareja = $pareja;



}



//funcion de destrucción del objeto: se ejecuta automaticamente
//al finalizar el script
function __destruct()
{

}

function add(){

	require_once 'Modelos/BDConector.php';
	$mysqli = BDConector::createConection();
	$time = time();
	$fechaActual = date("Y-m-d ", $time).date("H:i:s", $time);


	$stmt = $mysqli->prepare("SELECT * FROM CAMPEONATO WHERE id=?");
	$stmt->bind_param("i",$this->campeonatoID);
	$stmt->execute();
	$resultado = mysqli_stmt_get_result($stmt);

	if($resultado->num_rows == 1){
		$tupla = $resultado->fetch_array();
	
		if($tupla['inicio']<=$fechaActual){
			
			
			return false;
		}
	}

	$stmt = $mysqli->prepare("SELECT * FROM INSCRIPCIONCAMPEONATO WHERE (campeonatoID=? AND pareja=?)");
	$stmt->bind_param("ii",$this->campeonatoID,$this->pareja);
	$stmt->execute();
	$resultado = mysqli_stmt_get_result($stmt);
	
	if($resultado->num_rows == 0){
		
		$insertStmt = $mysqli->prepare("INSERT INTO INSCRIPCIONCAMPEONATO ( campeonatoID,pareja) VALUES(?,?)");
		$insertStmt->bind_param("ii",$this->campeonatoID,$this->pareja);

		$resultado = $insertStmt->execute();

		if($resultado == true){
			return true;
		}else if($resultado == false){
			return false;
		}else{
			return $mysqli->error;
		}
	}else if($resultado->num_rows >= 1){
		return "La pareja ya esta inscrita";
	}else{
		return $mysqli->error;
	}
	$mysqli->close();

}

//funcion DELETE : comprueba que la tupla a borrar existe y una vez verificado la borra
function delete($id){

	require_once 'Modelos/BDConector.php';
	$mysqli = BDConector::createConection();
	$stmt = $mysqli->prepare("DELETE FROM INSCRIPCIONCAMPEONATO WHERE id=?");
	$stmt->bind_param("i",$id);
	$resultado = $stmt->execute();

	$mysqli->close();
	if($resultado == true){
			return true;
	}else if($resultado == false){
		return false;
	}else{
		return $mysqli->error;
	}
}//Fin delete


function getParejaByIdCampeonato($id){
	require_once 'Modelos/BDConector.php';
	$mysqli = BDConector::createConection();

	$stmt = $mysqli->prepare("SELECT * FROM INSCRIPCIONCAMPEONATO WHERE campeonatoID= ?");
	$stmt->bind_param("i",$id);
	$stmt->execute();
	$resultado = mysqli_stmt_get_result($stmt);

	
	
	if($resultado->num_rows >= 1){
		return $resultado;
	}else if($resultado->num_rows == 0){
		return false;
	}else{
		return $mysqli->error;
	}
	$mysqli->close();
}

function getCampeonatosByIdPareja($id){
	require_once 'Modelos/BDConector.php';
	$mysqli = BDConector::createConection();

	$stmt = $mysqli->prepare("SELECT * FROM INSCRIPCIONCAMPEONATO WHERE pareja= ?");
	$stmt->bind_param("i",$id);
	$stmt->execute();
	$resultado = mysqli_stmt_get_result($stmt);

	
	
	if($resultado->num_rows >= 1){
		return $resultado;
	}else if($resultado->num_rows == 0){
		return false;
	}else{
		return $mysqli->error;
	}
	$mysqli->close();
}
public static function getParejas($idCampeonato){
	  require_once 'Modelos/BDConector.php';
	  $mysqli = BDConector::createConection();
  	  $stmt = $mysqli->prepare("SELECT * FROM INSCRIPCIONCAMPEONATO WHERE campeonatoID = ?");
	 var_dump($mysqli->error);
	  $stmt->bind_param("i",$idCampeonato);
	  $stmt->execute();
	  $resultado = mysqli_stmt_get_result($stmt);

	  if($resultado->num_rows >= 1){
		  return $resultado;
	  }else if($resultado->num_rows == 0){
		  return false;
	  }else{
		  return $mysqli->error;
	  }
	  $mysqli->close();
}

public static function getCampeonatos($usuario){


	require_once 'Modelos/BDConector.php';
	$mysqli = BDConector::createConection();
    $sql = "SELECT I.id, C.nombre, C.inicio, C.fin, C.normativa, C.nivel, C.categoria
        FROM INSCRIPCIONCAMPEONATO I, CAMPEONATO C , PAREJA P
        WHERE (P.capitan = '$usuario' OR P.acomp = '$usuario')  AND P.id = I.pareja AND I.campeonatoID = C.id";

        $resultado = $mysqli->query($sql);

        return $resultado;
	
}
	}//Register end


?>
