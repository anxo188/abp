<?php
class USUARIOS_Model {

	
	/**
	 * Contraseña del usuario
	 */
	var $pass;

	/**
	 * Email del usuario (PK en la BD)
	 */
	var $email;
	var $nombre;
	var $edad;
	var $genero;
	var $dni;
	var $tipo;

	/**
	 * Conexión con la BD usada
	 */
	var $mysqli;


//Constructor de la clase
function __construct($email,$pass,$edad,$nombre,$genero,$dni,$tipo){
        	
	$this->email = $email;
	$this->pass = $pass;
	$this->edad = $edad;
	$this->nombre = $nombre;
	$this->genero = $genero;
	$this->dni = $dni;
	$this->tipo = $tipo;

	require_once 'Modelos/BDConector.php';
	$this->mysqli = BDConector::createConection();
}



//funcion de destrucción del objeto: se ejecuta automaticamente
//al finalizar el script
function __destruct()
{
	
}

/**
 * Comprueba si el usuario existe y la pass proporcionada es correcta
 * @return int | String
 */
	function login(){
		$stmt = $this->mysqli->prepare("SELECT * FROM USER WHERE (email = ? OR dni = ?) AND password = ?");
		$stmt->bind_param("sss",$this->email,$this->dni,$this->pass);

		$stmt->execute();
		$resultado = $stmt->get_result();
		if ($resultado->num_rows == 0){
			return 'El usuario o contraseña es incorrecto';
		}
		else if ($resultado->num_rows == 1 ){
				$tupla = $resultado->fetch_array();
				 return $tupla['id'];
		}else{
			return $mysqli->error;
		}
	}//Login end

	/**
	 * Comprueba que el usuario a añadir no exista y en dicho caso lo añade
	 * @return String | boolean
	 */
	function register(){
		
		$stmt = $this->mysqli->prepare("SELECT * FROM USER WHERE (email = ? OR dni = ?)");
		$stmt->bind_param("ss",$this->email,$this->dni);

		$stmt->execute();
		$resultado = $stmt->get_result();
		if ($resultado->num_rows == 0){

			$insertStmt = $this->mysqli->prepare("INSERT INTO USER ( email,password,edad,nombre,genero,dni,tipo) VALUES(?,?,?,?,?,?,?)");
			$insertStmt->bind_param("ssissss",$this->email, 
											$this->pass, 
											$this->edad, 
											$this->nombre, 
											$this->genero, 
											$this->dni, 
											$this->tipo
										);

			$insertStmt->execute();
			$resultado = $insertStmt->get_result();
			$stmt = $this->mysqli->prepare("SELECT * FROM USER WHERE (email = ? OR dni = ?)");
			$stmt->bind_param("ss",$this->email,$this->dni);
			$stmt->execute();
			$resultado = $stmt->get_result();
			if ($resultado->num_rows == 0){
				return 'No se pudo insertar el usuario';
			}
			else if($resultado->num_rows == 1){
				return true;
			}else{
				return $this->mysqli->error;
			}
		}else if($resultado->num_rows == 0){
			return "El usuario ya existe";
		}else{
			return $this->mysqli->error;
			
		}
	}//Register end
// funcion getAll: recupera todas las tuplas
public static function getAll(){
	require_once 'Modelos/BDConector.php';
	$mysqli = BDConector::createConection();

	$stmt = $mysqli->prepare("SELECT * FROM USER");
	
	$stmt->execute();
	$resultado = mysqli_stmt_get_result($stmt);
	$mysqli->close();
	if($resultado->num_rows >= 1){
	return $resultado;
	}else if($resultado->num_rows == 0){
		return false;
	}else{
		return $mysqli->error;
	}
	$mysqli->close();
}//Fin getAll

	public static function get($id){
		require_once 'Modelos/BDConector.php';
		$mysqli = BDConector::createConection();

		$stmt = $mysqli->prepare("SELECT * FROM USER WHERE id=?");
		$stmt->bind_param("i",$id);
		$stmt->execute();
		$resultado = mysqli_stmt_get_result($stmt);
		$mysqli->close();
		if($resultado->num_rows >= 1){
		return $resultado;
		}else if($resultado->num_rows == 0){
			return false;
		}else{
			return $mysqli->error;
		}
	}
	function getId(){
		$stmt = $mysqli->prepare("SELECT id FROM USER WHERE email=? AND password=?");
		$stmt->bind_param("ss",$this->email,$this->pass);
		$resultado = $stmt->execute();

		if($resultado->num_rows == 1){
			$tmp = mysqli_fetch_object($resultado);
			return $tmp->id;
		}else if($resultado->num_rows == 0){
			return false;
		}else{
			return $mysqli->error;
		}
	}//Fin getId

	public static function isAdmin($id){

		require_once 'Modelos/BDConector.php';
		$mysqli = BDConector::createConection();

		$stmt = $mysqli->prepare("SELECT tipo FROM USER WHERE id=?");
		$stmt->bind_param("i",$id);
		$stmt->execute();
		$resultado = $stmt->get_result();
		$mysqli->close();		
		if($resultado->num_rows == 1){
			$tmp =  mysqli_fetch_object($resultado);
			if($tmp->tipo == 'A'){
				return true;
			}
			return false;
		}else if($resultado->num_rows == 0){
			return false;
		}else{
			return $mysqli->error;
		}
	}

	public static function isEntrenador($id){

		require_once 'Modelos/BDConector.php';
		$mysqli = BDConector::createConection();

		$stmt = $mysqli->prepare("SELECT tipo FROM USER WHERE id=?");
		$stmt->bind_param("i",$id);
		$stmt->execute();
		$resultado = $stmt->get_result();
		$mysqli->close();		
		if($resultado->num_rows == 1){
			$tmp =  mysqli_fetch_object($resultado);
			if($tmp->tipo == 'E'){
				return true;
			}
			return false;
		}else if($resultado->num_rows == 0){
			return false;
		}else{
			return $mysqli->error;
		}
	}


	public static function exists($id){
		require_once 'Modelos/BDConector.php';
		$mysqli = BDConector::createConection();
		$stmt = $mysqli->prepare("SELECT * FROM USER WHERE id=?");
		$stmt->bind_param("i",$id);
		$stmt->execute();
		$resultado = $stmt->get_result();
		$mysqli->close();
		if($resultado->num_rows == 1){
			return true;
		}else{
			return false;
		}
	}

	public static function getEntrenador(){
		require_once 'Modelos/BDConector.php';
		$mysqli = BDConector::createConection();

		$stmt = $mysqli->prepare("SELECT * FROM USER WHERE tipo='E'");
		
		$stmt->execute();
		$resultado = mysqli_stmt_get_result($stmt);
		$mysqli->close();
		if($resultado->num_rows >= 1){
		return $resultado;
		}else if($resultado->num_rows == 0){
			return false;
		}else{
			return $mysqli->error;
		}
		$mysqli->close();
	}


}//Class end

?>
