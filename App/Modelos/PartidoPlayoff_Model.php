<?php

require_once 'Modelos/BDConector.php';

class PartidoPlayoff_Model {

    public $playoff_id;
    public $enfrentamiento_id;
    public $fase;

	/**
	 * Conexión con la BD usada
	 */
	var $mysqli;

    function __construct($playoff_id, $enfrentamiento_id, $fase) {
        $this->playoff_id = $playoff_id;
        $this->enfrentamiento_id = $enfrentamiento_id;
        $this->fase = $fase;
        
        $this->mysqli = BDConector::createConection();
    }

    public function save() {
        $stmt = $this->mysqli->prepare("INSERT INTO PARTIDOSPLAYOFF VALUES (?,?,?)");
        $stmt->bind_param("iii",
                          $this->playoff_id,
                          $this->enfrentamiento_id,
                          $this->fase);
        return $stmt->execute();
    }

    public static function getByPlayoff($playoff_id) {
        $mysqli = BDConector::createConection();

        $stmt = $mysqli->prepare("SELECT * FROM PARTIDOSPLAYOFF WHERE (playoff = ?)");
        $stmt->bind_param("i", $playoff_id);
        $res = $stmt->execute();

        if($res->num_rows == 0)
            return NULL;

        $tupla = $res->fetch_array();
        return new PartidoPlayoff_Model($tupla['playoff'],
                                        $tupla['enfrent'],
                                        $tupla['fase']);
    }

    public static function getByFase($playoff_id, $fase) {
        $mysqli = BDConector::createConection();
        
        $stmt = $mysqli->prepare("SELECT * FROM PARTIDOSPLAYOFF WHERE (playoff = ? AND fase = ?)");
        $stmt->bind_param("ii", $playoff_id, $fase);
        $stmt->execute();
        $res = $stmt->get_result();
        
        if($res->num_rows == 0) {
            return array();
        }
        
        $resultado = array();
        while($tupla = $res->fetch_array()) {
            $pp = new PartidoPlayoff_Model($tupla['playoff'],
                                           $tupla['enfrent'],
                                           $tupla['fase']);
            array_push($resultado, $pp);
        }
    }

    function __destruct()
    {
        // vacia
    }
}

?>
