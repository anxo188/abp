<?php
class Clase_Model {

	var $id;

	/**
	 * id de la escuela donde se impartira la clase
	 */
	var $escuela;
	
	/**
	 * id del entrenador que impartira la clase
	 */
	var $entrenador;
	
	/**
	 * id de la reserva donde se impartira la clase
	 */
	var $reserva;
	/**
	 * Conexión con la BD usada
	 */
	var $mysqli;


//Constructor de la clase
    function __construct($escuela,$entrenador,$reserva){
     


	$this->escuela = $escuela;
	$this->entrenador = $entrenador;
	$this->reserva = $reserva;
 

}



//funcion de destrucción del objeto: se ejecuta automaticamente
//al finalizar el script
function __destruct()
{

}

//Metodo add : Inserta en la tabla de la bd los valores de los atributos del objeto.
function add(){

				require_once 'Modelos/BDConector.php';
				$mysqli = BDConector::createConection();
				$stmt = $mysqli->prepare("INSERT INTO CLASES (
					idEscuela,idEntrenador,idReserva) VALUES (?,?,?)");
				$stmt->bind_param("iii",$this->escuela,$this->entrenador,$this->reserva);
				$resultado = $stmt->execute();

				$mysqli->close();
				
				if($resultado == true){

					return true;
				}else if($resultado == false){

					return false;
				}else{
					return $mysqli->error;
				}
	}//Fin add


//funcion DELETE : comprueba que la tupla a borrar existe y una vez verificado la borra
public static function delete($id){

	require_once 'Modelos/BDConector.php';
	$mysqli = BDConector::createConection();
	$stmt = $mysqli->prepare("DELETE FROM CLASES WHERE id=?");
	$stmt->bind_param("i",$id);
	$resultado = $stmt->execute();

	$mysqli->close();
	if($resultado == true){
			return true;
	}else if($resultado == false){
		return false;
	}else{
		return $mysqli->error;
	}
}//Fin delete

// funcion getAll: recupera todas las tuplas
public static function getClaseByEscuela($idEscuela){
	require_once 'Modelos/BDConector.php';
	$mysqli = BDConector::createConection();

	$stmt = $mysqli->prepare("SELECT * FROM CLASES WHERE idEscuela=?");
	$stmt->bind_param("i",$idEscuela);
	$stmt->execute();
	$resultado = mysqli_stmt_get_result($stmt);
	$mysqli->close();
	if($resultado->num_rows >= 1){
	return $resultado;
	}else if($resultado->num_rows == 0){
		return false;
	}else{
		return $mysqli->error;
	}
	$mysqli->close();
}//Fin getAll

// funcion getAll: recupera todas las tuplas
public static function getClaseByEntrenador($idEntrenador){
	require_once 'Modelos/BDConector.php';
	$mysqli = BDConector::createConection();

	$stmt = $mysqli->prepare("SELECT * FROM CLASES WHERE idEntrenador=?");
	$stmt->bind_param("i",$idEntrenador);
	$stmt->execute();
	$resultado = mysqli_stmt_get_result($stmt);
	$mysqli->close();
	if($resultado->num_rows >= 1){
	return $resultado;
	}else if($resultado->num_rows == 0){
		return false;
	}else{
		return $mysqli->error;
	}
	$mysqli->close();
}//Fin getAll

// funcion get: recupera una tupla a partir de su clave
public static function get($id){
	
	require_once 'Modelos/BDConector.php';
	$mysqli = BDConector::createConection();

	$stmt = $mysqli->prepare("SELECT * FROM CLASES WHERE id=?");
	$stmt->bind_param("i",$id);
	$stmt->execute();
	$resultado = mysqli_stmt_get_result($stmt);
	$mysqli->close();
	if($resultado->num_rows >= 1){
		return $resultado;
	}else if($resultado->num_rows == 0){
		return false;
	}else{
		return $mysqli->error;
	}

}//Fin get


	
	}//Register end


?>
