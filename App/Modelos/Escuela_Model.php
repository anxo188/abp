<?php
class Escuela_Model {

	var $id;

	/**
	 * Nombre de la escuela
	 */
	var $nombre;
	
	/**
	 * Telefono de de la escuela
	 */
	var $telefono;
		
	/**
	 * Direccion de la escuela
	 */
	var $direccion;
	
	/**
	 * Conexión con la BD usada
	 */
	var $mysqli;


//Constructor de la clase
    function __construct($nombre,$telefono,$direccion){
     


	$this->nombre = $nombre;
	$this->telefono = $telefono;
	$this->direccion = $direccion;
 

}



//funcion de destrucción del objeto: se ejecuta automaticamente
//al finalizar el script
function __destruct()
{

}

//Metodo add : Inserta en la tabla de la bd los valores de los atributos del objeto.
function add(){

		if(
			preg_match('/^(6|7|9)([0-9]){8}/',$this->telefono)
		){
				require_once 'Modelos/BDConector.php';
				$mysqli = BDConector::createConection();
				$stmt = $mysqli->prepare("INSERT INTO ESCUELA (
					nombre,telefono,direccion) VALUES (?,?,?)");
				$stmt->bind_param("sis",$this->nombre,$this->telefono,$this->direccion);
				$resultado = $stmt->execute();

				$mysqli->close();
				
				if($resultado == true){

					return true;
				}else if($resultado == false){

					return false;
				}else{
					return $mysqli->error;
				}

		}else{
			return "Formato de telefono incorrecto: \n".$this->telefono."\n";
		}
	}//Fin add


//funcion DELETE : comprueba que la tupla a borrar existe y una vez verificado la borra
public static function delete($id){

	require_once 'Modelos/BDConector.php';
	$mysqli = BDConector::createConection();
	$stmt = $mysqli->prepare("DELETE FROM ESCUELA WHERE id=?");
	$stmt->bind_param("i",$id);
	$resultado = $stmt->execute();

	$mysqli->close();
	if($resultado == true){
			return true;
	}else if($resultado == false){
		return false;
	}else{
		return $mysqli->error;
	}
}//Fin delete

// funcion getAll: recupera todas las tuplas
public static function getAll(){
	require_once 'Modelos/BDConector.php';
	$mysqli = BDConector::createConection();

	$stmt = $mysqli->prepare("SELECT * FROM ESCUELA");
	
	$stmt->execute();
	$resultado = mysqli_stmt_get_result($stmt);
	$mysqli->close();
	if($resultado->num_rows >= 1){
	return $resultado;
	}else if($resultado->num_rows == 0){
		return false;
	}else{
		return $mysqli->error;
	}
	$mysqli->close();
}//Fin getAll

// funcion get: recupera una tupla a partir de su clave
public static function get($id){
	
	require_once 'Modelos/BDConector.php';
	$mysqli = BDConector::createConection();

	$stmt = $mysqli->prepare("SELECT * FROM ESCUELA WHERE id=?");
	$stmt->bind_param("i",$id);
	$stmt->execute();
	$resultado = mysqli_stmt_get_result($stmt);
	$mysqli->close();
	if($resultado->num_rows >= 1){
		return $resultado;
	}else if($resultado->num_rows == 0){
		return false;
	}else{
		return $mysqli->error;
	}

}//Fin get

// funcion Edit: realizar el update de una tupla despues de comprobar que existe
function edit($id)
{

	require_once 'Modelos/BDConector.php';
	$mysqli = BDConector::createConection();

	$stmt = $mysqli->prepare("SELECT * FROM ESCUELA WHERE id=?");
	$stmt->bind_param("i",$id);
	$stmt->execute();
	$resultado = mysqli_stmt_get_result($stmt);
	if($resultado->num_rows == 1){
		if(
			preg_match('/^(6|7|9)([0-9]){8}/',$this->telefono) 
			){
			$stmt = $mysqli->prepare("UPDATE escuela SET nombre = ? ,telefono = ?,direccion = ?  WHERE id=? ");
			$stmt->bind_param("sii",$this->nombre,$this->telefono,$this->direccion,$id);
			$resultado = $stmt->execute();
			
			$mysqli->close();
			if($resultado == true){
				return true;
			}else if($resultado == false){
				return false;
			}else{
				return $mysqli->error;
			}

		}else{
			return "Formato de telefono incorrecto: \n".$this->telefono."\n";
		}
		
	}else if($resultado->num_rows == 0){
		return false;
	}else{
		return $mysqli->error;
	}


}


	
	}//Register end


?>
