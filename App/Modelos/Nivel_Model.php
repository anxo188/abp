<?php
class Nivel_Model {

	var $id;

	/**
	 * Nombre del nivel
	 */
	var $nombre;
	
	/**
	 * Descripcion del nivel
	 */
	var $descripcion;

	/**
	 * Conexión con la BD usada
	 */
	var $mysqli;


//Constructor de la clase
function __construct($nombre,$descripcion){
     


	$this->nombre = $nombre;
	$this->descripcion = $descripcion;



}



//funcion de destrucción del objeto: se ejecuta automaticamente
//al finalizar el script
function __destruct()
{

}


// funcion getAll: recupera todas las tuplas
public static function getAll(){
	
	require_once 'Modelos/BDConector.php';
	$mysqli = BDConector::createConection();

	$stmt = $mysqli->prepare("SELECT * FROM NIVEL");
	$stmt->execute();
	$resultado = mysqli_stmt_get_result($stmt);
	
	
	
	if($resultado->num_rows >= 1){
		return $resultado;
	}else if($resultado->num_rows == 0){
		return false;
	}else{
		return $mysqli->error;
	}
	$mysqli->close();
}//Fin getAll
	}//Register end


?>
