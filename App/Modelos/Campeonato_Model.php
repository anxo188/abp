<?php
class Campeonato_Model {

	var $id;

	/**
	 * Nombre del campeonato
	 */
	var $nombre;
	
	/**
	 * Horario de inicio del campeonato
	 */
	var $inicio;
	
	/**
	 * Horario de fin del campeonato
	 */
	var $fin;
	
	/**
	 * Normativa del campeonato
	 */
	var $normativa;
	
	/**
	 * Nivel que tiene el campeonato
	 */
	var $nivel;
	
	/**
	 * Categoria a la que pertenece el campeonato
	 */
	var $categoria;
	
	/**
	 * Conexión con la BD usada
	 */
	var $mysqli;


//Constructor de la clase
    function __construct($nombre,$inicio,$fin,$normativa,$nivel,$categoria){
     


	$this->nombre = $nombre;
	$this->inicio = $inicio;
	$this->fin = $fin;
	$this->normativa = $normativa;
	$this->nivel = $nivel;
	$this->categoria = $categoria;
 



}



//funcion de destrucción del objeto: se ejecuta automaticamente
//al finalizar el script
function __destruct()
{

}

//Metodo add : Inserta en la tabla de la bd los valores de los atributos del objeto.
function add(){

		if(
			preg_match('/^[0-9]{4}\-(0[1-9]|1[0-2])\-(0[1-9]|[1-2][0-9]|3[0-1])\s[0-9]{2}\:[0-9]{2}\:[0-9]{2}$/',$this->inicio) 
			&& 
			preg_match('/^[0-9]{4}\-(0[1-9]|1[0-2])\-(0[1-9]|[1-2][0-9]|3[0-1])\s[0-9]{2}\:[0-9]{2}\:[0-9]{2}$/',$this->fin)
		){
		$time = time();
		$fechaActual = date("Y-m-d ", $time).date("H:i:s", $time);
			if($this->inicio>$fechaActual){
				require_once 'Modelos/BDConector.php';
				$mysqli = BDConector::createConection();
				$stmt = $mysqli->prepare("INSERT INTO CAMPEONATO (
					nombre,inicio,fin,normativa,nivel,categoria) VALUES (?,?,?,?,?,?)");
				$stmt->bind_param("ssssii",$this->nombre,$this->inicio,$this->fin,$this->normativa,$this->nivel,$this->categoria);
				$resultado = $stmt->execute();
				$mysqli->close();
				if($resultado == true){
					return true;
				}else if($resultado == false){
					
					return false;
				}else{
					return $mysqli->error;
				}
			}else{
				
			return false; 
			
		}
		}else{
			return "Formato de fecha incorrecto: \n".$this->inicio."\n".$this->fin."\n";
		}
	}//Fin add


//funcion DELETE : comprueba que la tupla a borrar existe y una vez verificado la borra
public static function delete($id){

	require_once 'Modelos/BDConector.php';
	$mysqli = BDConector::createConection();
	$stmt = $mysqli->prepare("DELETE FROM CAMPEONATO WHERE id=?");
	$stmt->bind_param("i",$id);
	$resultado = $stmt->execute();

	$mysqli->close();
	if($resultado == true){
			return true;
	}else if($resultado == false){
		return false;
	}else{
		return $mysqli->error;
	}
}//Fin delete

// funcion getAll: recupera todas las tuplas
public static function getAll(){
	require_once 'Modelos/BDConector.php';
	$mysqli = BDConector::createConection();

	$stmt = $mysqli->prepare("SELECT * FROM CAMPEONATO");
	
	$stmt->execute();
	$resultado = mysqli_stmt_get_result($stmt);
	$mysqli->close();
	if($resultado->num_rows >= 1){
	return $resultado;
	}else if($resultado->num_rows == 0){
		return false;
	}else{
		return $mysqli->error;
	}
	$mysqli->close();
}//Fin getAll

// funcion get: recupera una tupla a partir de su clave
public static function get($id){
	
	require_once 'Modelos/BDConector.php';
	$mysqli = BDConector::createConection();

	$stmt = $mysqli->prepare("SELECT * FROM CAMPEONATO WHERE id=?");
	$stmt->bind_param("i",$id);
	$stmt->execute();
	$resultado = mysqli_stmt_get_result($stmt);
	$mysqli->close();
	if($resultado->num_rows >= 1){
		return $resultado;
	}else if($resultado->num_rows == 0){
		return false;
	}else{
		return $mysqli->error;
	}

}//Fin get


// funcion Edit: realizar el update de una tupla despues de comprobar que existe
function edit($id)
{

	require_once 'Modelos/BDConector.php';
	$mysqli = BDConector::createConection();

	$stmt = $mysqli->prepare("SELECT * FROM CAMPEONATO WHERE id=?");
	$stmt->bind_param("i",$id);
	$stmt->execute();
	$resultado = mysqli_stmt_get_result($stmt);
	if($resultado->num_rows == 1){
		if(
			preg_match('/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}\s[0-9]{2}\:[0-9]{2}\:[0-9]{2}$/',$this->inicio) 
			&& 
			preg_match('/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}\s[0-9]{2}\:[0-9]{2}\:[0-9]{2}$/',$this->fin)
		){
			$stmt = $mysqli->prepare("UPDATE CAMPEONATO SET nombre = ? ,inicio = ?,fin = ?,normativa = ?,nivel = ?,categoria = ?  WHERE id=? ");
			$stmt->bind_param("ssssiii",$this->nombre,$this->inicio,$this->fin,$this->normativa,$this->nivel,$this->categoria ,$id);
			$resultado = $stmt->execute();
			
			$mysqli->close();
			if($resultado == true){
				return true;
			}else if($resultado == false){
				return false;
			}else{
				return $mysqli->error;
			}

		}else{
			return "Formato de fecha incorrecto: \n".$this->inicio."\n".$this->fin."\n";
		}
		
	}else if($resultado->num_rows == 0){
		return false;
	}else{
		return $mysqli->error;
	}


}


	
	}//Register end


?>
