<?php

include_once('Modelos/BDConector.php');

class Contenido_Model {

    public $id;
    public $titulo;
    public $texto;
    public $creacion;
    public $creador_id;

	/**
	 * Conexión con la BD usada
	 */
	private $mysqli;

    function __construct($id, $titulo, $texto, $creacion, $creador_id) {
        $this->id = $id;
        $this->titulo = $titulo;
        $this->texto = $texto;
        $this->creacion = new DateTime($creacion);
        $this->creador_id = $creador_id;
        
        $this->mysqli = BDConector::createConection();
    }

    public function save() {
        $stmt = $this->mysqli->prepare("INSERT INTO CONTENIDO VALUES (?,?,?,?,?)");
        $hora = $this->creacion->format("Y-m-d H:i:s");
        $stmt->bind_param("isssi",
                          $this->id,
                          $this->titulo,
                          $this->texto,
                          $hora,
                          $this->creador_id);
        $ok = $stmt->execute();
        if(!$ok) return FALSE;
        $this->id = $this->mysqli->insert_id;
        return TRUE;
    }

    public function delete() {
        $stmt = $this->mysqli->prepare("DELETE FROM CONTENIDO WHERE id = ?");
        $stmt->bind_param("i", $this->id);
        return $stmt->execute();
    }

    public function update() {
        $stmt = $this->mysqli->prepare("UPDATE CONTENIDO " .
                                       "SET titulo = ?, texto = ?, " .
                                       "creacion = ?, creador = ? " .
                                       "WHERE id = ?");
        $stmt->bind_param("sssii",
                            $this->titulo,
                            $this->texto,
                          $this->creacion->format("Y-m-d H:i:s"),
                          $this->creador_id,
                          $this->id);
        return $stmt->execute();
    }

    public static function getByCreador($creador_id) {
        $mysqli = BDConector::createConection();

        $stmt = $mysqli->prepare("SELECT * FROM CONTENIDO WHERE (creador = ?)");
        $stmt->bind_param("i", $creador_id);
        $stmt->execute();
        $res = $stmt->get_result();

        if($res->num_rows == 0)
            return array();
        
        $resultado = array();

        while($tupla = $res->fetch_array()) {
            $t = new Contenido_Model($tupla['id'],
                                   $tupla['titulo'],
                                   $tupla['texto'],
                                   $tupla['creacion'],
                                   $tupla['creador']);
            array_push($resultado, $t);
        }
        $mysqli->close();
        return $resultado;
    }

    public static function getById($contenido_id) {
        $mysqli = BDConector::createConection();
        
        $stmt = $mysqli->prepare("SELECT * FROM CONTENIDO WHERE (id = ?)");
        $stmt->bind_param("i", $contenido_id);
        $stmt->execute();
        $res = $stmt->get_result();
        
        if($res->num_rows == 0) {
            return NULL;
        }
        $tupla = $res->fetch_array();
        $resultado =  new Contenido_Model($tupla['id'],
                                 $tupla['titulo'],
                                 $tupla['texto'],
                                 $tupla['creacion'],
                                 $tupla['creador']);
        $mysqli->close();
        return $resultado;
    }
    
    public static function getAll() {
        $mysqli = BDConector::createConection();

        $stmt = $mysqli->prepare("SELECT * FROM CONTENIDO ORDER BY creacion DESC");
        $stmt->execute();
        $res = $stmt->get_result();

        if($res->num_rows == 0)
            return array();
        
        $resultado = array();

        while($tupla = $res->fetch_array()) {
            $t = new Contenido_Model($tupla['id'],
                                   $tupla['titulo'],
                                   $tupla['texto'],
                                   $tupla['creacion'],
                                   $tupla['creador']);
            array_push($resultado, $t);
        }
        $mysqli->close();
        return $resultado;
    }
    
    function __destruct()
    {
        // vacia
    }
}

?>
