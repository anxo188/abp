<?php

require_once 'Modelos/BDConector.php';
require_once 'Modelos/Partido_Model.php';
require_once 'Modelos/Reserva_Model.php';

class Playoff_Model {

    public $id;
    public $comienzo;
    public $campeonato_id;
    public $campeonato;
    public $enfrentamientos;

	/**
	 * Conexión con la BD usada
	 */
	private $mysqli;

    function __construct($id, $comienzo, $campeonato_id) {
        $this->id = $id;
        $this->comienzo = new DateTime($comienzo);
        $this->campeonato_id = $campeonato_id;
        
        $this->mysqli = BDConector::createConection();
    }

    public function save() {
        $stmt = $this->mysqli->prepare("INSERT INTO PLAYOFF VALUES (?,?,?)");
        $comienzo = $this->comienzo->format("Y-m-d H:i:s");
        $stmt->bind_param("isi",
                          $this->id,
                          $comienzo, $this->campeonato_id);
        
        $res = $stmt->execute();
        $this->id = $this->mysqli->insert_id;
        return $res;
    }

    public function update() {
        $stmt = $this->mysqli->prepare("UPDATE PLAYOFF " .
                                       "SET comienzo = ?, campeonatoID = ? " .
                                       "WHERE id = ?");
        $comienzo = $this->comienzo->format("Y-m-d H:i:s");
        $stmt->bind_param("sii",
                          $comienzo,
                          $this->campeonato_id,
                          $this->id);
        return $stmt->execute();
    }

    public function delete() {
        $stmt = $this->mysqli->prepare("DELETE FROM PLAYOFF WHERE id = ?");
        $stmt->bind_param(i, $this->id);
        return $stmt->execute();
    }
    
    /*
    public static function getByJugador($jugador_id) {
        $mysqli = BDConector::createConection();

        $stmt = $mysqli->prepare("SELECT * FROM INSCRIPCION WHERE (jugador = ?)");
        $stmt->bind_param("i", $jugador_id);
        $stmt->execute();
        $res = $stmt->get_result();
        
        if($res->num_rows == 0)
            return array();

        $resultado = array();
        while($tupla = $res->fetch_array()) {
            $i = new Inscripcion_Model($tupla['id'],
                                       $tupla['jugador'],
                                       $tupla['partido']);
            array_push($resultado, $i);
        }
        return $resultado;
    }
    */
    
    public static function getByCampeonato($campeonato_id) {
        $mysqli = BDConector::createConection();

        $stmt = $mysqli->prepare("SELECT * FROM PLAYOFF WHERE campeonatoID = ?");
        $stmt->bind_param("i", $campeonato_id);
        $stmt->execute();
        $res = $stmt->get_result();
        
        if($res->num_rows == 0)
            return FALSE;
            
        $tupla = $res->fetch_array();
        $resultado = new Playoff_Model($tupla['id'],
                                    $tupla['comienzo'],
                                    $tupla['campeonatoID']);
        $enf = array();
        $stmt = $mysqli->prepare("SELECT * FROM PARTIDOSPLAYOFF JOIN ENFRENTAMIENTO ON PARTIDOSPLAYOFF.enfrent = ENFRENTAMIENTO.id WHERE playoff = ?");
        $stmt->bind_param("i", $resultado->id);
        $stmt->execute();
        $res = $stmt->get_result();
        
        while($tupla = $res->fetch_array()) {
            $e = array();
            
            $e['fase'] = $tupla['fase'];
            $e['p1'] = $tupla['pareja1'];
            $e['p2'] = $tupla['pareja2'];
            $e['r1'] = $tupla['res1'];
            $e['r2'] = $tupla['res2'];
            $e['eid'] = $tupla['id'];
            $e['reserva'] = Reserva_Model::getById($tupla['reserva']);
            array_push($enf, $e);
        }
        $resultado->enfrentamientos = $enf;
        return $resultado;
    }

    public static function getById($playoff_id) {
        $mysqli = BDConector::createConection();
        
        $stmt = $mysqli->prepare("SELECT * FROM INSCRIPCION WHERE (id = ?)");
        $stmt->bind_param("i", $playoff_id);
        $stmt->execute();
        $res = $stmt->get_result();
        
        if($res->num_rows == 0) {
            return NULL;
        }

        $tupla = $res->fetch_array();
        return new Playoff_Model($tupla['id'],
                                 $tupla['comienzo'],
                                 $tupla['campeonatoID']);
    }
    
    public static function getParejasBloque($campeonato_id) {
        $mysqli = BDConector::createConection();
        
        $stmt = $mysqli->prepare("SELECT * FROM PAREJASBLOQUE JOIN BLOQUE ON BLOQUE.id = PAREJASBLOQUE.idBloque WHERE campeonatoID = ?");
        $stmt->bind_param("i", $campeonato_id);
        $stmt->execute();
        $res = $stmt->get_result();
        
        $resultado = array();
        while($tupla = $res->fetch_array()) {
            array_push($resultado, array($tupla['idBloque'],$tupla['idPareja']));
        }
        return $resultado;
    }

    function __destruct()
    {
        // vacia
    }
}

?>
