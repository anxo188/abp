<?php
class Pareja_Model {

	var $id;

	/**
	 * id del capitan de la pareja
	 */
	var $capitan;
	
	/**
	 * id del acompañante de la pareja
	 */
	var $acomp;
	
	/**
	 * Conexión con la BD usada
	 */
	var $mysqli;


//Constructor de la clase
function __construct($capitan,$acomp){
     


	$this->capitan = $capitan;
	$this->acomp = $acomp;
	

}



//funcion de destrucción del objeto: se ejecuta automaticamente
//al finalizar el script
function __destruct()
{

}

function add(){
	if($this->capitan != $this->acomp){
	require_once 'Modelos/BDConector.php';
	$mysqli = BDConector::createConection();
	$stmt = $mysqli->prepare("SELECT * FROM PAREJA WHERE ((capitan=? AND acomp=?) OR (capitan=? AND acomp=?))");
	$stmt->bind_param("iiii",$this->capitan,$this->acomp,$this->acomp,$this->capitan);
	$stmt->execute();
	$resultado = mysqli_stmt_get_result($stmt);
	
	if($resultado->num_rows == 0){
		
		$stmt = $mysqli->prepare("INSERT INTO PAREJA ( capitan,acomp) VALUES(?,?)");
		$stmt->bind_param("ii",$this->capitan,$this->acomp);

		$resultado = $stmt->execute();

		if($resultado == true){

			return true;
		}else if($resultado == false){

			return false;
		}else{
			return $mysqli->error;
		}

	}else if($resultado->num_rows >= 1){
		return false;
	}else{
		return $mysqli->error;
	}
	}else{
	return 'No puedes jugar contigo mismo';

	}

	$mysqli->close();
	}




function getId(){
	require_once 'Modelos/BDConector.php';
	$mysqli = BDConector::createConection();
	$stmt = $mysqli->prepare("SELECT * FROM PAREJA WHERE (capitan=? AND acomp=?)");
	$stmt->bind_param("ii",$this->capitan,$this->acomp);
	$stmt->execute();
	$resultado = mysqli_stmt_get_result($stmt);

	if($resultado->num_rows == 1){
		$tmp = mysqli_fetch_object($resultado);
		return $tmp->id;
	}else if($resultado->num_rows == 0){
		return false;
	}else{
		return $mysqli->error;
	}
	$mysqli->close();
}//Fin getId


function getParejaById($id){
	require_once 'Modelos/BDConector.php';
	$mysqli = BDConector::createConection();
	$stmt = $mysqli->prepare("SELECT * FROM PAREJA WHERE capitan=? OR acomp=? ");
	$stmt->bind_param("ii",$id,$id);
	$stmt->execute();
	$resultado = mysqli_stmt_get_result($stmt);
	$mysqli->close();
	if($resultado->num_rows >= 1){
		return $resultado;
	}else if($resultado->num_rows == 0){
		return false;
	}else{
		return $mysqli->error;
	}
	
}//Fin getId

public static function get($idPareja){
	require_once 'Modelos/BDConector.php';
	$mysqli = BDConector::createConection();
	$stmt = $mysqli->prepare("SELECT * FROM PAREJA WHERE id=?");
	$stmt->bind_param("i",$idPareja);
	$stmt->execute();
	$resultado = $stmt->get_result();
	if($resultado->num_rows == 1){
		$mysqli->close();
		return $resultado;
	}else if($resultado->num_rows == 0){
		$mysqli->close();
		return false;
	}else{
		return $mysqli->error;
	}
}
public static function pertenece($idPareja, $idMiembro){
	require_once 'Modelos/BDConector.php';
	$mysqli = BDConector::createConection();
	$stmt = $mysqli->prepare("SELECT * FROM PAREJA WHERE id=? AND (capitan=? OR acomp=?)");
	$stmt->bind_param("iii",$idPareja,$idMiembro,$idMiembro);
	$stmt->execute();
	$resultado = mysqli_stmt_get_result($stmt);
	if($resultado->num_rows >= 1){
		$mysqli->close();
		return true;
	}else if($resultado->num_rows < 1){
		$mysqli->close();
		return false;
	}else{
		console_log($mysqli->error);
	}
}


function comprobarUsuario($idPareja,$idCapitan,$idAcomp){
    $contCapitan=0;
    $contAcomp=0;

	require_once 'Modelos/BDConector.php';
	$mysqli = BDConector::createConection();
	$stmt = $mysqli->prepare("SELECT * FROM PAREJA WHERE id=?");
	$stmt->bind_param("i",$idPareja);
	$stmt->execute();
	$resultado = mysqli_stmt_get_result($stmt);

	$mysqli->close();

	if($resultado->num_rows >= 1){
		while($tupla =  mysqli_fetch_object($resultado)){ 
            if($tupla->capitan == $idCapitan || $tupla->acomp == $idCapitan){
                $contCapitan++;
            }

            if($tupla->capitan == $idAcomp || $tupla->acomp == $idAcomp){
                $contAcomp++;
            }
	}
	if($contCapitan==0 && $contAcomp==0){
            return ;
        }else{

       		 if(($contCapitan!=0 && $contAcomp==0) || ($contCapitan==0 && $contAcomp!=0)){
                
                return "Uno de los miembros ya esta anotado en el campeonato";
            }else{
                
                return "Ambos miembros de la pareja ya estan anotados en el campeonato";
                
            }
        }

	}else if($resultado->num_rows == 0){
		return false;
	}else{
		return $mysqli->error;
	}
	


}	


	}//Register end


?>
