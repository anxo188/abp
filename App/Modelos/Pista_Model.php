<?php
class Pista_Model {

    var $id;
	/**
	 * Nombre de la pista
	 */
	var $nombre;

	/**
	 * Descripcion de la pista
	 */
	var $descripcion;

	/**
	 * Inicio del horario de la pista
	 */
	var $inicio;

	/**
	 * Horario de cierre de la pista
	 */
	var $fin;
	
	/**
	 * Conexión con la BD usada
	 */
	var $mysqli;


//Constructor de la clase
    function __construct($nombre,$descripcion,$inicio,$fin,$id){
        
	$this->nombre = $nombre;
	$this->descripcion = $descripcion;
	$this->inicio = $inicio;
	$this->fin = $fin;
    $this->id = $id;

	require_once 'Modelos/BDConector.php';
	$this->mysqli = BDConector::createConection();
}



//funcion de destrucción del objeto: se ejecuta automaticamente
//al finalizar el script
function __destruct()
{
}

	function add(){

		if(
			preg_match('/^[0-9]{4}\-(0[1-9]|1[0-2])\-(0[1-9]|[1-2][0-9]|3[0-1])\s[0-9]{2}\:[0-9]{2}\:[0-9]{2}$/',$this->inicio) 
			&& 
			preg_match('/^[0-9]{4}\-(0[1-9]|1[0-2])\-(0[1-9]|[1-2][0-9]|3[0-1])\s[0-9]{2}\:[0-9]{2}\:[0-9]{2}$/',$this->fin)
		){
			
			$stmt = $this->mysqli->prepare("INSERT INTO PISTA (nombre,descripcion,inicio,fin) VALUES (?,?,?,?)");
			$stmt->bind_param("ssss",$this->nombre,$this->descripcion,$this->inicio,$this->fin);
			$resultado = $stmt->execute();
			
			if($resultado == true){
				return true;
			}else if($resultado == false){
				return false;
			}else{
				return $this->mysqli->error;
			}

		}else{
			return "Formato de fecha incorrecto: \n".$this->inicio."\n".$this->fin."\n";
		}
	}//Fin add

	public function edit(){
			if(
				preg_match('/^[0-9]{4}\-(0[1-9]|1[0-2])\-(0[1-9]|[1-2][0-9]|3[0-1])\s[0-9]{2}\:[0-9]{2}\:[0-9]{2}$/',$this->inicio) 
				&& 
				preg_match('/^[0-9]{4}\-(0[1-9]|1[0-2])\-(0[1-9]|[1-2][0-9]|3[0-1])\s[0-9]{2}\:[0-9]{2}\:[0-9]{2}$/',$this->fin)
			){
				
				$stmt = $this->mysqli->prepare("UPDATE PISTA SET nombre =?, descripcion=?, inicio=?, fin=? WHERE id=?");
				$stmt->bind_param("ssssi",$this->nombre,$this->descripcion,$this->inicio,$this->fin,$this->id);
				$resultado = $stmt->execute();
				
				if($resultado == true){
					return true;
				}else if($resultado == false){
					return false;
				}else{
					return $this->mysqli->error;
				}
	
			}else{
				return "Formato de fecha incorrecto: \n".$this->inicio."\n".$this->fin."\n";
			}
		
	}

	public static function get($id){
		require_once 'Modelos/BDConector.php';
		$mysqli = BDConector::createConection();

		$stmt = $mysqli->prepare("SELECT * FROM PISTA WHERE id=?");
		$stmt->bind_param("i",$id);
		$stmt->execute();
		$resultado = mysqli_stmt_get_result($stmt);
		$mysqli->close();
		if($resultado->num_rows == 1){
			$tupla = $resultado->fetch_array();
            return new Pista_Model($tupla['nombre'],
                                   $tupla['descripcion'],
                                   $tupla['inicio'],
                                   $tupla['fin'],
                                   $tupla['id']);
		}else if($resultado->num_rows == 0){
			return false;
		}else{
			return $mysqli->error;
		}

	}//Fin get

	public static function getAll(){
		
		require_once 'Modelos/BDConector.php';
		$mysqli = BDConector::createConection();
		echo "\n\n\n";
		$stmt = $mysqli->prepare("SELECT * FROM PISTA");
		$stmt->execute();
		$resultado = $stmt->get_result();
		if($resultado->num_rows >= 1){
			$res = array();
            		while($tupla = $resultado->fetch_array()) {
                		$pista = new Pista_Model($tupla['nombre'],
                                         $tupla['descripcion'],
                                         $tupla['inicio'],
                                         $tupla['fin'],
                                         $tupla['id']);
                		array_push($res, $pista);
            		}		
            		return $res;
		}else if($resultado->num_rows == 0){
			return false;
		}else{
			return $mysqli->error;
		}
	}//Fin getAll

	public static function delete($id){
		
		require_once 'Modelos/BDConector.php';
		$mysqli = BDConector::createConection();

		$stmt = $mysqli->prepare("DELETE FROM PISTA WHERE id=?");
		$stmt->bind_param("i",$id);
		$resultado = $stmt->execute();

		$mysqli->close();
		if($resultado == true){
				return true;
		}else if($resultado == false){
			return false;
		}else{
			return $mysqli->error;
		}
	}//Fin delete

	public static function isAvailable($idPista,$time){

		$resultado = Pista_Model::get($idPista);
		$pista = mysqli_fetch_object($resultado);
		
		if($pista == false){
			return false;
		}else{
			$inicio = $pista->inicio;
			$fin = $pista->fin;

			if(is_string($inicio)){
				$inicio = strtotime($inicio);
			}

			if(is_string($fin)){
				$fin = strtotime($fin);
			}
			if(is_string($time)){
				$checkedTime = strtotime($time);
			}else{
				$checkedTime = $time;
			}

			if($inicio < $checkedTime && $checkedTime < $fin){
				return true;
				require_once 'Modelos/Reserva_Model.php';
				if(!reserved($idPista,$time)){
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}
		
	}

}//Class end

?>
