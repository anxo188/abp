<?php
class Bloque_Model {


	var $idCampeonato;

	var $numParejas;

	/**
	 * Conexión con la BD usada
	 */
	var $mysqli;


//Constructor de la clase
function __construct($idCampeonato,$numParejas){
        
	$this->idCampeonato = $idCampeonato;
	$this->numParejas = $numParejas;


	require_once 'Modelos/BDConector.php';
	$this->mysqli = BDConector::createConection();
}



//funcion de destrucción del objeto: se ejecuta automaticamente
//al finalizar el script
function __destruct()
{
	$this->mysqli->close();
}

	function add(){


			
			$stmt = $this->mysqli->prepare("INSERT INTO BLOQUE (campeonatoID,numParejas) VALUES (?,?)");
			$stmt->bind_param("ii",$this->idCampeonato,$this->numParejas);
			$resultado = $stmt->execute();

			if($resultado == true){
				return true;
			}else if($resultado == false){
				return false;
			}else{
				return $this->mysqli->error;
			}
	}//Fin add
	function getId(){
		$stmt = $this->mysqli->prepare("SELECT MAX(id) AS id FROM BLOQUE WHERE campeonatoID=?");
		$stmt->bind_param("i",$this->idCampeonato);
		$resultado = $stmt->execute();
		$respuesta = $stmt->get_result();
		$bloque=mysqli_fetch_object($respuesta);

		if($resultado == true){
			return $bloque->id;
		}else if($resultado == false){
			return false;
		}else{
			return $this->mysqli->error;
		}
	}
	public static function get($id){
		require_once 'Modelos/BDConector.php';
		$mysqli = BDConector::createConection();

		$stmt = $mysqli->prepare("SELECT * FROM BLOQUE WHERE id=?");
		$stmt->bind_param("i",$id);
		$stmt->execute();
		$resultado = mysqli_stmt_get_result($stmt);
		$mysqli->close();
		if($resultado->num_rows >= 1){
			return $resultado;
		}else if($resultado->num_rows == 0){
			return false;
		}else{
			return $mysqli->error;
		}

	}//Fin get

	public static function getAll($idCampeonato){
		require_once 'Modelos/BDConector.php';
		$mysqli = BDConector::createConection();
		
		$stmt = $mysqli->prepare("SELECT * FROM BLOQUE WHERE campeonatoID = ?");
		$stmt->bind_param("i",$idCampeonato);
		$stmt->execute();
		$resultado = mysqli_stmt_get_result($stmt);
		$mysqli->close();
		if($resultado->num_rows >= 1){
			return $resultado;
		}else if($resultado->num_rows == 0){
			return false;
		}else{
			return $mysqli->error;
		}
	}//Fin getAll

	public static function delete($id){
		
		require_once 'Modelos/BDConector.php';
		$mysqli = BDConector::createConection();

		$stmt = $mysqli->prepare("DELETE FROM BLOQUE WHERE id=?");
		$stmt->bind_param("i",$id);
		$resultado = $stmt->execute();

		$mysqli->close();
		if($resultado == true){
				return true;
		}else if($resultado == false){
			return false;
		}else{
			return $mysqli->error;
		}
	}//Fin delete
	public static function increaseNumPairs($idBloque){
	                require_once 'Modelos/BDConector.php';
			$mysqli = BDConector::createConection();
			$stmt = $mysqli->prepare("SELECT * FROM BLOQUE WHERE id=?");
			$stmt->bind_param("i",$idBloque);
			$stmt->execute();
			$resultado = mysqli_stmt_get_result($stmt);
			if($resultado->num_rows == 1){
				$bloque = mysqli_fetch_object($resultado);
				$numParejas = $bloque->numParejas + 1;
				$stmt = $mysqli->prepare("UPDATE BLOQUE SET numParejas=? WHERE id=?");
				$stmt->bind_param("ii",$numParejas,$idBloque);	
				return $stmt->execute();

				
			}else if($resultado->num_rows == 0){
				return false;
			}else{
				return $mysqli->error;
			}
	}
}//Class end

?>
