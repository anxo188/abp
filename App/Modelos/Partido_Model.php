<?php

require_once 'Modelos/BDConector.php';
require_once 'Modelos/Reserva_Model.php';

class Partido_Model {

    public $id;
    public $reserva_id;
    public $reserva;

	/**
	 * Conexión con la BD usada
	 */
	var $mysqli;

    function __construct($id, $reserva_id) {
        $this->id = $id;
        $this->reserva_id = $reserva_id;
        
        $this->mysqli = BDConector::createConection();
    }

    public function save() {
        $stmt = $this->mysqli->prepare("INSERT INTO PARTIDO VALUES (?,?)");
        $stmt->bind_param("ii",
                          $this->id,
                          $this->reserva_id);
        return $stmt->execute();
    }

    public function update() {
        $stmt = $this->mysqli->prepare("UPDATE PARTIDO " .
                                       "SET reservaID = ? " .
                                       "WHERE id = ?");
        $stmt->bind_param("ii",
                          $this->reserva_id,
                          $this->id);
        return $stmt->execute();
    }

    public function delete() {
        $stmt = $this->mysqli->prepare("DELETE FROM PARTIDO WHERE id = ?");
        $stmt->bind_param("i",
                          $this->id);
        return $stmt->execute();
    }
    
    public function getNumInscritos() {
        $stmt = $this->mysqli->prepare("select count(*) from INSCRIPCION " .
                                       "join PARTIDO on PARTIDO.id = INSCRIPCION.partido " .
                                       "where PARTIDO.id = ?");
        $stmt->bind_param("i", $this->id);
        $stmt->execute();
        $res = $stmt->get_result();
        return ($res->fetch_array())[0];
    }

    public static function getAfterDate($fecha) {
        $mysqli = BDConector::createConection();

        $stmt = $mysqli->prepare("SELECT * FROM RESERVA JOIN " .
                                 "PARTIDO ON RESERVA.id = PARTIDO.reservaID " .
                                 "WHERE dia > ?");
        $fecha_str = $fecha->format("Y-m-d H:i:s");
        $stmt->bind_param("s", $fecha_str);
        $stmt->execute();
        $res = $stmt->get_result();

        $resultado = array();
        while($tupla = $res->fetch_array()) {
            $t = new Partido_Model($tupla['id'], $tupla['reservaID']);
            array_push($resultado, $t);
        }
        $mysqli->close();
        return $resultado;
    }

    /*
    public static function getByReserva($reserva_id) {
        var $mysqli = BDConector::createConection();

        $stmt = $mysqli->prepare("SELECT * FROM PARTIDO WHERE (reservaID = ?)");
        $stmt->bind_param("i", $reserva_id);
        $res = $stmt->execute();

        if($res->num_rows == 0)
            return NULL;

        $tupla = $res->fetch_array();
        return new Partido_Model($tupla['id'],
                                 $tupla['reservaID']);
    }
    */
    public static function getById($partido_id) {
        $mysqli = BDConector::createConection();
        
        $stmt = $mysqli->prepare("SELECT * FROM PARTIDO WHERE id = ?");
        $stmt->bind_param("i", $partido_id);
        $stmt->execute();
        $res = $stmt->get_result();

        if($res->num_rows == 0) {
            return NULL;
        }

        $tupla = $res->fetch_array();
        $resultado = new Partido_Model($tupla['id'], $tupla['reservaID']);
        $mysqli->close();
        return $resultado;
    }
    
    function __destruct()
    {
        // vacia
    }
}

?>
