<?php

class Horario{
    function __construct(){   
    
    }

    public static function getHorarios(){
		require_once 'Modelos/BDConector.php';
  		$mysqli = BDConector::createConection();
		$stmt = $mysqli->prepare("SELECT * FROM HORARIO ORDER BY horaInicio");
		$stmt->execute();
		$resultado = mysqli_stmt_get_result($stmt);
		if($resultado->num_rows >= 1){
				$mysqli->close();
			return $resultado;
		}else if($resultado->num_rows == 0){
				$mysqli->close();
			return false;
		}else{
			return $mysqli->error;
		}
	}
	
	public static function get($id){
		require_once 'Modelos/BDConector.php';
  		$mysqli = BDConector::createConection();
		$stmt = $mysqli->prepare("SELECT * FROM HORARIO WHERE id= ?");
		$stmt->bind_param("i",$id);
		$stmt->execute();
		$resultado = mysqli_stmt_get_result($stmt);
		$horario = mysqli_fetch_object($resultado);

		$dateI = explode(" ",$horario->horaInicio);
		$dateI = $dateI[1];
		$dateF = explode(" ",$horario->horaFin);
		$dateF = $dateF[1];
		return $dateI." - ".$dateF;
	}
}

?>