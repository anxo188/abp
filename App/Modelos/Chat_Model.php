<?php
class Chat_Model
{
    public static function getFilePath($chatId){    
		require_once 'Modelos/BDConector.php';
        $mysqli = BDConector::createConection();
        $stmt = $mysqli->prepare("SELECT fichero FROM CHAT WHERE id=?");
		$stmt->bind_param("i",$chatId);
		$stmt->execute();
		$resultado = mysqli_stmt_get_result($stmt);
		$mysqli->close();
		if($resultado->num_rows == 1){
            $dato = mysqli_fetch_object($resultado);
            return $dato->fichero;
		}else if($resultado->num_rows == 0){
			return false;
		}else{
			return $mysqli->error;
		}
    
    }

    public static function getChatsByUser($userId){
		require_once 'Modelos/BDConector.php';
        $mysqli = BDConector::createConection();
        
        $stmt = $mysqli->prepare("SELECT id FROM CHAT WHERE usuario1=? OR usuario2=?");
		$stmt->bind_param("ii",$chatId,$chatId);
		$stmt->execute();
		$resultado = mysqli_stmt_get_result($stmt);
		$mysqli->close();
		if($resultado->num_rows > 0){
           return $resultado;
		}else if($resultado->num_rows  == 0){
			return false;
		}else{
			return $mysqli->error;
		}
    }

    public static function createChat($usuario1,$usuario2,$fichero){
        require_once 'Modelos/BDConector.php';
        $mysqli = BDConector::createConection();
        $stmt = $mysqli->prepare("INSERT INTO CHAT (usuario1,usuario2,fichero) VALUES (?,?,?)");
        $stmt->bind_param("iis",$usuario1,$usuario2,$fichero);
        $resultado = $stmt->execute();
        $stmt = $mysqli->prepare("SELECT id FROM CHAT WHERE usuario1=? AND usuario2=? AND fichero=?");
        $stmt->bind_param("iis",$usuario1,$usuario2,$fichero);
        $stmt->execute();
        $resultado = mysqli_stmt_get_result($stmt);
        $resultado=mysqli_fetch_object($resultado);
        $mysqli->close();
        return $resultado->id;
    
    }

    public static function deleteChat($chatId){
        unlink(getFilePath($chatId));
        require_once 'Modelos/BDConector.php';
        $mysqli = BDConector::createConection();
        $stmt = $mysqli->prepare("DELETE FROM NOTIFICACION WHERE id=?");
        $stmt->bind_param("i",$chatId);
        $resultado = $stmt->execute();
        $mysqli->close();
        return $resultado;
    }

    public static function Exists($usuario1, $usuario2){
        require_once 'Modelos/BDConector.php';
        $mysqli = BDConector::createConection();
        $stmt = $mysqli->prepare("SELECT id FROM CHAT WHERE (usuario1=? AND usuario2=?) OR (usuario1=? AND usuario2=?)");
        $stmt->bind_param("iiii",$usuario1,$usuario2,$usuario2,$usuario1);
        $stmt->execute();
        $resultado = mysqli_stmt_get_result($stmt);
        $mysqli->close();
        console_log($resultado->num_rows);
        if($resultado->num_rows == 1){
            $resultado=mysqli_fetch_object($resultado);
            return $resultado->id;
        }else{
            return -1;
        }
    }

    public static function pubishNotification($usuario,$fichero){
        require_once 'Modelos/BDConector.php';
        $mysqli = BDConector::createConection();
        $stmt = $mysqli->prepare("INSERT INTO NOTIFICACION (usuario,fichero,leido) VALUES (?,?,0)");
        $stmt->bind_param("si",$usuario,$codigo);
        $resultado = $stmt->execute();
        $mysqli->close();
        return $resultado;
    }
    public static function markAsNew($id){
        require_once 'Modelos/BDConector.php';
        $mysqli = BDConector::createConection();
        $stmt = $mysqli->prepare("UPDATE NOTIFICACION SET leido=0 WHERE id=?");
        $stmt->bind_param("i",$id);
        $resultado = $stmt->execute();
        $mysqli->close();
        return $resultado;
    }

    public static function removeNotification($id){
        require_once 'Modelos/BDConector.php';
        $mysqli = BDConector::createConection();
        $stmt = $mysqli->prepare("UPDATE NOTIFICACION SET leido=1 WHERE id=?");
        $stmt->bind_param("i",$id);
        $resultado = $stmt->execute();
        $mysqli->close();
        return $resultado;
    }
}

?>