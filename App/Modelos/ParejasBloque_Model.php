<?php
	
class ParejaBloque{
	var $id;

	var $idBloque;
	

	var $idPareja;
	
	/**
	 * Conexión con la BD usada
	 */
	var $mysqli;


//Constructor de la clase
function __construct($idBloque,$idPareja){
     


	$this->idBloque = $idBloque;
	$this->idPareja = $idPareja;
	

}



//funcion de destrucción del objeto: se ejecuta automaticamente
//al finalizar el script
function __destruct()
{

}

	function add(){
		if($this->idBloque != $this->idPareja){
		require_once 'Modelos/BDConector.php';
		$mysqli = BDConector::createConection();
		$stmt = $mysqli->prepare("SELECT * FROM PAREJASBLOQUE WHERE (idBloque=? AND idPareja=?) ");
		$stmt->bind_param("ii",$this->idBloque,$this->idPareja);
		$stmt->execute();
		$resultado = mysqli_stmt_get_result($stmt);
		
		if($resultado->num_rows == 0){
			
			$stmt = $mysqli->prepare("INSERT INTO PAREJASBLOQUE ( idBloque,idPareja) VALUES(?,?)");
			$stmt->bind_param("ii",$this->idBloque,$this->idPareja);
			
			$resultado = $stmt->execute();

			if($resultado == true){
				return true;
			}else if($resultado == false){
				return false;
			}else{
				return $mysqli->error;
			}

		}else if($resultado->num_rows >= 1){
			return false;
		}else{
			return $mysqli->error;
		}
		}else{
		return false;

		}

		$mysqli->close();
	}
	public static function getParejas($idBloque){
		require_once 'Modelos/BDConector.php';
  		$mysqli = BDConector::createConection();
		$stmt = $mysqli->prepare("SELECT * FROM PAREJASBLOQUE WHERE idBloque=?");
		$stmt->bind_param("i",$idBloque);
		$stmt->execute();
		$resultado = mysqli_stmt_get_result($stmt);
		if($resultado->num_rows >= 1){
				$mysqli->close();
			return $resultado;
		}else if($resultado->num_rows == 0){
				$mysqli->close();
			return false;
		}else{
			return $mysqli->error;
		}
	}

	public static function pertenece($idBloque, $idJugador){
		require_once 'Modelos/Pareja_Model.php';
		$parejas = ParejaBloque::getParejas($idBloque);
		$resultado = false;
		while( ($pareja = mysqli_fetch_object($parejas) ) && $resultado==false){
			if( Pareja_Model::pertenece($pareja->idPareja,$idJugador) ){
				$resultado = true;
			}
		}
		return $resultado;
		
	}
}
?>
