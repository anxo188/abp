<?php

require_once('Modelos/BDConector.php');
require_once('Modelos/Pista_Model.php');

class Reserva_Model {

    public $id;
    public $dia;
    public $horario;
    public $pista_id;
    public $creador_id;
    public $confirmada;
    public $pista;

	/**
	 * Conexión con la BD usada
	 */
	private $mysqli;

    function __construct($id, $dia, $horario, $pista_id, $creador_id, $confirmada ) {
        $this->id = $id;
        $this->dia = new DateTime($dia);
        $this->horario = $horario;
        $this->pista_id = $pista_id;
        $this->creador_id = $creador_id;
        $this->confirmada = $confirmada;
        
        $this->mysqli = BDConector::createConection();
    }

    public function save() {
        $stmt = $this->mysqli->prepare("INSERT INTO RESERVA VALUES (?,?,?,?,?,?)");
        $dia = $this->dia->format("Y-m-d H:i:s");
        $stmt->bind_param("isiiii",
                          $this->id,
                          $dia,
                          $this->horario,
                          $this->pista_id,
                          $this->creador_id,
                          $this->confirmada);
        $ok = $stmt->execute();

        if(!$ok) return FALSE;
        $this->id = $this->mysqli->insert_id;
        return TRUE;
    }

    public function delete() {
        $stmt = $this->mysqli->prepare("DELETE FROM RESERVA WHERE id = ?");
        $stmt->bind_param("i", $this->id);
        return $stmt->execute();
    }

    public function update() {
        $stmt = $this->mysqli->prepare("UPDATE RESERVA " .
                                       "SET dia = ?, horario = ?, pista = ?, " .
                                       "creador = ?, confirmada = ? " .
                                       "WHERE id = ?");
        $stmt->bind_param("siiiii",
                          $this->dia->format("Y-m-d H:i:s"),
                          $this->horario,
                          $this->pista_id,
                          $this->creador_id,
                          $this->confirmada,
                          $this->id);
        return $stmt->execute();
    }

    public static function getByCreador($creador_id) {
        $mysqli = BDConector::createConection();

        $stmt = $mysqli->prepare("SELECT * FROM RESERVA WHERE (creador = ?)");
        $stmt->bind_param("i", $creador_id);
        $stmt->execute();
        $res = $stmt->get_result();

        if($res->num_rows == 0)
            return array();
        
        $resultado = array();

        while($tupla = $res->fetch_array()) {
            $pista_id = $tupla['pista'];
            $t = new Reserva_Model($tupla['id'],
                                   $tupla['dia'],
                                   $tupla['horario'],
                                   $pista_id,
                                   $tupla['creador'],
                                   $tupla['confirmada']);
            array_push($resultado, $t);
        }
        $mysqli->close();
        return $resultado;
    }

    public static function getById($reserva_id) {
        $mysqli = BDConector::createConection();
        
        $stmt = $mysqli->prepare("SELECT * FROM RESERVA WHERE (id = ?)");
        $stmt->bind_param("i", $reserva_id);
        $stmt->execute();
        $res = $stmt->get_result();
        
        if($res->num_rows == 0) {
            return NULL;
        }
        $tupla = $res->fetch_array();
        $resultado =  new Reserva_Model($tupla['id'],
                                 $tupla['dia'],
                                 $tupla['horario'],
                                 $tupla['pista'],
                                 $tupla['creador'],
                                 $tupla['confirmada']);
        $mysqli->close();
        return $resultado;
    }

    public static function isReserved($pista_id, $hora) {
        $mysqli = BDConector::createConection();
        $stmt = $mysqli->prepare("SELECT COUNT(*) FROM RESERVA WHERE (pista = ? AND dia = ?)");
        $stmt->bind_param("is", $pista_id, $hora);
        $stmt->execute();
        $res = $stmt->get_result();
        return $res;
    }
    
    //Obtiene las reservas de aqui a una semana
    public static function getWeek(){
        $mysqli = BDConector::createConection();
        $todayArray = getdate();
        $today = $todayArray['year']."-".$todayArray['mon']."-".$todayArray['mday'];//año-numero mes- numero dia del mes
        for($i = 0; $i<7;$i++){
            $day = getdate(strtotime("+".$i." day"));
            $analizedDay = $day['year']."-".$day['mon']."-".$day['mday'];
            $stmt = $mysqli->prepare("SELECT * FROM RESERVA WHERE dia =? AND confirmada=1");
            $stmt->bind_param("s",  $analizedDay);
            $stmt->execute();
            $res[$i]=$stmt->get_result();
        }

        return $res;
    }

     public static function getIdReserva($dia,$horario,$pista,$creador){
      
      require_once 'Modelos/BDConector.php';
      $mysqli = BDConector::createConection();
      $stmt = $mysqli->prepare("SELECT * FROM RESERVA WHERE dia=? AND horario=? AND pista=? AND creador=?");
      $stmt->bind_param("siii",$dia,$horario,$pista,$creador);
      $stmt->execute();
      $resultado = mysqli_stmt_get_result($stmt);
      $mysqli->close();
      if($resultado->num_rows == 1){
      
        return $resultado;
      }else if($resultado->num_rows == 0){
        return false;
      }else{
        return $mysqli->error;
      }

    }

    function __destruct()
    {
        // vacia
    }
}

?>
