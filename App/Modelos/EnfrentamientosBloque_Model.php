<?php
	
class EnfrentamientosBloque{
	var $idBloque;
	var $idEnfrentamiento;

	function __construct($idEnfrentamiento,$idBloque){
		$this->idBloque = $idBloque;
		$this->idEnfrentamiento = $idEnfrentamiento;
	}

	function add(){
		console_log("Registrando enfrentamiento: ".	$this->idEnfrentamiento." en bloque: ".$this->idBloque);
			require_once 'Modelos/BDConector.php';
			$mysqli = BDConector::createConection();

				
				$stmt = $mysqli->prepare("INSERT INTO ENFRENTAMIENTOSBLOQUE ( idBloque,idEnfrentamiento) VALUES(?,?)");
				$stmt->bind_param("ii",$this->idBloque,$this->idEnfrentamiento);
				console_log("Registrando...");
				$resultado = $stmt->execute();

				if($resultado == true){
					console_log("Exito");
					return true;
				}else if($resultado == false){
					return false;
				}else{
					console_log("Error: ".$mysqli->error);
					return $mysqli->error;
				}
	
		$mysqli->close();

	}



	public static function getEnfrentamientos($idBloque){
		require_once 'Modelos/BDConector.php';
  		$mysqli = BDConector::createConection();

  		$stmt = $mysqli->prepare("SELECT * FROM ENFRENTAMIENTOSBLOQUE WHERE idBloque=?");
  		$stmt->bind_param("i",$idBloque);
  		$stmt->execute();
  		$resultado = $stmt->get_result();
		if($resultado->num_rows >= 1){
				$mysqli->close();
			return $resultado;
		}else if($resultado->num_rows == 0){
				$mysqli->close();
			return false;
		}else{
			return $mysqli->error;
		}
	}
}
?>