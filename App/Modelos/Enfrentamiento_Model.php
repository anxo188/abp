<?php
	
class Enfrentamiento{
	var $idPareja1;
	var $idPareja2;

	function __construct($idPareja1,$idPareja2){
		$this->idPareja1 = $idPareja1;
		$this->idPareja2 = $idPareja2;

		
	
	}

	function add(){
		if($this->idPareja1 != $this->idPareja2){
			require_once 'Modelos/BDConector.php';
			$mysqli = BDConector::createConection();
				
				$stmt = $mysqli->prepare("INSERT INTO ENFRENTAMIENTO ( pareja1,pareja2) VALUES(?,?)");
				$stmt->bind_param("ii",$this->idPareja1,$this->idPareja2);
				$resultado = $stmt->execute();

				if($resultado == true){
					return true;
				}else if($resultado == false){
					return false;
				}else{
					console_log("Error al insertar enfrentamiento: ".$mysqli->error);
					return $mysqli->error;
				}
		}else{
			return false;

		}
	}
	public static function edit($id,$res1,$res2){
		
			require_once 'Modelos/BDConector.php';
			$mysqli = BDConector::createConection();
				
				$stmt = $mysqli->prepare("UPDATE ENFRENTAMIENTO SET res1=?, res2=? 	WHERE id=?");
				$stmt->bind_param("iii",$res1,$res2,$id);
				$resultado = $stmt->execute();

				if($resultado == true){
					return true;
				}else if($resultado == false){
					return false;
				}else{
					console_log("Error al editar enfrentamiento: ".$mysqli->error);
					return $mysqli->error;
				}
		
	}

    public static function get($id) {
		
        include_once('Modelos/BDConector.php');
		$mysqli = BDConector::createConection();
        $stmt = $mysqli->prepare("SELECT * FROM ENFRENTAMIENTO WHERE id = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $res = $stmt->get_result();
		$tupla = mysqli_fetch_object($res);
        return $tupla;
	}


	public function getId() {
		require_once('Modelos/BDConector.php');
        $mysqli = BDConector::createConection();
        $stmt = $mysqli->prepare("SELECT * FROM ENFRENTAMIENTO WHERE pareja1 = ? AND pareja2 = ? AND res1 = 0 AND res2 = 0 AND reserva IS NULL");
	
		$stmt->bind_param("ii",$this->idPareja1,$this->idPareja2);
        $stmt->execute();
        $res = $stmt->get_result();
		$tupla = $res->fetch_object();
        return $tupla->id;
	}



	public static function ganado($idEnfrentamiento,$idPareja){
		require_once 'Modelos/BDConector.php';
		$mysqli = BDConector::createConection();
		$stmt = $mysqli->prepare ("SELECT * FROM ENFRENTAMIENTO WHERE id=? AND (pareja1 = ? OR pareja2 = ?)");
		$stmt->bind_param("iii",$idEnfrentamiento,$idPareja,$idPareja);
		$stmt->execute();
		$resultado =$stmt->get_result();
		$enfrentamiento = mysqli_fetch_object($resultado);
		if($enfrentamiento == NULL){
			return false;
		}
		if($enfrentamiento->pareja1 == $idPareja){
			if($enfrentamiento->res1 > $enfrentamiento->res2){
				return true;
			}else{
				return false;
			}
		}else{
			if($enfrentamiento->res2 > $enfrentamiento->res1){
				return true;
			}else{
				return false;
			}
		}
		
	}

        public static function jugado($idEnfrentamiento,$idPareja){
	
	                require_once 'Modelos/BDConector.php';
		        $mysqli = BDConector::createConection();
 			$stmt = $mysqli->prepare ("SELECT * FROM ENFRENTAMIENTO WHERE id=? AND (pareja1=? OR pareja2=?)");
			$stmt->bind_param("iii",$idEnfrentamiento,$idPareja,$idPareja);
			$stmt->execute();
			$resultado =$stmt->get_result();
			$enfrentamiento = mysqli_fetch_object($resultado);
			$mysqli->close();
			if($enfrentamiento == null){
				return false;
			}
			if($enfrentamiento->res1>0 || $enfrentamiento->res2>0){
				
				return true;
			}else{
				return false;
			}

	}	
}//Fin clase
?>
