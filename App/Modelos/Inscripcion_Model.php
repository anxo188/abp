<?php

require_once 'Modelos/BDConector.php';
require_once 'Modelos/Partido_Model.php';

class Inscripcion_Model {

    public $id;
    public $jugador_id;
    public $partido_id;
    public $partido;

	/**
	 * Conexión con la BD usada
	 */
	private $mysqli;

    function __construct($id, $jugador_id, $partido_id) {
        $this->id = $id;
        $this->jugador_id = $jugador_id;
        $this->partido_id = $partido_id;
        
        $this->mysqli = BDConector::createConection();
    }

    public function save() {
        $stmt = $this->mysqli->prepare("INSERT INTO INSCRIPCION VALUES (?,?,?)");
        $stmt->bind_param("iii",
                          $this->id,
                          $this->jugador_id, $this->partido_id);
        return $stmt->execute();
    }

    public function update() {
        $stmt = $this->mysqli->prepare("UPDATE INSCRIPCION " .
                                       "SET jugador = ?, partido = ? " .
                                       "WHERE id = ?");
        $stmt->bind_param("iii",
                          $this->jugador_id,
                          $this->partido_id,
                          $this->id);
        return $stmt->execute();
    }

    public function delete() {
        $stmt = $this->mysqli->prepare("DELETE FROM INSCRIPCION WHERE id = ?");
        $stmt->bind_param(i, $this->id);
        return $stmt->execute();
    }

    public static function getByJugador($jugador_id) {
        $mysqli = BDConector::createConection();

        $stmt = $mysqli->prepare("SELECT * FROM INSCRIPCION WHERE (jugador = ?)");
        $stmt->bind_param("i", $jugador_id);
        $stmt->execute();
        $res = $stmt->get_result();
        
        if($res->num_rows == 0)
            return array();

        $resultado = array();
        while($tupla = $res->fetch_array()) {
            $i = new Inscripcion_Model($tupla['id'],
                                       $tupla['jugador'],
                                       $tupla['partido']);
            array_push($resultado, $i);
        }
        return $resultado;
    }

    public static function getByJugadorPartido($jugador_id, $partido_id) {
        $mysqli = BDConector::createConection();

        $stmt = $mysqli->prepare("SELECT * FROM INSCRIPCION WHERE (jugador = ? AND partido = ?)");
        $stmt->bind_param("ii", $jugador_id, $partido_id);
        $stmt->execute();
        $res = $stmt->get_result();
        
        if($res->num_rows == 0)
            return array();

        $resultado = array();
        while($tupla = $res->fetch_array()) {
            $i = new Inscripcion_Model($tupla['id'],
                                       $tupla['jugador'],
                                       $tupla['partido']);
            array_push($resultado, $i);
        }
        return $resultado;
    }

    public static function getById($inscripcion_id) {
        $mysqli = BDConector::createConection();
        
        $stmt = $mysqli->prepare("SELECT * FROM INSCRIPCION WHERE (id = ?)");
        $stmt->bind_param("i", $inscripcion_id);
        $stmt->execute();
        $res = $stmt->get_result();
        
        if($res->num_rows == 0) {
            return NULL;
        }

        $tupla = $res->fetch_array();
        return new Inscripcion_Model($tupla['id'],
                                     $tupla['jugador'],
                                     $tupla['partido']);
    }

    function __destruct()
    {
        // vacia
    }
}

?>
