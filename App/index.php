<?php
session_start();

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include "Funciones/consoleLogger.php";
if(!isset($_GET["controller"])){
    $_GET["controller"]="Front";
}

switch($_GET["controller"]){
    case "CampeonatoGeneral":
        require_once "Controladores/Campeonato_controller.php";
        break;
    case "CampeonatoInscription":
        require_once "Controladores/CampeonatoInscripcion_Controller.php";
    break;
    case "EscuelaGeneral":
        require_once "Controladores/Escuela_controller.php";
    break;
    case "ClaseGeneral":
        require_once "Controladores/Clase_Controller.php";
    break;
    case "GestorLiga":
        require_once "Controladores/GestorLiga_controller.php";
    break;
    case "Login":
        require_once "Controladores/Login_controller.php";
    break;
    case "Register":
        require_once "Controladores/Register_controller.php";
    break;
    case "Partido":
        require_once "Controladores/Partido_controller.php";
    break;
    case "Pista":
        require_once "Controladores/Pista_controller.php";
    break;
    case "Playoff":
        require_once "Controladores/Playoff_controller.php";
    break;
    case "Reserva":
        require_once "Controladores/Reserva_controller.php";
    break;
    case "Mensaje":
        require_once "Controladores/Mensaje_controller.php";
    break;
    case "Chat":
        require_once "Controladores/Chat_controller.php";
    break;
    case "Front": //Este frontal es el que muestra los contenidos del caso "Contenido WEB"
    default:
        require_once "Controladores/Front_controller.php";
        //require_once "Controladores/Login_controller.php";
        break;
}
if(isset($_GET['action']) && $_GET['action']=== "logout"){
    require_once "Funciones/logout.php";
    session_destroy();
	header('Location: /?controller=Front');
}
session_write_close();
/**
 * Permisos para Files
 * chown -R www-data:www-data Files
 * chmod go-rwx Files
*/
?>
