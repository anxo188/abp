<?php
require_once 'Modelos/Pareja_Model.php';
require_once 'Modelos/ParejasBloque_Model.php';
$idBloque = 1;
$parejas = ParejaBloque::getParejas($idBloque);
$parejaArray = array();
$cont = 0;
while($cont < 3 && $parejaID = mysqli_fetch_object($parejas)){
	$pareja = mysqli_fetch_object(
			Pareja_Model::get($parejaID->idPareja)
		);
	$parejaScore['idCapitan'] = $pareja->capitan;
	$parejaScore['idAcomp']=$pareja->acomp; 
	$parejaScore['score'] = calculateScore($pareja->id,$idBloque);
	array_push( $parejaArray, $parejaScore);	
$cont = $cont +1;
}
var_dump($parejaArray);




	function calculateScore($idPareja,$idBloque){
		require_once 'Modelos/EnfrentamientosBloque_Model.php';
		require_once 'Modelos/Enfrentamiento_Model.php';

		$enfrentamientos = EnfrentamientosBloque::getEnfrentamientos($idBloque);
		$cont = 0;
		$numGanados = 0;
		$numJugados = 0;
		while($cont < 3 && $enfrentamiento = mysqli_fetch_object($enfrentamientos)){
			$resultado = Enfrentamiento::ganado($enfrentamiento->idEnfrentamiento,$idPareja);
			if($resultado==true){
				$numGanados = $numGanados + 1;
				$numJugados = $numJugados + 1;
			}else if(Enfrentamiento::jugado($enfrentamiento->idEnfrentamiento)){
					$numJugados = $numJugados + 1;
			}
			$cont = $cont +1;
		}
		

		$score = ($numGanados*3)+($numJugados);
		return $score;
	}
?>
