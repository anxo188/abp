<?php
    $mysqli = new mysqli("localhost", "admin", "admin", "ABP_BD");
    
    $res = $mysqli->query("SELECT id FROM PAREJA");
    $pareja_ids = $res->fetch_all();
    
    $emparejamientos = array();
    $puntos = array();
    
    foreach($pareja_ids as $p) {
        $puntos[$p[0]] = 0;
    }
    
    foreach($pareja_ids as $p1) {
        foreach($pareja_ids as $p2) {
            if($p1[0] == $p2[0]) continue;
            
            $ganador_p1 = random_int(0, 100) > 50 ? TRUE : FALSE; 
            array_push($emparejamientos, array($p1[0], $p2[0], $ganador_p1? 3 : random_int(0, 2),
                        !$ganador_p1? 3 : random_int(0,2)));
            if($ganador_p1) {
                $puntos[$p1[0]] += 3;
                $puntos[$p2[0]] += 1;
            } else {
                $puntos[$p1[0]] += 1;
                $puntos[$p2[0]] += 3;
            }
        }
    }
    
    $res = $mysqli->query("select id from CAMPEONATO");
    $campeonato_id = $res->fetch_all()[0][0];
    
    foreach($pareja_ids as $p) {
        $pid = $p[0];
        $mysqli->query("insert into INSCRIPCIONCAMPEONATO values (0, $campeonato_id, $pid)");
    }
    $num_parejas = sizeof($pareja_ids);
    
    $mysqli->query("delete from BLOQUE");
    $mysqli->query("insert into BLOQUE values (0, $campeonato_id, $num_parejas)");
    
    $bloque_id = $mysqli->insert_id;
    $mysqli->query("delete from PAREJASBLOQUE");
    foreach($pareja_ids as $p) {
        $mysqli->query("insert into PAREJASBLOQUE values ($bloque_id, $p[0])");
    }
    
    $mysqli->query("delete from RESERVA");
    $mysqli->query("delete from ENFRENTAMIENTO");
    $mysqli->query("delete from ENFRENTAMIENTOSBLOQUE");
    $mysqli->query("delete from RANKINGLIGA");
    foreach($emparejamientos as $e) {
        $mysqli->query("insert into RESERVA values (0, '2019-11-21 12:00:00', 1, 1, 1)");
        $rid = $mysqli->insert_id;
        $mysqli->query("insert into ENFRENTAMIENTO values (0, $e[0], $e[1], $e[2], $e[3], $rid)");
        $eid = $mysqli->insert_id;
        $mysqli->query("insert into ENFRENTAMIENTOSBLOQUE values ($bloque_id, $eid)");
    }
    
    foreach($pareja_ids as $p) {
        $pid = $p[0];
        $p = $puntos[$pid];
        $mysqli->query("insert into RANKINGLIGA values ($bloque_id, $pid, $p)");
    }
?>
